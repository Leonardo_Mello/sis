﻿using SIS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SIS.ViewModel
{
    public class UserViewModel
    {
        /** DECLARAÇÃO DOS ATRIBUTOS DA CLASSE **/
        #region parameters
        // --Dado de identificação do usuário--
        private string id;
        // --Dados de contato do usuário--
        private string email;
        private List<Telephone> telephoneList = new List<Telephone>();

        /** Como nenhum dos atributos pode ser acessado ou modificado diretamente por alguma outra classe, criam-se 
        variavéis que serão as pontes para acessá-los e modificá-los (Getters e Setters)**/
        // --Dado de identificação do usuário--
        public string Id
        {
            get { return id; }
            set { id = value; }
        }

        // --Dados de contato do usuário--
        public string Email
        {
            get { return email; }
            set { email = value; }
        }
        public List<Telephone> TelephoneList
        {
            get { return telephoneList; }
            set { telephoneList = value; }
        }
        #endregion

        /** CONSTRUTORES DA CLASSE **/
        public UserViewModel(string id, string email, List<Telephone> telephoneList){
            Id = id;
            Email = email;
            TelephoneList = telephoneList;
        }

        /** MÉTODOS DA CLASSE **/
        #region methods
        // --Método desenvolvido para encobrir os verdadeiros valores das opções de contato informadas ao usuário--
        public void HideValues()
        {
            // Camuflando o endereço de e-mail do usuário recebido
            int emailAddress = email.IndexOf("@");
            string hiddenEmail = email.Substring(0, 2) + "*****" + email.Substring(emailAddress);
            email = hiddenEmail;                                                                                                                     
            // Camuflando o(s) número(s) de telefone do usuário recebido
            if (telephoneList != null)
            {
                foreach (Telephone item in telephoneList)
                {
                    // Guardando os dois últimos dígitos do número atual, pois os mesmos poderão ser visualizados pelo usuário
                    string lastNumbers = item.Numero.Substring(item.Numero.Length - 2);
                    // Criando a string que conterá os dígitos que serão camuflados
                    string hiddenNumber = item.Numero.Substring(0, item.Numero.Length - 2);
                    // Camuflando o número de telefone
                    hiddenNumber = hiddenNumber.Replace("0", "*").Replace("1", "*").Replace("2", "*").Replace("3", "*").Replace("4", "*")
                        .Replace("5", "*").Replace("6", "*").Replace("7", "*").Replace("8", "*").Replace("9", "*");

                    item.Numero = hiddenNumber + lastNumbers;
                }
            }
        }
        #endregion
    }
}