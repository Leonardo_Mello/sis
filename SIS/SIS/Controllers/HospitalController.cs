﻿using SIS.Models;
using SIS.Models.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SIS.Controllers
{
    public class HospitalController : Controller
    {
        public ActionResult Hospital_profile()
        {
            return View("Hospital_profile", (Hospital)Session["usuario"]);
        }

        ////---------------------------------METODO ALTERAR HOSPITAL-----------------------------------//
        //public ActionResult AlterarHospital(string cnes)
        //{
        //    //UTILIZAR O OBJETO USUARIO PARA CHAMAR O MÉTODO BUSCA USUARIO DO MODELO
        //    Hospital hospital = Hospital.GetInfo(cnes);

        //    //COMPARAR PARA VER SE ENCONTRAR UM OBJETO IGUAL
        //    if (hospital == null)
        //    {
        //        //MENSAGEM DE CONFIRMAÇÃO
        //        TempData["Msg"] = "Erro ao buscar hospitais!";
        //        //RETORNA PARA A VIEW 
        //        return RedirectToAction("Perfil");
        //    }
        //    return View(hospital);
        //}

        //---------------------------------METODO ALTERAR HOSPITAL-----------------------------------//
        [HttpPost]
        // CONSTRUTOR COM OS ATRIBUTOS
        public ActionResult AlterarHospital(byte[] foto, string email, string telefoneNumero, string enderecoNumero,
                                            string cep, string estado, string cidade, string bairro, string rua, string complemento)
        {
            //CRIANDO OBJETO DE USUARIO
            Hospital hospital = new Hospital();
            Telephone telephone = new Telephone();

            //ATRIBUINDO O VALORES            
            hospital.Foto = foto;
            hospital.Email = email;
            hospital.Telephones.Numero = telefoneNumero;
            hospital.Endereco.Numero = enderecoNumero;
            hospital.Endereco.Cep = cep;
            hospital.Endereco.Estado = estado;
            hospital.Endereco.Cidade = cidade;
            hospital.Endereco.Bairro = bairro;
            hospital.Endereco.Rua = rua;
            hospital.Endereco.Complemento = complemento;

            //CHAMA MÉTODO DO MODELO
            string res = hospital.Editar();
            TempData["Msg"] = res;
            // COMPARA SE ESTA CORRETO E APRESENTA A MENSAGEM DE CONFIRMAÇÃO
            if (res == "Salvo com sucesso!")
                //RETORNA PARA A VIEW 
                return RedirectToAction("Change_profile");
            else
                return View();
        }

        //----------------------------------------------------MÉDICOS-----------------------------------------------//


        //----------------------------------------MÉTODO CADASTRAR----------------------------------------//
        public ActionResult Register_doctor()
        {
            return View();
        }

        //----------------------------------------MÉTODO CADASTRAR MÉDICOS----------------------------------------//
        [HttpPost]
        public ActionResult Register_doctor(string id, string cpf, string nome, string sobrenome, string datanascimento,
                                            string sexo, string email, string senha, string nivel_acesso, string area_atuacao,
                                            string especialidade_descricao, string telefoneNumero, string telefoneTipo,
                                            string estado, string cidade, string bairro, string cep, string rua, string enderecoNumero,
                                            string complemento, string foto)
        {
            Doctor doctor = new Doctor();

            doctor.Crm = id;
            doctor.Cpf = cpf;
            doctor.Nome = nome;
            doctor.Sobrenome = sobrenome;
            string[] valoresData = datanascimento.Split('/');
            doctor.Data_nascimento = new DateTime(int.Parse(valoresData[2]),int.Parse(valoresData[1]),int.Parse(valoresData[0]));
            doctor.Sexo = sexo;
            doctor.Email = email;
            doctor.Senha = senha;
            doctor.Nivel_acesso = nivel_acesso;
            //doctor.Area_atuacao = area_atuacao;
            //doctor.Especialidade.Descricao = especialidade_descricao;
            //doctor.Telefone.Numero = telefoneNumero;
            //doctor.Telefone.Tipo = telefoneTipo;
            doctor.Endereco.Estado = estado;
            doctor.Endereco.Cidade = cidade;
            doctor.Endereco.Cep = cep;
            doctor.Endereco.Bairro = bairro;
            doctor.Endereco.Rua = rua;
            doctor.Endereco.Numero = enderecoNumero;
            doctor.Endereco.Complemento = complemento;

            try
            {
                HttpPostedFileBase arqPostado = Request.Files[0];

                int tamImagem = arqPostado.ContentLength; //pega o tamanho
                string tipoArq = arqPostado.ContentType; //pega o tipo

                if (tipoArq.IndexOf("jpeg") > 0 || tipoArq.IndexOf("png") > 0)
                {
                    byte[] imgBytes = new byte[tamImagem];
                    arqPostado.InputStream.Read(imgBytes, 0, tamImagem);

                    doctor.Foto = imgBytes;
                }
            }

            catch (Exception erro)
            {
                string msgErro = erro.Message;
            }

            doctor.Register();
            
            return RedirectToAction("List_doctors");
        }


        //---------------------------------METODO EXCLUIR MÉDICOS-----------------------------------//
        public ActionResult DeleteMedico(string id)
        {
            // Objeto do usuario
            Doctor medico = new Doctor();
            // Atribuindo os valores
            medico.Id = id;
            // Chama método do controlador
            medico.Remover();
            // Mensagem de confirmação
            return RedirectToAction("List_doctors");
        }

        // -------------------------------- LISTAR MÉDICOS ------------------------------------//
        public ActionResult List_doctors()
        {
            Hospital hospital = (Hospital)Session["usuario"];

            return View(Doctor.List_doctors(hospital.Cnes));
        }

        //-------------------------------MÉTODO PERFIL-----------------------------//
        public ActionResult PerfilMedico(string id)
        {
            return View();
        }

        //-------------------------------------------------------Recepcionista--------------------------------------------------------//


        //----------------------------------------MÉTODO CADASTRAR----------------------------------------//
        public ActionResult Register_receptionist()
        {
            return View();
        }

        //----------------------------------------MÉTODO CADASTRAR Recepcionista----------------------------------------//
        [HttpPost]
        public ActionResult Register_receptionist(string id, string cpf, string nome, string sobrenome, string datanascimento,
                                                  string sexo, string email, string nivel_acesso, string senha, string telefoneNumero,
                                                  string telefoneTipo, string estado, string cidade, string bairro, string cep,
                                                  string rua, string enderecoNumero, string complemento, string foto)
        {
            //CRIANDO OBJETO DE USUARIO
            Receptionist recepcionist = new Receptionist();
            Telephone telephone = new Telephone();

            //ATRIBUINDO OS VALORES 
            recepcionist.Id = id;
            recepcionist.Cpf = cpf;
            recepcionist.Nome = nome;
            recepcionist.Sobrenome = sobrenome;
            string[] valoresData = datanascimento.Split('/');
            recepcionist.Data_nascimento = new DateTime(int.Parse(valoresData[2]), int.Parse(valoresData[1]), int.Parse(valoresData[0]));
            recepcionist.Sexo = sexo;
            recepcionist.Email = email;
            recepcionist.Senha = senha;
            recepcionist.Nivel_acesso = nivel_acesso;
            recepcionist.Telefone.Numero = telefoneNumero;
            recepcionist.Telefone.Tipo = telefoneTipo;
            recepcionist.Endereco.Estado = estado;
            recepcionist.Endereco.Cidade = cidade;
            recepcionist.Endereco.Cep = cep;
            recepcionist.Endereco.Bairro = bairro;
            recepcionist.Endereco.Rua = rua;
            recepcionist.Endereco.Numero = enderecoNumero;
            recepcionist.Endereco.Complemento = complemento;

            try
            {
                HttpPostedFileBase arqPostado = Request.Files[0];

                int tamImagem = arqPostado.ContentLength; //pega o tamanho
                string tipoArq = arqPostado.ContentType; //pega o tipo

                if (tipoArq.IndexOf("jpeg") > 0 || tipoArq.IndexOf("png") > 0)
                {
                    byte[] imgBytes = new byte[tamImagem];
                    arqPostado.InputStream.Read(imgBytes, 0, tamImagem);

                    recepcionist.Foto = imgBytes;
                }
            }

            catch (Exception erro)
            {
                string msgErro = erro.Message;
            }

            recepcionist.Register();

            return RedirectToAction("List_receptionists");
        }

        //---------------------------------METODO EXCLUIR RECEPCIONISTAS-----------------------------------//
        public ActionResult DeleteRecepcionista(string id)
        {
            // Objeto do usuario
            Receptionist Recepcionista = new Receptionist();
            // Atribuindo os valores
            Recepcionista.Id = id;
            // Chama método do controlador
            Recepcionista.Remover();
            // Mensagem de confirmação
            return RedirectToAction("List_receptionists");
        }

        // -------------------------------- LISTAR RECEPCIONISTAS ------------------------------------//
        public ActionResult List_receptionists()
        {
            Hospital hospital = (Hospital)Session["usuario"];

            return View(Receptionist.List_receptionists(hospital.Cnes));
        }

        public ActionResult Perfil(string id)
        {
            ViewBag.PessoaDoPerfil = id;
            return View();
        }

        public ActionResult TrocarSenha()
        {
            return View();
        }

        public ActionResult Sair() // Apaga os cookies e volta para a tela de login
        {
            int aux = Request.Cookies.Count; // Para evitar loop :P
            string[] nomes = Request.Cookies.AllKeys; // Para chamar um cookie específico
            for (int i = 0; i < aux; i++)
            {
                HttpCookie myCookie = new HttpCookie(nomes[i]);
                myCookie.Expires = DateTime.Now.AddDays(-1d);
                Response.Cookies.Add(myCookie);
            }
            Request.Cookies.Clear(); // Limpa os cookies do servidor, mas não os da máquina do cliente

            return RedirectToAction("Login", "Home");
        }
    }
}