﻿using SIS.Models;
using SIS.Models.Users;
using SIS.Models.Users.Agenda;
using SIS.ViewModel;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Web;
using System.Web.Mvc;
using System.Web.Services;

namespace SIS.Controllers
{
    [HandleError]
    public class HomeController : Controller
    {
        #region account_recovery

        #region forgot_password
        // --Ação para retornar a primeira tela do processo de recuperação de conta do usuário:
        // nessa tela será feita a requisição do id do usuário que busca recuperar sua conta--
        public ActionResult Forgot_password()
        {
            return View();
        }

        // --Ação desenvolvida para validar a existência do usuário que possui o id informado e passar
        // para a próxima etapa de recuperação de conta caso a existência do usuário tenha sido confirmada--
        [HttpPost]
        public ActionResult Forgot_password(string username)
        {
            /** DESENVOLVIMENTO DO PROCESSO **/
            // Procurando usuário do tipo pessoa que possua as especificações informadas
            Person person = Person.GetContactData(username);

            // Se o usuário procurado foi encontrado entre as pessoas físicas
            if (person != null)
            {
                // Instanciando a classe 'UserViewModel', a qual será responsável por armazenar e camuflar
                // os dados de contato do usuário validado
                UserViewModel contactInformation = new UserViewModel(username, person.Email, person.ListaTelefone);
                contactInformation.HideValues();

                return View("Forgot_password_options", contactInformation);
            }
            else
            {
                // Procurando usuário do tipo hospital que possua as especificações informadas
                Hospital hospital = Hospital.GetContactInfo(username);

                // Se o usuário procurado foi encontrado entre as pessoas jurídicas
                if (hospital != null)
                {
                    // Instanciando a classe 'UserViewModel', a qual será responsável por armazenar e camuflar
                    // os dados de contato do usuário validado
                    UserViewModel contactInformation = new UserViewModel(username, hospital.Email, null);
                    contactInformation.HideValues();

                    return View("Forgot_password_options", contactInformation);
                }
                // Caso o usuário procurado não foi encontrado entre as pessoas físicas e jurídicas
                else
                {
                    ViewBag.Message = "Nenhum usuário foi encontrado com o código de identificação especificado.";
                    return View();
                }
            }
        }
        #endregion

        #region forgot_password_options
        // --Ação para retornar a segunda tela do processo de recuperação de conta do usuário:
        // nessa tela será disposta as opções que o usuário possui para receber o seu código de verificação de identidade--
        public ActionResult Forgot_password_options()
        {
            return View();
        }

        // --Ação desenvolvida para enviar o código de verificação de identidade ao endereço escolhido pelo
        // usuário caso as informações coletadas sejam válidas--
        [HttpPost]
        public ActionResult Forgot_password_options(string username, string selectedContactOption, string informedData)
        {
            /** DECLARAÇÃO DE VARIÁVEL **/
            string typeData = null; // variável declarada para armazenar um dado que irá manipular a informação 
                                    // que será disposta na próxima View

            /** DESENVOLVIMENTO DO PROCESSO **/
            // Procurando usuário do tipo pessoa que possua as especificações informadas
            Person person = Person.GetContactData(username);

            // Se o usuário procurado foi encontrado entre as pessoas físicas
            if (person != null)
            {
                // Se a opção de contato selecionada for igual a 0 e o dado inserido pelo usuário condizer com o
                // e-mail do mesmo, envie o código de verificação de identidade para o endereço de e-mail desejado
                if (selectedContactOption.Equals("0"))
                {
                    if (informedData.Equals(person.Email))
                    {
                        string code = Person.GetCode(username);
                        Email.Send_email(person.Email, username, code);
                    }

                    typeData = "Email";
                }
                else
                {
                    int counter = 1;
                    foreach (Telephone item in person.ListaTelefone)
                    {
                        if (selectedContactOption.Equals(counter.ToString()))
                        {
                            if (informedData.Equals(item.Numero.Substring(item.Numero.Length - 4)))
                            {
                                string code = Person.GetCode(username);
                                Telephone.SendViaNett(item.Numero, code);
                            }

                            typeData = "Telephone";
                        }
                        counter++;
                    }
                }
            }
            else
            {
                Hospital hospital = Hospital.GetContactInfo(username);

                if (informedData.Equals(hospital.Email))
                {
                    string code = Hospital.GetCode(username);
                    Email.Send_email(hospital.Email, username, code);
                }

                typeData = "Email";
            }

            ViewBag.Username = username;
            ViewBag.TypeData = typeData;
            ViewBag.InformedData = informedData;
            return View("Verify_identity");
        }
        #endregion

        #region verify_identity
        public ActionResult Verify_identity()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Verify_identity(string username, string informedCode, string typeData, string informedData)
        {
            /** DECLARAÇÃO DE VARIÁVEL **/
            bool[,] result = null;

            /** DESENVOLVIMENTO DO PROCESSO **/
            // Procurando usuário do tipo pessoa que possua as especificações informadas
            bool existencePerson = Person.CheckExistenceUser(username);

            // Se o usuário procurado foi encontrado entre as pessoas físicas
            if (existencePerson == true)
            {
                result = Person.ValidateCode(username, informedCode);
            }
            // Caso contrário ele está entre as pessoas jurídicas
            else
            {
                result = Hospital.ValidateCode(username, informedCode);
            }

            if (result[0, 0] == false)
            {
                ViewBag.Message = "Sua sessão expirou. Reinicie o processo.";
                ViewBag.Username = username;
                ViewBag.TypeData = typeData;
                ViewBag.InformedData = informedData;
                return View();
            }
            else
            {
                if (result[0, 1] == false)
                {
                    ViewBag.Message = "Este código não funcionou. Verifique o código e tente novamente.";
                    ViewBag.Username = username;
                    ViewBag.TypeData = typeData;
                    ViewBag.InformedData = informedData;
                    return View();
                }
                else
                {
                    ViewBag.Username = username;
                    return View("Reset_password");
                }
            }
        }
        #endregion

        #region reset_password
        //--
        public ActionResult Reset_password()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Reset_password(string username, string newPassword)
        {
            bool existencePerson = Person.CheckExistenceUser(username);

            if (existencePerson == true)
            {
                Person person = new Person
                {
                    Id = username
                };
                person.ChangePassword(newPassword);
            }
            else
            {
                Hospital hospital = new Hospital
                {
                    Cnes = username
                };
                hospital.ChangePassword(newPassword);
            }

            return View("Recovered_account");
        }
        #endregion

        #region recovered_account
        public ActionResult Recovered_account()
        {
            return View();
        }
        #endregion

        #endregion

        #region user_authentication
        #region log_in
        public ActionResult Login()
        {
            // Caso o usuário selecionou a opção "Mantenha-me conectado", insira os dados do mesmo nos campos dispostos para efetuação do 'Login'
            HttpCookie cookie = Request.Cookies["continuar_conectado"];
            if(cookie != null)
            {
                string user_name = Request.Cookies["continuar_conectado"]["user_name"];
                string user_password = Request.Cookies["continuar_conectado"]["user_password"];

                return Login(user_name, user_password, "on");
            }
            else
            {
                return View();
            }
        }

        // --Método de autenticação de usuário--
        [HttpPost]
        public ActionResult Login(string username, string pass, string continuarConectado)
        {
            object result;

            // Procurando usuário do tipo pessoa que possua as especificações informadas
            result = Person.Login(username, pass);

            if (result != null) // se o usuário foi encontrado
            {
                Session.Add("usuario", result);
                string teste = result.GetType().ToString();
                switch (teste) // obtendo a classe do objeto
                {
                    case "SIS.Models.Users.Administrator":
                        this.CriaCookie(((Administrator)result).Id, ((Administrator)result).Senha, continuarConectado);
                        return RedirectToAction("Administrator_profile", "Administrator");

                    case "SIS.Models.Users.Doctor":
                        this.CriaCookie(((Doctor)result).Id, ((Doctor)result).Senha, continuarConectado);
                        return RedirectToAction("Doctor_profile", "Doctor");

                    case "SIS.Models.Users.Receptionist":
                        this.CriaCookie(((Receptionist)result).Id, ((Receptionist)result).Senha, continuarConectado);
                        return RedirectToAction("Receptionist_profile", "Receptionist");

                    case "SIS.Models.Users.Patient":
                        CriaCookie(((Patient)result).Id, ((Patient)result).Senha, continuarConectado);
                        return RedirectToAction("Patient_profile", "Patient");

                    default:
                        return View();
                }
            }
            else // caso o usuário não foi encontrado entre as pessoas físicas
            {
                // Procurando usuário do tipo hospital que possua as especificações informadas
                result = Hospital.Login(username, pass);

                if (result != null) // se o usuário foi encontrado
                {
                    Session.Add("usuario", result);
                    CriaCookie(((Hospital)result).Cnes, ((Hospital)result).Senha, continuarConectado);
                    return RedirectToAction("Hospital_profile", "Hospital");
                }
                else // caso nenhum tipo de usuário foi encontrado com as especificações informadas
                {
                    ViewBag.Message = "Código de identificação e/ou senha incorreto(s).";
                    return View();
                }
            }
        }
        #endregion

        #region keep_user_logged_in
        private void CriaCookie(string id, string senha, string continuarConectado)
        {
            if (continuarConectado != null) // SE O USUÁRIO QUISER CONTINUAR CONECTADO DEPOIS DE SAIR, ESSE COOKIE É GERADO PARA LEMBRAR QUE TIPO DE USUÁRIO LOGOU
            {
                // Verificando se já existe o cookie na máquina do usuário
                HttpCookie cookie_continuar_conectado = Request.Cookies["continuar_conectado"];
                if (cookie_continuar_conectado == null)
                {
                    // Criando a Instância do cookie
                    cookie_continuar_conectado = new HttpCookie("continuar_conectado");
                    //Adicionando a propriedade "user_name" no cookie
                    cookie_continuar_conectado.Values.Add("user_name", id);
                    //Adicionando a propriedade "user_password" no cookie
                    cookie_continuar_conectado.Values.Add("user_password", senha);
                    //colocando o cookie para expirar em 365 dias
                    cookie_continuar_conectado.Expires = DateTime.Now.AddDays(365);
                    // Definindo a segurança do nosso cookie
                    cookie_continuar_conectado.HttpOnly = true;
                    // Registrando cookie
                    Response.AppendCookie(cookie_continuar_conectado);
                }
                else
                {
                    cookie_continuar_conectado.Values.Set("user_name", id);
                    cookie_continuar_conectado.Values.Set("user_password", senha);
                }
            }
            else
            {
                // Verificando se já existe o cookie na máquina do usuário
                HttpCookie cookie_continuar_conectado = Request.Cookies["continuar_conectado"];
                if (cookie_continuar_conectado != null)
                {
                    Response.Cookies["continuar_conectado"].Expires = DateTime.Now.AddDays(-1);
                }
            }
        }
        #endregion

        #region log_out
        // --Método desenvolvido para realizar o 'log out' do usuário autenticado--
        public ActionResult Sair()
        {
            int aux = Request.Cookies.Count; // Para evitar loop :P
            string[] nomes = Request.Cookies.AllKeys; // Para chamar um cookie específico
            for (int i = 0; i < aux; i++)
            {
                HttpCookie myCookie = new HttpCookie(nomes[i])
                {
                    Expires = DateTime.Now.AddDays(-1)
                };
                Response.Cookies.Add(myCookie);
            }
            Request.Cookies.Clear(); // Limpa os cookies do servidor, mas não os da máquina do cliente

            return RedirectToAction("Login");
        }
        #endregion

        #region without_permission
        public ActionResult SemPermissao()
        {
            Session.Clear();
            return View("Login");
        }
        #endregion
        #endregion

        #region auxiliary_methods
        public string CarregaFoto()
        {
            try
            {
                Person usuario = new Receptionist();
                usuario.Id = Person.GetIdByName(Request.Cookies.Get("nome").Value);

                if (usuario.Id != null && usuario.Foto != null)
                {
                    Bitmap imagem = new Bitmap(Image.FromStream(new MemoryStream(usuario.Foto)));
                    int dimensaoMaior = Math.Max(imagem.Width, imagem.Height);
                    Bitmap imagemRedimencionada = ResizeImage(imagem, dimensaoMaior, dimensaoMaior); // Redimensionamento da imagem para ser sempre quadrada, para não sair toda cagada no layout da tela

                    MemoryStream ms = new MemoryStream();
                    imagemRedimencionada.Save(ms, ImageFormat.Png);

                    string foto = Convert.ToBase64String(ms.ToArray());
                    return string.Format("data:image/jpeg;base64, {0}", foto);
                }
                else
                {
                    Hospital hospital = new Hospital();
                    hospital.Cnes = Hospital.GetIdByCnes(Request.Cookies.Get("nome").Value);

                    if (hospital.Cnes != null && hospital.Foto != null)
                    {
                        Bitmap imagem = new Bitmap(Image.FromStream(new MemoryStream(hospital.Foto)));
                        int dimensaoMaior = Math.Max(imagem.Width, imagem.Height);
                        Bitmap imagemRedimencionada = ResizeImage(imagem, dimensaoMaior, dimensaoMaior); // Redimensionamento da imagem para ser sempre quadrada, para não sair toda cagada no layout da tela

                        MemoryStream ms = new MemoryStream();
                        imagemRedimencionada.Save(ms, ImageFormat.Png);

                        string foto = Convert.ToBase64String(ms.ToArray());
                        return string.Format("data:image/jpeg;base64, {0}", foto);
                    }
                    else
                        return "";
                }
            }
            catch (Exception)
            {
                return "";
            }
        }
        
        public static string CarregaFotoPerfil(string nomeUsuario)
        {
            try
            {
                Person usuario = new Receptionist();
                usuario.Id = Person.GetIdByName(nomeUsuario);

                Hospital hospital = new Hospital();
                hospital.Cnes = Hospital.GetIdByCnes(nomeUsuario);

                if (usuario.Id != null && usuario.Foto != null)
                {
                    Bitmap imagem = new Bitmap(Image.FromStream(new MemoryStream(usuario.Foto)));
                    int dimensaoMaior = Math.Max(imagem.Width, imagem.Height);
                    Bitmap imagemRedimencionada = ResizeImage(imagem, dimensaoMaior, dimensaoMaior); // Redimensionamento da imagem para ser sempre quadrada, para não sair toda cagada no layout da tela

                    MemoryStream ms = new MemoryStream();
                    imagemRedimencionada.Save(ms, ImageFormat.Png);

                    string foto = Convert.ToBase64String(ms.ToArray());
                    return string.Format("data:image/jpeg;base64, {0}", foto);
                }
                else if (hospital.Cnes != null && hospital.Foto != null)
                {
                    Bitmap imagem = new Bitmap(Image.FromStream(new MemoryStream(usuario.Foto)));
                    int dimensaoMaior = Math.Max(imagem.Width, imagem.Height);
                    Bitmap imagemRedimencionada = ResizeImage(imagem, dimensaoMaior, dimensaoMaior); // Redimensionamento da imagem para ser sempre quadrada, para não sair toda cagada no layout da tela

                    MemoryStream ms = new MemoryStream();
                    imagemRedimencionada.Save(ms, ImageFormat.Png);

                    string foto = Convert.ToBase64String(ms.ToArray());
                    return string.Format("data:image/jpeg;base64, {0}", foto);
                }
                else
                    return "";
            }
            catch (Exception erro)
            {
                string msgErro = erro.Message;
                return "";
            }
        }

        public string TipoUsuario(string nome) // Retorna se é Hospital ou Person
        {
            return SIS.Models.Users.Perfil.tipoUsuario(nome);
        }

        public string TipoUsuarioDetalhado(string nome) // Retorna se é Hospital, Medico, Paciente, Recepcionista ou Admin
        {
            return SIS.Models.Users.Perfil.tipoUsuarioDetalhado(Person.GetIdByName(nome));
        }

        //----------------Fotos Hospitais---------------//
        [WebMethod]
        public static string CarregaFotoHospital(string cnes)
        {
            Hospital hospital = new Hospital();
            hospital.Cnes = cnes;
            if (hospital.Cnes != null && hospital.Foto != null)
            {
                string foto = Convert.ToBase64String(hospital.Foto); // Conversão de bytes para string
                return string.Format("data:image/jpeg;base64, {0}", foto);
            }
            else
                return "";
        }

        private static Bitmap ResizeImage(Image image, int width, int height)
        {
            var destRect = new Rectangle(0, 0, width, height);
            var destImage = new Bitmap(width, height);

            destImage.SetResolution(image.HorizontalResolution, image.VerticalResolution);

            using (var graphics = Graphics.FromImage(destImage))
            {
                graphics.CompositingMode = CompositingMode.SourceCopy;
                graphics.CompositingQuality = CompositingQuality.HighQuality;
                graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphics.SmoothingMode = SmoothingMode.HighQuality;
                graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;

                using (var wrapMode = new ImageAttributes())
                {
                    wrapMode.SetWrapMode(WrapMode.TileFlipXY);
                    graphics.DrawImage(image, destRect, 0, 0, image.Width, image.Height, GraphicsUnit.Pixel, wrapMode);
                }
            }

            return destImage;
        }
        #endregion
    }
}