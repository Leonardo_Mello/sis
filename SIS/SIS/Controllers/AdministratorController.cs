﻿using SIS.Models.Users;
using System;
using System.Web;
using System.Web.Mvc;

namespace SIS.Controllers
{
    public class AdministratorController : Controller
    {
        public ActionResult Administrator_profile()
        {
            return View("Administrator_profile", (Administrator)Session["usuario"]);
        }

        public ActionResult Change_profile(string id)
        {
            Person pessoa = new Person();
            return View();
        }

        [HttpPost]
        public ActionResult Change_profile(Administrator administrator)
        {
            return View();
        }


        public ActionResult Edit_hospital(string cnes)
        {
            //UTILIZAR O OBJETO USUARIO PARA CHAMAR O MÉTODO BUSCA USUARIO DO MODELO
            Hospital hospital = Hospital.GetInfo(cnes);

            //COMPARAR PARA VER SE ENCONTRAR UM OBJETO IGUAL
            if (hospital == null)
            {
                //MENSAGEM DE CONFIRMAÇÃO
                TempData["Msg"] = "Erro ao buscar o hospital especificado!";
                //RETORNA PARA A VIEW 
                return RedirectToAction("List_hospitals");
            }

            return View(hospital);
        }

        public ActionResult Change_hospital(string cnes)
        {
            //UTILIZAR O OBJETO USUARIO PARA CHAMAR O MÉTODO BUSCA USUARIO DO MODELO
            Hospital hospital = Hospital.GetInfo(cnes);

            //COMPARAR PARA VER SE ENCONTRAR UM OBJETO IGUAL
            if (hospital == null)
            {
                //MENSAGEM DE CONFIRMAÇÃO
                TempData["Msg"] = "Erro ao buscar hospital!";
                //RETORNA PARA A VIEW 
                return RedirectToAction("List_hospitals");
            }
            return View(hospital);
        }

        [HttpPost]
        //CONSTRUTOR COM OS ATRIBUTOS
        public ActionResult Change_hospital(string foto, string email, string telefoneNumero, string estado, string cidade,
                                            string bairro, string cep, string rua, string enderecoNumero, string complemento)
        {
            //CRIANDO OBJETO DE USUARIO
            Hospital hospital = new Hospital();

            //ATRIBUINDO O VALORES 
            //hospital.Foto = (byte[])foto;
            hospital.Email = email;
            hospital.Telephones.Numero = telefoneNumero;
            hospital.Endereco.Estado = estado;
            hospital.Endereco.Cidade = cidade;
            hospital.Endereco.Cep = cep;
            hospital.Endereco.Bairro = bairro;
            hospital.Endereco.Rua = rua;
            hospital.Endereco.Numero = enderecoNumero;
            hospital.Endereco.Complemento = complemento;

            try
            {
                HttpPostedFileBase arqPostado = Request.Files[foto];

                int tamImagem = arqPostado.ContentLength; //pega o tamanho
                string tipoArq = arqPostado.ContentType; //pega o tipo

                if (tipoArq.IndexOf("jpeg") > 0 || tipoArq.IndexOf("png") > 0)
                {
                    byte[] imgBytes = new byte[tamImagem];
                    arqPostado.InputStream.Read(imgBytes, 0, tamImagem);

                    hospital.Foto = imgBytes;
                }
            }
            catch (Exception erro)
            {
                string msgErro = erro.Message;
            }

            //CHAMA MÉTODO DO MODELO
            string res = hospital.Editar();
            TempData["Msg"] = res;
            // COMPARA SE ESTA CORRETO E APRESENTA A MENSAGEM DE CONFIRMAÇÃO
            if (res == "Salvo com sucesso!")
                //RETORNA PARA A VIEW 
                return RedirectToAction("List_hospitals");
            else
                return View();
        }


        public ActionResult Administrador()
        {
            return View();
        }

        // --FUNCIONANDO--
        public ActionResult List_hospitals()
        {
            return View(Hospital.List_hospitals());
        }

        public ActionResult Register_hospital()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Register_hospital(string cnes, string cnpj, string nome, string razaosocial, string foto, string email, string senha,
                                             string telefoneNumero, string telefoneTipo, string estado, string cidade, string bairro, string cep,
                                             string rua, string enderecoNumero, string complemento)
        {

            //CRIANDO OBJETO DE USUARIO
            Hospital hospital = new Hospital();

            //ATRIBUINDO OS VALORES 
            hospital.Cnes = cnes;
            hospital.Cnpj = cnpj;
            hospital.Nome_fantasia = nome;
            hospital.Razao_social = razaosocial;
            hospital.Email = email;
            hospital.Senha = senha;
            hospital.Telephones.Numero = telefoneNumero;
            hospital.Telephones.Tipo = telefoneTipo;
            hospital.Endereco.Estado = estado;
            hospital.Endereco.Cidade = cidade;
            hospital.Endereco.Cep = cep;
            hospital.Endereco.Bairro = bairro;
            hospital.Endereco.Rua = rua;
            hospital.Endereco.Numero = enderecoNumero;
            hospital.Endereco.Complemento = complemento;

            try
            {
                HttpPostedFileBase arqPostado = Request.Files[0];

                int tamImagem = arqPostado.ContentLength; //pega o tamanho
                string tipoArq = arqPostado.ContentType; //pega o tipo

                if (tipoArq.IndexOf("jpeg") > 0 || tipoArq.IndexOf("png") > 0)
                {
                    byte[] imgBytes = new byte[tamImagem];
                    arqPostado.InputStream.Read(imgBytes, 0, tamImagem);

                    hospital.Foto = imgBytes;
                }
            }
            catch (Exception erro)
            {
                string msgErro = erro.Message;
            }

            // CHAMA MÉTODO DO CONTROLLER
            TempData["Msg"] = hospital.Register();

            //MENSAGEM DE CONFIRMAÇÃO
            return RedirectToAction ("List_hospitals");
        }

        public ActionResult DeleteHospital(string id)
        {
            //CRIANDO OBJETO DE USUARIO
            Hospital hospital = new Hospital();
            //ATRIBUINDO O VALORES 
            hospital.Cnes = id;
            //CHAMA MÉTODO DO CONTROLLER
            hospital.Remover();
            //MENSAGEM DE CONFIRMAÇÃO
            return RedirectToAction("List_hospitals");
        }

        public ActionResult Perfil(string id)
        {
            ViewBag.PessoaDoPerfil = id;
            return View();
        }

        public ActionResult TrocarSenha()
        {
            return View();
        }

        [HttpPost]
        public ActionResult TrocarSenha(string id)
        {
            return View();
        }

        public ActionResult Sair() // Apaga os cookies e volta para a tela de login
        {
            int aux = Request.Cookies.Count; // Para evitar loop :P
            string[] nomes = Request.Cookies.AllKeys; // Para chamar um cookie específico
            for (int i = 0; i < aux; i++)
            {
                HttpCookie myCookie = new HttpCookie(nomes[i]);
                myCookie.Expires = DateTime.Now.AddDays(-1d);
                Response.Cookies.Add(myCookie);
            }
            Request.Cookies.Clear(); // Limpa os cookies do servidor, mas não os da máquina do cliente

            return RedirectToAction("Login", "Home");
        }
    }
}