﻿using SIS.Models;
using SIS.Models.Users;
using SIS.Models.Users.Agenda;
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;

namespace SIS.Controllers
{
    public class DoctorController : Controller
    {
        #region profile
        public ActionResult Doctor_profile()
        {
            return View("Doctor_profile", (Doctor)Session["usuario"]);
        }
        #endregion

        ////---------------------------------METODO ALTERAR-----------------------------------//
        //public ActionResult AlterarMedico(string id)
        //{
        //    //UTILIZAR O OBJETO USUARIO PARA CHAMAR O MÉTODO BUSCA USUARIO DO MODELO
        //    List<Doctor> doutor = Doctor.ListaMedico(id);
        //    //COMPARAR PARA VER SE ENCONTRAR UM OBJETO IGUAL
        //    if (doutor == null)
        //    {
        //        //MENSAGEM DE CONFIRMAÇÃO
        //        TempData["Msg"] = "Erro ao buscar Médico!";
        //        //RETORNA PARA A VIEW 
        //        return RedirectToAction("ListarMedicos");
        //    }
        //    return View(doutor);
        //}

        //---------------------------------METODO ALTERAR-----------------------------------//
        [HttpPost]
        // CONSTRUTOR COM OS ATRIBUTOS
        public ActionResult AlterarMedico(string nome, string sobrenome, string sexo, string email,
                                          string area_atuacao, DateTime data_nascimento, byte[] foto, string telefoneNumero)
        {
            //CRIANDO OBJETO DE USUARIO
            Doctor doutor = new Doctor();
            Telephone telephone = new Telephone();

            //ATRIBUINDO O VALORES 
            doutor.Nome = nome;
            doutor.Sobrenome = sobrenome;
            doutor.Sexo = sexo;
            doutor.Email = email;
            //doutor.Area_atuacao = area_atuacao;
            doutor.Data_nascimento = data_nascimento;
            doutor.Foto = foto;
            //telephone.Numero = telefoneNumero;
            
            //CHAMA MÉTODO DO MODELO
            string res = doutor.Editar();
            TempData["Msg"] = res;
            // COMPARA SE ESTA CORRETO E APRESENTA A MENSAGEM DE CONFIRMAÇÃO
            if (res == "Salvo com sucesso!")
                //RETORNA PARA A VIEW 
                return RedirectToAction("ListarMedicos");
            else
                return View();
        }

        //---------------------------------METODO LISTAR-----------------------------------//
        public ActionResult List_patients()
        {
            // ATRIBUI A VIEW A LISTAR USUARIOS E APRESENTA TODOS USUARIOS DA LISTA
            return View(Patient.List_patients());
        }

        //-----------------------MÉTODO ADICIONAR DOCUMENTAÇÃO PACIENTE APOS CONSULTA MÉDICA---------------------//
        public ActionResult AdicionarRelatorio(string paciente)
        {
            return View();
        }


        public ActionResult Perfil(string id)
        {
            ViewBag.PessoaDoPerfil = id;
            return View();
        }
        
        public ActionResult TrocarSenha()
        {
            return View();
        }

        public ActionResult Sair() // Apaga os cookies e volta para a tela de login
        {
            int aux = Request.Cookies.Count; // Para evitar loop :P
            string[] nomes = Request.Cookies.AllKeys; // Para chamar um cookie específico
            for (int i = 0; i < aux; i++)
            {
                HttpCookie myCookie = new HttpCookie(nomes[i]);
                myCookie.Expires = DateTime.Now.AddDays(-1d);
                Response.Cookies.Add(myCookie);
            }
            Request.Cookies.Clear(); // Limpa os cookies do servidor, mas não os da máquina do cliente

            return RedirectToAction("Login", "Home");
        }
        
        public ActionResult List_Appointment()
        {
            return View(Agenda.listaConsultas());
        }

        public ActionResult List_exames()
        {
            return View(Agenda.listaExames());
        }

        public ActionResult Remove_Appointment(string id)
        {
            Appointment.CancelarConsulta(id);

            return RedirectToAction("List_Appointment");
        }

        public ActionResult Continue_Appointment(string id)
        {
            Session.Add("consultaAtual", id);
            return View(Agenda.VisualizarConsulta(id));
        }

        [HttpPost]
        public ActionResult Continue_Appointment(string observacao, string medicamento, string quantidade, string duracao, string descricao)
        {
            string idConsulta = Session["consultaAtual"].ToString();

            PrescricaoMedica prescricao = null;
            Exame exame = null;

            if(medicamento != null && quantidade != null && duracao != null)
            {
                prescricao = new PrescricaoMedica();
                prescricao.Consulta = idConsulta;
                prescricao.Medicamento = medicamento;
                prescricao.Quantidade = quantidade;
                prescricao.Duracao = duracao;
            }

            if(descricao != null)
            {
                exame = new Exame();
                exame.Descricao = descricao;
            }

            Appointment.ConfirmaConsulta(idConsulta, observacao, prescricao, exame);

            return RedirectToAction("List_Appointment");
        }


        public ActionResult Remove_Exame(string id)
        {
            Exame.CancelarExame(id);

            return RedirectToAction("List_exames");
        }


        public ActionResult Finish_Exame(string id)
        {
            Session.Add("exameAtual", id);
            return View(Agenda.VisualizarExame(id));
        }

        [HttpPost]
        public ActionResult Finish_Exame(string observacao, string foto)
        {
            string idExame = Session["exameAtual"].ToString();
            byte[] documento_resultado = null;

            try
            {
                HttpPostedFileBase arqPostado = Request.Files[0];

                int tamImagem = arqPostado.ContentLength; //pega o tamanho
                string tipoArq = arqPostado.ContentType; //pega o tipo

                if (tipoArq.IndexOf("jpeg") > 0 || tipoArq.IndexOf("png") > 0)
                {
                    byte[] imgBytes = new byte[tamImagem];
                    arqPostado.InputStream.Read(imgBytes, 0, tamImagem);

                    documento_resultado = imgBytes;
                }
            }

            catch (Exception erro)
            {
                string msgErro = erro.Message;
            }

            Exame.ConcluirExame(idExame, documento_resultado, observacao);
            return RedirectToAction("List_exames");
        }

        public ActionResult View_Exame(string id)
        {
            return View(Agenda.VisualizarExame(id));
        }
    }
}