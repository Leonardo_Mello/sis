﻿using SIS.Models;
using SIS.Models.Users;
using SIS.Models.Users.Agenda;
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;

namespace SIS.Controllers
{
    public class ReceptionistController : Controller
    {
        // GET: Receptionist

        #region profile
        public ActionResult Receptionist_profile()
        {
            return View("Receptionist_profile", (Receptionist)Session["usuario"]);
        }

        public ActionResult ChangeProfile()
        {
            return View();
        }
        #endregion

        #region patient_accounts
        public ActionResult Register_patient()
        {
            /** DECLARAÇÃO E INICIALIZAÇÃO DAS VARIÁVEIS LOCAIS **/
            // --Declaração das variáveis locais--
            List<Type_disability> types_disability = new List<Type_disability>();
            List<Type_allergy> types_allergy = new List<Type_allergy>();
            List<Allergy> allergies = new List<Allergy>();

            // --Inicialização dos valores das variáveis declaradas--
            types_disability = Type_disability.List_types_disability();
            types_allergy = Type_allergy.List_allergy_types();
            allergies = Allergy.List_allergies();

            /** DESENVOLVIMENTO DO PROCESSO **/
            // --Passagem dos valores obtidos a ViewBags que armazenarão os mesmos para que mais tarde eles possam ser 
            // disponibilizados para visualização ao usuário--
            ViewBag.list_types_disability = types_disability;
            ViewBag.list_allergy_types = types_allergy;
            ViewBag.list_allergies = allergies;

            return View();
        }

        [HttpPost]
        public ActionResult Register_patient(Patient patient)
        {
            //CRIANDO OBJETO DE USUARIO
            Patient Pessoa = new Patient();
            Telephone telephone = new Telephone();
            Disability deficiencia = new Disability();

            //ATRIBUINDO OS VALORES 
            //Pessoa.Id = id;
            //Pessoa.Cpf = cpf;
            //Pessoa.Nome = nome;
            //Pessoa.Sobrenome = sobrenome;
            //string[] valoresData = data_nascimento.Split('/');
            //Pessoa.Data_nascimento = new DateTime(int.Parse(valoresData[2]), int.Parse(valoresData[1]), int.Parse(valoresData[0]));
            //Pessoa.Sexo = sex;
            //Pessoa.Email = email;
            //Pessoa.Senha = senha;
            //Pessoa.Nivel_acesso = "Pessoa";
            //Pessoa.Nome_mae = nome_mae;
            //Pessoa.Nome_pai = nome_pai;
            ////Pessoa.Deficiencia.Descricao = descricaoDeficiencia;
            ////Pessoa.Telefone.Numero = telefoneNumero;
            ////Pessoa.Telefone.Tipo = telefoneTipo;           
            //Pessoa.Endereco.Estado = estado;
            //Pessoa.Endereco.Cidade = cidade;
            //Pessoa.Endereco.Cep = cep;
            //Pessoa.Endereco.Bairro = bairro;
            //Pessoa.Endereco.Rua = rua;
            //Pessoa.Endereco.Numero = enderecoNumero;
            //Pessoa.Endereco.Complemento = complemento;

            //try
            //{
            //    HttpPostedFileBase arqPostado = Request.Files[0];

            //    int tamImagem = arqPostado.ContentLength; //pega o tamanho
            //    string tipoArq = arqPostado.ContentType; //pega o tipo

            //    if (tipoArq.IndexOf("jpeg") > 0 || tipoArq.IndexOf("png") > 0)
            //    {
            //        byte[] imgBytes = new byte[tamImagem];
            //        arqPostado.InputStream.Read(imgBytes, 0, tamImagem);

            //        Pessoa.Foto = imgBytes;
            //    }
            //}

            //catch (Exception erro)
            //{
            //    string msgErro = erro.Message;
            //}

            //Pessoa.Register();

            return RedirectToAction("List_patients");
        }
        #endregion

        public ActionResult List_patients()
        {
            return View(Patient.List_patients());
        }
        
        public ActionResult Edit_patient(string rg)
        {
            return View(Patient.GetInfo(rg));
        }
        //public ActionResult AlterarRecepcionista(string id)
        //{
        //    //UTILIZAR O OBJETO USUARIO PARA CHAMAR O MÉTODO BUSCA USUARIO DO MODELO
        //    List<Receptionist> Recepcionista = Receptionist.ListaRecepcionista(id);
        //    //COMPARAR PARA VER SE ENCONTRAR UM OBJETO IGUAL
        //    if (Recepcionista == null)
        //    {
        //        //MENSAGEM DE CONFIRMAÇÃO
        //        TempData["Msg"] = "Erro ao buscar Recepcionista!";
        //        //RETORNA PARA A VIEW 
        //        return RedirectToAction("ListarRecepcionista");
        //    }
        //    return View(Recepcionista);
        //}
        
        [HttpPost]
        // CONSTRUTOR COM OS ATRIBUTOS
        public ActionResult AlterarRecepcionista(string nome, string sobrenome, string sexo, string email, DateTime data_nascimento,
                                                 byte[] foto, string telefoneNumero, string descricaoDeficiencia, string enderecoNumero, string cep, string estado,
                                                 string cidade, string bairro, string rua, string complemento)
        {
            //CRIANDO OBJETO DE USUARIO
            Receptionist recepcionista = new Receptionist();
            Telephone telephone = new Telephone();
            Disability deficiencia = new Disability();

            //ATRIBUINDO O VALORES 
            recepcionista.Nome = nome;
            recepcionista.Sobrenome = sobrenome;
            recepcionista.Sexo = sexo;
            recepcionista.Email = email;
            recepcionista.Data_nascimento = data_nascimento;
            recepcionista.Foto = foto;

            //telephone.Numero = telefoneNumero;
            //deficiencia.Descricao = descricaoDeficiencia;

            recepcionista.Endereco.Numero = enderecoNumero;
            recepcionista.Endereco.Cep = cep;
            recepcionista.Endereco.Estado = estado;
            recepcionista.Endereco.Cidade = cidade;
            recepcionista.Endereco.Bairro = bairro;
            recepcionista.Endereco.Rua = rua;
            recepcionista.Endereco.Complemento = complemento;

            //CHAMA MÉTODO DO MODELO
            string res = recepcionista.Editar();
            TempData["Msg"] = res;
            // COMPARA SE ESTA CORRETO E APRESENTA A MENSAGEM DE CONFIRMAÇÃO
            if (res == "Salvo com sucesso!")
                //RETORNA PARA A VIEW 
                return RedirectToAction("ListarRecepcionista");
            else
                return View();
        }
        
        

        #region medical_appointment
        public ActionResult Book_appointment()
        {

            return View();
        }

        public ActionResult List_medical_appointments()
        {
            return View();
        }

        public ActionResult VerConsulta()
        {
            return View();
        }

        public ActionResult AlterarConsulta()
        {
            return View();
        }

        public ActionResult ExcluirConsulta()
        {
            return View();
        }
        #endregion
        public ActionResult AgendamentoConsultas(string id)
        {
            return View();
        }
        
        public ActionResult TrocarSenha()
        {
            return View();
        }

        public ActionResult Sair() // Apaga os cookies e volta para a tela de login
        {
            int aux = Request.Cookies.Count; // Para evitar loop :P
            string[] nomes = Request.Cookies.AllKeys; // Para chamar um cookie específico
            for (int i = 0; i < aux; i++)
            {
                HttpCookie myCookie = new HttpCookie(nomes[i]);
                myCookie.Expires = DateTime.Now.AddDays(-1d);
                Response.Cookies.Add(myCookie);
            }
            Request.Cookies.Clear(); // Limpa os cookies do servidor, mas não os da máquina do cliente

            return RedirectToAction("Login", "Home");
        }

        public ActionResult Register_appointment()
        {
            return View(new Appointment());
        }

        [HttpPost]
        public ActionResult Register_appointment(string data, string duracao, string especialidade, string medico, string titulo, string comentario, string paciente, string hospital)
        {
            Appointment consulta = new Appointment();

            string[] dataFragmentada = data.Split('-');
            consulta.Data = new DateTime(int.Parse(dataFragmentada[0]), int.Parse(dataFragmentada[1]), int.Parse(dataFragmentada[2]));
            
            DateTime _duracao = consulta.Data;
            string[] _duracaoFragmentada = duracao.Split(':');
            _duracao.AddHours(int.Parse(_duracaoFragmentada[0]));
            _duracao.AddMinutes(int.Parse(_duracaoFragmentada[1]));
            _duracao.AddSeconds(int.Parse(_duracaoFragmentada[2]));
            consulta.Duracao = _duracao;

            consulta.Especialidade = especialidade;

            Patient _paciente = new Patient();
            _paciente.Id = paciente;
            consulta.Patient_doctor[0] = _paciente;


            Doctor _medico = new Doctor();
            _medico.Id = medico;
            consulta.Patient_doctor[1] = _medico;

            consulta.Titulo = titulo;
            consulta.Comentario = comentario;
            consulta.Estado = false;
            consulta.Hospital.Cnes = hospital;

            consulta.AgendaConsulta();

            return RedirectToAction("Receptionist_profile");
        }
    }
}