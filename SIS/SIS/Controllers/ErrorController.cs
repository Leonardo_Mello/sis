﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SIS.Controllers
{
    public class ErrorController : Controller
    {
        public ActionResult Erro404()
        {
            return View();
        }

        public ActionResult Erro500()
        {
            return View();
        }
    }
}