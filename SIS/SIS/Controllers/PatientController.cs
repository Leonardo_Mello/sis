﻿using SIS.Models;
using SIS.Models.Users;
using SIS.Models.Users.Agenda;
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;

namespace SIS.Controllers
{
    public class PatientController : Controller
    {
        #region profile

        public ActionResult Patient_profile()
        {
            return View("Patient_profile", (Patient)Session["usuario"]);
        }

        #endregion

        ////---------------------------------METODO ALTERAR-----------------------------------//
        //public ActionResult AlterarPessoa(string id)
        //{
        //    //UTILIZAR O OBJETO USUARIO PARA CHAMAR O MÉTODO BUSCA USUARIO DO MODELO
        //    List<Patient> Pessoa = Patient.ListaPessoa(id);
        //    //COMPARAR PARA VER SE ENCONTRAR UM OBJETO IGUAL
        //    if (Pessoa == null)
        //    {
        //        //MENSAGEM DE CONFIRMAÇÃO
        //        TempData["Msg"] = "Erro ao buscar Pessoa!";
        //        //RETORNA PARA A VIEW 
        //        return RedirectToAction("ListarPessoas");
        //    }
        //    return View();
        //}

        //---------------------------------METODO ALTERAR-----------------------------------//
        //[HttpPost]
        //// CONSTRUTOR COM OS ATRIBUTOS
        //public ActionResult AlterarPessoa(string nome, string sobrenome, DateTime data_nascimento,
        //                                    string sexo, byte[] foto, string email, string nome_mae,
        //                                    string nome_pai, string telefoneNumero, string enderecoNumero,
        //                                    string cep, string estado, string cidade, string bairro, string rua, string complemento)
        //{
        //    //CRIANDO OBJETO DE USUARIO
        //    Patient Pessoa = new Patient();
        //    Telephone telephone = new Telephone();

        //    //ATRIBUINDO O VALORES 
        //    Pessoa.Nome = nome;
        //    Pessoa.Sobrenome = sobrenome;
        //    Pessoa.Data_nascimento = data_nascimento;
        //    Pessoa.Sexo = sexo;
        //    Pessoa.Foto = foto;
        //    Pessoa.Email = email;
        //    Pessoa.Nome_mae = nome_mae;
        //    Pessoa.Nome_pai = nome_pai;

        //    //telephone.Numero = telefoneNumero;

        //    Pessoa.Endereco.Numero = enderecoNumero;
        //    Pessoa.Endereco.Cep = cep;
        //    Pessoa.Endereco.Estado = estado;
        //    Pessoa.Endereco.Cidade = cidade;
        //    Pessoa.Endereco.Bairro = bairro;
        //    Pessoa.Endereco.Rua = rua;
        //    Pessoa.Endereco.Complemento = complemento;

        //    //CHAMA MÉTODO DO MODELO
        //    string res = Pessoa.Editar();
        //    TempData["Msg"] = res;
        //    // COMPARA SE ESTA CORRETO E APRESENTA A MENSAGEM DE CONFIRMAÇÃO
        //    if (res == "Salvo com sucesso!")
        //        //RETORNA PARA A VIEW 
        //        return RedirectToAction("ListarPessoas");
        //    else
        //        return View();
        //}

        //public ActionResult TrocarSenha(string id)
        //{
        //    //return View();
        //    return View("Agenda",Hospital.ListarHospitais());
        //}

        public ActionResult List_appointment()
        {
            return View(Agenda.listaConsultas());
        }

        public ActionResult List_exames()
        {
            return View(Agenda.listaExames());
        }

        public ActionResult Appointment_Info(string id)
        {
            return View();
        }

        public ActionResult TrocarSenha()
        {
            return View();
        }
        public ActionResult Sair() // Apaga os cookies e volta para a tela de login
        {
            int aux = Request.Cookies.Count; // Para evitar loop :P
            string[] nomes = Request.Cookies.AllKeys; // Para chamar um cookie específico
            for (int i = 0; i < aux; i++)
            {
                HttpCookie myCookie = new HttpCookie(nomes[i]);
                myCookie.Expires = DateTime.Now.AddDays(-1d);
                Response.Cookies.Add(myCookie);
            }
            Request.Cookies.Clear(); // Limpa os cookies do servidor, mas não os da máquina do cliente

            return RedirectToAction("Login", "Home");
        }
    }
}