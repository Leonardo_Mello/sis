﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;

namespace SIS.Models.Users
{
    public static class Perfil
    {
        private static MySqlConnection con = new MySqlConnection(ConfigurationManager.ConnectionStrings["BCD"].ConnectionString);

        public static string tipoUsuario(string ID) // Retorna se é Hospital ou Person
        {
            MySqlConnection conexao = new MySqlConnection(ConfigurationManager.ConnectionStrings["BCD"].ConnectionString);
            string resultado = null;

            #region seForPerson
            try
            {
                if (conexao.State == ConnectionState.Closed)
                    conexao.Open();

                MySqlCommand query = new MySqlCommand("select pessoa.nivel_acesso from pessoa WHERE CONCAT(nome, ' ', sobrenome) = @id", conexao);
                query.Parameters.AddWithValue("@id", ID);
                MySqlDataReader reader = query.ExecuteReader();

                if (reader.Read())
                {
                    resultado = "Person";
                }
                reader.Close();

                conexao.Close();
            }
            catch (Exception error)
            {
                string errorMessage = error.Message;
                conexao.Close();
            }
            #endregion


            #region seForHospital
            try
            {
                if (conexao.State == ConnectionState.Closed)
                    conexao.Open();

                MySqlCommand query = new MySqlCommand("select hospital.cnes from hospital where nome_fantasia = @id", conexao);
                query.Parameters.AddWithValue("@id", ID);
                MySqlDataReader reader = query.ExecuteReader();

                if (reader.Read())
                {
                    resultado = "Hospital";
                }
                reader.Close();

                conexao.Close();
            }
            catch (Exception error)
            {
                string errorMessage = error.Message;
                conexao.Close();
            }
            #endregion

            return resultado;
        }

        public static string tipoUsuarioDetalhado(string ID) // Retorna se é Hospital ou Person
        {
            string resultado = null;

            #region seForPerson
            try
            {
                if (con.State == ConnectionState.Closed)
                    con.Open();

                MySqlCommand query = new MySqlCommand("select pessoa.nivel_acesso from pessoa WHERE CONCAT(nome, ' ', sobrenome) = @id", con);
                query.Parameters.AddWithValue("@id", ID);
                MySqlDataReader reader = query.ExecuteReader();

                if (reader.Read())
                {
                    resultado = reader[0].ToString();
                }
                reader.Close();

                con.Close();
            }
            catch (Exception error)
            {
                string errorMessage = error.Message;
                con.Close();
            }
            #endregion


            #region seForHospital
            try
            {
                if (con.State == ConnectionState.Closed)
                    con.Open();

                MySqlCommand query = new MySqlCommand("select hospital.cnes from hospital where nome_fantasia = @id", con);
                query.Parameters.AddWithValue("@id", ID);
                MySqlDataReader reader = query.ExecuteReader();

                if (reader.Read())
                {
                    resultado = "Hospital";
                }
                reader.Close();

                con.Close();
            }
            catch (Exception error)
            {
                string errorMessage = error.Message;
                con.Close();
            }
            #endregion

            return resultado;
        }

        public static object InformacoesDoPerfil(string id)
        {
            object resultado = InformacoesHospital(id);

            if (resultado != null)
                return resultado;
            else
                return InformacoesPessoa(id);
        }

        private static Hospital InformacoesHospital(string cnes)
        {
            Hospital hospital = null;

            try
            {
                if (con.State == ConnectionState.Closed)
                    con.Open();

                // Comando MySql desenvolvido para selecionar os dados do usuário autenticado
                MySqlCommand query = new MySqlCommand("select h.cnes, h.cnpj, h.nome_fantasia, h.razao_social, h.email, " +
                    "e.cep, e.estado, e.cidade, e.bairro, e.rua, e.numero, e.complemento, t.numero as telephone_number " +
                    "from hospital h, endereco e, telefone t " +
                    "where h.cnes = @cnes and e.id = h.endereco and t.hospital = h.cnes;", con);
                // Definindo os valores dos parâmetros passados no comando MySql
                query.Parameters.AddWithValue("@cnes", cnes);
                query.Parameters.AddWithValue("@cryptographyKey", ConfigurationManager.ConnectionStrings["cryptographyKey"].ConnectionString);

                // Guardando o retorno do comando efetuado
                MySqlDataReader reader = query.ExecuteReader();

                if (reader.Read())
                {
                    hospital = new Hospital
                    {
                        Cnes = Convert.ToString(reader["cnes"]),
                        Cnpj = Convert.ToString(reader["cnpj"]),
                        Nome_fantasia = Convert.ToString(reader["nome_fantasia"]),
                        Razao_social = Convert.ToString(reader["razao_social"]),
                        Email = Convert.ToString(reader["email"]),
                    };

                    hospital.Endereco.Cep = Convert.ToString(reader["cep"]);
                    hospital.Endereco.Estado = Convert.ToString(reader["estado"]);
                    hospital.Endereco.Cidade = Convert.ToString(reader["cidade"]);
                    hospital.Endereco.Bairro = Convert.ToString(reader["bairro"]);
                    hospital.Endereco.Rua = Convert.ToString(reader["rua"]);
                    hospital.Endereco.Numero = Convert.ToString(reader["numero"]);
                    hospital.Endereco.Complemento = Convert.ToString(reader["complemento"]);

                    hospital.Telephones.Numero = Convert.ToString(reader["telephone_number"]);
                    hospital.Telephones.Tipo = Convert.ToString(reader["tipo"]);
                }
                else
                    hospital = null;
                reader.Close();

                con.Close();
            }
            catch (Exception error)
            {
                string errorMessage = error.Message;
                hospital = null;
                con.Close();
            }

            return hospital;
        }

        private static Person InformacoesPessoa(string rg)
        {
            Person pessoa = null;

            try
            {
                // Abrindo conexão com o banco de dados
                if (con.State == ConnectionState.Closed)
                    con.Open();

                // Comando MySql desenvolvido para selecionar os dados do usuário autenticado
                MySqlCommand query = new MySqlCommand("select p.id, p.cpf, p.nome, p.sobrenome, p.data_nascimento, p.sexo, p.foto, p.email, p.nivel_acesso, p.nome_mae, p.nome_pai, " +
                    "e.cep, e.estado, e.cidade, e.bairro, e.rua, e.numero, e.complemento " +
                    "from pessoa p, endereco e " +
                    "where p.id = @rg and e.id = p.endereco;", con);

                query.Parameters.AddWithValue("@rg", rg);
                query.Parameters.AddWithValue("@cryptographyKey", ConfigurationManager.ConnectionStrings["cryptographyKey"].ConnectionString);
                
                MySqlDataReader reader = query.ExecuteReader();

                if (reader.Read())
                {
                    pessoa = new Person
                    {
                        Id = Convert.ToString(reader["id"]),
                        Cpf = Convert.ToString(reader["cpf"]),
                        Nome = Convert.ToString(reader["nome"]),
                        Sobrenome = Convert.ToString(reader["sobrenome"]),
                        Data_nascimento = Convert.ToDateTime(reader["data_nascimento"]),
                        Sexo = Convert.ToString(reader["sexo"]),
                        Email = Convert.ToString(reader["email"]),
                        Senha = Convert.ToString(reader["senha"]),
                        Nivel_acesso = Convert.ToString(reader["nivel_acesso"])
                    };

                    try
                    {
                        pessoa.Foto = (byte[])reader["foto"];
                    }
                    catch (Exception error)
                    {
                        string errorMessage = error.Message;
                    }

                    pessoa.Endereco.Cep = Convert.ToString(reader["cep"]);
                    pessoa.Endereco.Estado = Convert.ToString(reader["estado"]);
                    pessoa.Endereco.Cidade = Convert.ToString(reader["cidade"]);
                    pessoa.Endereco.Bairro = Convert.ToString(reader["bairro"]);
                    pessoa.Endereco.Rua = Convert.ToString(reader["rua"]);
                    pessoa.Endereco.Numero = Convert.ToString(reader["numero"]);
                    pessoa.Endereco.Complemento = Convert.ToString(reader["complemento"]);
                }
                reader.Close();

                if (!pessoa.Equals(null))
                {
                    pessoa.ListaTelefone = Person.CatchTelephone(rg);
                }

                con.Close();
            }
            catch (Exception error)
            {
                string errorMessage = error.Message;
                pessoa = null;
                con.Close();
            }

            return pessoa;
        }
    }
}