﻿using MySql.Data.MySqlClient;
using System.Collections.Generic;
using System.Configuration;

namespace SIS.Models.Users
{
    public class Employee
    {
        /** DECLARAÇÃO DOS ATRIBUTOS DA CLASSE **/
        #region parameters
        // --Variável declarada para conexão com o banco de  dados--
        private static MySqlConnection con = new MySqlConnection(ConfigurationManager.ConnectionStrings["BCD"].ConnectionString);
        // --Objetos instanciados para ligação entre as tabelas no banco de dados--
        private Hospital hospital = new Hospital();
        private List<Receptionist> receptionists = new List<Receptionist>();
        private List<Doctor> doctors = new List<Doctor>();
        #endregion
    }
}