﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Web;

namespace SIS.Models.Users
{
    public class Hospital
    {
        /** DECLARAÇÃO DOS ATRIBUTOS DA CLASSE **/
        #region parameters
        // --Variável declarada para conexão com o banco de  dados--
        private static MySqlConnection con = new MySqlConnection(ConfigurationManager.ConnectionStrings["BCD"].ConnectionString);
        // --Atributos da tabela 'hospital'--
        private string cnes, cnpj, nome_fantasia, razao_social, email, senha;
        private byte[] foto;
        // --Objetos instanciados para ligação entre as tabelas no banco de dados--
        private Address endereco = new Address();
        private List<Telephone> telephones = new List<Telephone>();

        /** Como nenhum dos atributos pode ser acessado ou modificado diretamente por alguma outra classe, criam-se 
       variavéis que serão as pontes para acessá-los e modificá-los (Getters e Setters)**/
        // --Atributos da tabela 'hospital'--
        [Required(ErrorMessage = "Obrigatório informar o CNES")]
        [StringLength(30, ErrorMessage = "O CNES deve possuir no máximo 30 caracteres")]
        [Display(Name = "CNES")]
        public string Cnes
        {
            get { return cnes; }
            set { cnes = value; }
        }

        [Required(ErrorMessage = "Obrigatório informar o CNPJ")]
        [StringLength(18, ErrorMessage = "O CNPJ deve possuir no máximo 18 caracteres")]
        [Display(Name = "CNPJ")]
        public string Cnpj
        {
            get { return cnpj; }
            set { cnpj = value; }
        }

        [Required(ErrorMessage = "Obrigatório informar o nome fantasia")]
        [StringLength(80, ErrorMessage = "O nome fantasia deve possuir no máximo 80 caracteres")]
        [Display(Name = "Nome fantasia")]
        public string Nome_fantasia
        {
            get { return nome_fantasia; }
            set { nome_fantasia = value; }
        }

        [Required(ErrorMessage = "Obrigatório informar a razão social")]
        [StringLength(150, ErrorMessage = "A razão social deve possuir no máximo 150 caracteres")]
        [Display(Name = "Razão social")]
        public string Razao_social
        {
            get { return razao_social; }
            set { razao_social = value; }
        }

        [Display(Name = "Foto de perfil")]
        public byte[] Foto
        {
            get { return getImage(); }
            set { foto = ResizeImageToDefault(value, 512, 512); }
        }

        [Required(ErrorMessage = "Obrigatório informar um endereço de e-mail")]
        [StringLength(100, ErrorMessage = "O e-mail deve possuir no máximo 100 caracteres")]
        [Display(Name = "E-mail")]
        public string Email
        {
            get { return email; }
            set { email = value; }
        }

        [Required(ErrorMessage = "Obrigatório informar uma senha")]
        [StringLength(30, ErrorMessage = "A senha deve possuir no máximo 30 caracteres")]
        [Display(Name = "Senha")]
        public string Senha
        {
            get { return senha; }
            set { senha = value; }
        }

        // --Objetos instanciados para ligação entre as tabelas no banco de dados--
        public Address Endereco
        {
            get { return endereco; }
            set { endereco = value; }
        }

        public List<Telephone> Telephones
        {
            get { return telephones; }
            set { telephones = value; }
        }

        public string Endereco_Formatado
        {
            get { return endereco.Rua + ", " + endereco.Numero + " - " + endereco.Bairro + "," + endereco.Cidade; }
        }
        #endregion

        /** MÉTODOS DA CLASSE **/
        #region methods

        #region recover_password
        #region get_contact_info
        // --Método desenvolvido para retornar as informações de contato do usuário--
        public static Hospital GetContactInfo(string cnes)
        {
            // Declaração de variável
            Hospital hospital = null;

            try
            {
                // Abrindo conexão com o banco de dados
                if (con.State == ConnectionState.Closed)
                    con.Open();

                // Comando MySql desenvolvido para selecionar os dados do usuário informado
                MySqlCommand query = new MySqlCommand("select h.email from hospital h where h.cnes = @cnes;", con);
                // Definindo os valores dos parâmetros passados no comando MySql
                query.Parameters.AddWithValue("@cnes", cnes);

                // Guardando o retorno do comando efetuado
                MySqlDataReader reader = query.ExecuteReader();

                if (reader.Read())
                {
                    hospital = new Hospital
                    {
                        Email = Convert.ToString(reader["email"])
                    };
                }
                reader.Close();

                con.Close();
            }
            catch (Exception error)
            {
                string errorMessage = error.Message;
                hospital = null;
                con.Close();
            }

            return hospital;
        }
        #endregion

        #region generate_code
        // --Método desenvolvido para gerar código de 7 dígitos e criar, ou atualizar, registro na tabela 'codigo_hospital' para esse usuário em específico--
        public static string GetCode(string username)
        {
            /** DECLARAÇÃO DE VARIÁVEIS **/
            // Criando objeto da classe Hospital com o id do usuário que requeriu o código de verificação de identidade
            Hospital user = new Hospital
            {
                Cnes = username
            };

            /** Geração de código de 7 dígitos com letras e números aleatórios **/
            // Declarando e inicializando string que conterá os caracteres disponíveis para geração do código
            string characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            // Declarando string que conterá o código aleatório a ser gerado
            string code = "";
            // Gerando código randômico
            Random random = new Random();
            code = new string(
                Enumerable.Repeat(characters, 7)
                  .Select(s => s[random.Next(s.Length)])
                  .ToArray());

            /** DESENVOLVIMENTO DO PROCESSO **/
            // Se o usuário já possui um código registrado, atualize o valor do código e da data em que foi gerado
            if (user.CheckExistenceCodeRecord())
            {
                user.UpdateCodeRecord(code);
            }
            // Senão crie um novo registro na tabela 'codigo_hospital' para esse usuário em específico
            else
            {
                user.CreateCodeRecord(code);
            }

            return code;
        }

        // --Método desenvolvido para checar se um usuário em específicio possui registro na tabela 'codigo_hospital'--
        private bool CheckExistenceCodeRecord()
        {
            // Declaração de variável
            bool result;

            try
            {
                // Abrindo conexão com o banco de dados
                if (con.State == ConnectionState.Closed)
                    con.Open();

                MySqlCommand query = new MySqlCommand("select * from codigo_hospital WHERE usuario = @username;", con);
                // Definindo os valores dos parâmetros passados no comando
                query.Parameters.AddWithValue("@username", cnes);

                //FAZ EXECUÇÃO DOS VALORES NO BANCO
                MySqlDataReader reader = query.ExecuteReader();
                reader.Read();
                result = reader.HasRows;
                reader.Close();
            }

            catch (Exception error)
            {
                string errorMsg = error.Message; //USAR BREAKPOINT PARA VISUALIZAR O ERRO
                result = false;
            }

            if (con.State == ConnectionState.Open)
                con.Close();

            return result;
        }

        // --Método desenvolvido para inserir novo registro na tabela 'codigo_hospital'--
        private void CreateCodeRecord(string code)
        {
            try
            {
                // Abrindo conexão com o banco de dados
                if (con.State == ConnectionState.Closed)
                    con.Open();

                // Comando MySql desenvolvido para inserir novo registro na tabela 'codigo_hospital'
                MySqlCommand query = new MySqlCommand("insert into codigo_hospital(usuario, codigo) values(@username, @code);", con);
                // Definindo os valores dos parâmetros passados no comando MySql
                query.Parameters.AddWithValue("@username", cnes);
                query.Parameters.AddWithValue("@code", code);

                // Efetuando o comando MySql desenvolvido acima
                query.ExecuteNonQuery();
            }

            catch (Exception error)
            {
                string errorMsg = error.Message;
            }

            if (con.State == ConnectionState.Open)
                con.Close();
        }

        // --Método desenvolvido para atualizar registro presente na tabela 'codigo_hospital'--
        private void UpdateCodeRecord(string code)
        {
            try
            {
                // Abrindo conexão com o banco de dados
                if (con.State == ConnectionState.Closed)
                    con.Open();

                // Comando MySql desenvolvido para atualizar registro presente na tabela 'codigo_hospital'
                MySqlCommand query = new MySqlCommand("update codigo_hospital set codigo = @code, data = @date where usuario = @username;", con);
                // Definindo os valores dos parâmetros passados no comando MySql
                query.Parameters.AddWithValue("@username", cnes);
                query.Parameters.AddWithValue("@code", code);
                query.Parameters.AddWithValue("@date", DateTime.Now.ToString("yyyy-MM-dd HH:mm"));

                // Efetuando o comando MySql desenvolvido acima
                query.ExecuteNonQuery();
            }

            catch (Exception error)
            {
                string errorMsg = error.Message;
            }

            if (con.State == ConnectionState.Open)
                con.Close();
        }
        #endregion

        #region code_validation
        // --Método desenvolvido para validar o código informado pelo usuário--
        public static bool[,] ValidateCode(string username, string informedCode)
        {
            // Declaração de variável
            bool[,] result = new bool[1, 2]; // a primeira posição da matriz é destinada a armazenar o valor booleano referente ao tempo de validade 
                                             // do último código gerado a esse usuário e a segunda posição é destinada a armazenar o valor booleano 
                                             // referente a validade do código informado pelo usuário

            try
            {
                // Abrindo conexão com o banco de dados
                if (con.State == ConnectionState.Closed)
                    con.Open();

                MySqlCommand query = new MySqlCommand("select * from codigo_hospital where usuario = @username", con);
                // Definindo os valores dos parâmetros passados no comando
                query.Parameters.AddWithValue("@username", username);

                //FAZ EXECUÇÃO DOS VALORES NO BANCO
                MySqlDataReader reader = query.ExecuteReader();

                if (reader.Read())
                {
                    DateTime date = Convert.ToDateTime(reader["data"]);
                    DateTime deadline = date.AddMinutes(20);
                    if (DateTime.Now >= deadline)
                    {
                        result[0, 0] = false; // o tempo de validade do último código gerado esgotou-se
                    }
                    else
                    {
                        result[0, 0] = true; // o tempo de validade do último código gerado ainda não esgotou-se

                        if (!informedCode.Equals(Convert.ToString(reader["codigo"])))
                            result[0, 1] = false; // o código informado pelo usuário é inválido
                        else
                            result[0, 1] = true; // o código informado pelo usuário é válido
                    }
                }
                else
                {
                    result[0, 0] = true;
                    result[0, 1] = false;
                }

                reader.Close();
            }

            catch (Exception error)
            {
                string errorMsg = error.Message; //USAR BREAKPOINT PARA VISUALIZAR O ERRO
                result[0, 0] = false;
            }

            if (con.State == ConnectionState.Open)
                con.Close();

            return result;
        }
        #endregion

        #region redefine_password
        // --Método desenvolvido para alterar a atual senha do usuário--
        public void ChangePassword(string newPassword)
        {
            try
            {
                // Abrindo conexão com o banco de dados
                if (con.State == ConnectionState.Closed)
                    con.Open();

                // Comando MySql desenvolvido para alterar a senha de um usuário presente na tabela 'hospital'
                MySqlCommand query = new MySqlCommand("update hospital set senha = aes_encrypt(@newPassword, @cryptographyKey) where cnes = @cnes;", con);
                // Definindo os valores dos parâmetros passados no comando MySql
                query.Parameters.AddWithValue("@cnes", cnes);
                query.Parameters.AddWithValue("@newPassword", newPassword);
                query.Parameters.AddWithValue("@cryptographyKey", ConfigurationManager.ConnectionStrings["cryptographyKey"].ConnectionString);

                // Efetuando o comando MySql desenvolvido acima
                query.ExecuteNonQuery();
            }

            catch (Exception error)
            {
                string errorMsg = error.Message;
            }

            if (con.State == ConnectionState.Open)
                con.Close();
        }
        #endregion
        #endregion

        #region log_in
        // --Método de autenticação de usuário--
        public static Hospital Login(string login, string password)
        {
            // Declaração de variável
            Hospital result = null;

            try
            {
                // Abrindo conexão com o banco de dados
                if (con.State == ConnectionState.Closed)
                    con.Open();

                // Comando MySql desenvolvido para selecionar o usuário que possui as especificações recebidas
                MySqlCommand query = new MySqlCommand("select * from hospital where cnes = @login and senha = aes_encrypt(@password, @cryptographyKey);", con);
                // Definindo os valores dos parâmetros passados no comando
                query.Parameters.AddWithValue("@login", login);
                query.Parameters.AddWithValue("@password", password);
                query.Parameters.AddWithValue("@cryptographyKey", ConfigurationManager.ConnectionStrings["cryptographyKey"].ConnectionString);

                // Guardando o retorno do comando efetuado
                MySqlDataReader reader = query.ExecuteReader();

                if (reader.Read())
                {
                    reader.Close();
                    result = Hospital.GetInfo(login);
                }

                if (reader.IsClosed == false)
                    reader.Close();
            }

            catch (Exception error)
            {
                string errorMsg = error.Message;
                con.Close();
                return null;
            }

            if (con.State == ConnectionState.Open)
                con.Close();

            return result;
        }
        #endregion

        #region hospital_account
        #region view_profile
        // --Método desenvolvido para retornar os dados do hospital autenticado no sistema--
        public static Hospital GetInfo(string cnes)
        {
            // Declaração de variável
            Hospital hospital = null;

            try
            {
                // Abrindo conexão com o banco de dados
                if (con.State == ConnectionState.Closed)
                    con.Open();

                // Comando MySql desenvolvido para selecionar os dados do usuário autenticado
                MySqlCommand query = new MySqlCommand("select h.cnes, h.cnpj, h.nome_fantasia, h.razao_social, h.foto, h.email, cast(aes_decrypt(h.senha, @cryptographyKey) as char) as senha, " +
                    "e.cep, e.estado, e.cidade, e.bairro, e.rua, e.numero, e.complemento " +
                    "from hospital h, endereco e " +
                    "where h.cnes = @cnes and e.id = h.endereco;", con);
                // Definindo os valores dos parâmetros passados no comando MySql
                query.Parameters.AddWithValue("@cryptographyKey", ConfigurationManager.ConnectionStrings["cryptographyKey"].ConnectionString);
                query.Parameters.AddWithValue("@cnes", cnes);

                // Guardando o retorno do comando efetuado
                MySqlDataReader reader = query.ExecuteReader();

                if (reader.Read())
                {
                    hospital = new Hospital
                    {
                        Cnes = Convert.ToString(reader["cnes"]),
                        Cnpj = Convert.ToString(reader["cnpj"]),
                        Nome_fantasia = Convert.ToString(reader["nome_fantasia"]),
                        Razao_social = Convert.ToString(reader["razao_social"]),
                        Email = Convert.ToString(reader["email"]),
                        Senha = Convert.ToString(reader["senha"])
                    };

                    hospital.Endereco.Cep = Convert.ToString(reader["cep"]);
                    hospital.Endereco.Estado = Convert.ToString(reader["estado"]);
                    hospital.Endereco.Cidade = Convert.ToString(reader["cidade"]);
                    hospital.Endereco.Bairro = Convert.ToString(reader["bairro"]);
                    hospital.Endereco.Rua = Convert.ToString(reader["rua"]);
                    hospital.Endereco.Numero = Convert.ToString(reader["numero"]);
                    hospital.Endereco.Complemento = Convert.ToString(reader["complemento"]);
                }
                reader.Close();

                if (!hospital.Equals(null))
                {
                    hospital.telephones = GetTelephones(cnes);
                }

                con.Close();
            }
            catch (Exception error)
            {
                string errorMessage = error.Message;
                hospital = null;
                con.Close();
            }

            return hospital;
        }

        private static List<Telephone> GetTelephones(string cnes)
        {
            // Declaração de variável
            List<Telephone> telephones = new List<Telephone>();

            try
            {
                // Abrindo conexão com o banco de dados
                if (con.State == ConnectionState.Closed)
                    con.Open();

                // Comando MySql desenvolvido para selecionar os dados do usuário autenticado
                MySqlCommand query = new MySqlCommand("select t.numero from telefone t where t.hospital = @cnes", con);
                // Definindo os valores dos parâmetros passados no comando MySql
                query.Parameters.AddWithValue("@cnes", cnes);

                // Guardando o retorno do comando efetuado
                MySqlDataReader reader = query.ExecuteReader();

                while (reader.Read())
                {
                    Telephone telephone = new Telephone
                    {
                        Numero = Convert.ToString(reader["numero"]),
                    };

                    telephones.Add(telephone);
                }

                con.Close();
            }
            catch (Exception error)
            {
                string errorMessage = error.Message;
                telephones = null;
                con.Close();
            }

            return telephones;
        }
        #endregion

        #region list
        // --Método desenvolvido para retornar lista populada com hospitais--
        public static List<Hospital> List_hospitals()
        {
            // Declaração de variável
            List<Hospital> hospitals = new List<Hospital>();

            try
            {
                // Abrindo conexão com o banco de dados
                if (con.State == ConnectionState.Closed)
                    con.Open();

                // Comando MySql desenvolvido para selecionar todos os hospitais existentes no banco de dados
                MySqlCommand query = new MySqlCommand("select cnes, cnpj, nome_fantasia, razao_social from hospital;", con);

                // Guardando o retorno do comando efetuado
                MySqlDataReader reader = query.ExecuteReader();

                while (reader.Read())
                {
                    Hospital hospital = new Hospital
                    {
                        Cnes = Convert.ToString(reader["cnes"]),
                        Cnpj = Convert.ToString(reader["cnpj"]),
                        Nome_fantasia = Convert.ToString(reader["nome_fantasia"]),
                        Razao_social = Convert.ToString(reader["razao_social"])
                    };

                    hospitals.Add(hospital);
                }

                con.Close();
            }
            catch (Exception error)
            {
                string errorMessage = error.Message;
                hospitals = null;
                con.Close();
            }

            return hospitals;
        }
        #endregion
        #endregion

        // --Método desenvolvido para retornar as imagens de contato do usuário na lista--
        private byte[] getImage()
        {
            byte[] resultado;
            try
            {
                // Abrindo conexão com o banco de dados
                if (con.State == ConnectionState.Closed)
                    con.Open();


                MySqlCommand query = new MySqlCommand("SELECT hospital.foto FROM hospital WHERE cnes = @cnes", con);
                query.Parameters.AddWithValue("@cnes", cnes);
                MySqlDataReader reader = query.ExecuteReader();

                reader.Read();
                resultado = (byte[])reader["foto"];

                con.Close();
            }
            catch (Exception error)
            {
                string errorMessage = error.Message;
                resultado = null;
                con.Close();
            }

            return resultado;
        }

        // Método para inserir novos registros na tabela 'hospital'
        public string Register()
        {
            //endereco.Register(); //GUARDA O ENDEREÇO DA PESSOA NA TABELA DE ENDEREÇOS

            try
            {
                // Abrindo conexão com o banco de dados
                con.Open();

                //INSERE O COMANDO QUE VAI SER UTILIZADO NO BANCO DE DADOS
                MySqlCommand query = new MySqlCommand("INSERT INTO hospital VALUES (@cnes, @cnpj, @nome_fantasia, @razao_social, @foto," +
                                            " @email, aes_encrypt(@senha, @chave), @endereco);", con);
                // Definindo os valores dos parâmetros passados no comando
                query.Parameters.AddWithValue("@cnes", cnes);
                query.Parameters.AddWithValue("@cnpj", cnpj);
                query.Parameters.AddWithValue("@nome_fantasia", nome_fantasia);
                query.Parameters.AddWithValue("@razao_social", razao_social);
                query.Parameters.AddWithValue("@foto", foto);
                query.Parameters.AddWithValue("@email", email);
                query.Parameters.AddWithValue("@senha", senha);
                query.Parameters.AddWithValue("@chave", ConfigurationManager.ConnectionStrings["cryptographyKey"].ConnectionString);
                query.Parameters.AddWithValue("@endereco", endereco.Id);

                //FAZ EXECUÇÃO DOS VALORES NO BANCO
                query.ExecuteNonQuery();
            }

            catch (Exception error)
            {
                con.Close();
                //APRESENTA A MENSAGEM CASO HOUVER ERRO
                return error.Message;
            }

            /*telefone.RegisterTelephone();*/ //GUARDA O TELEFONE DA PESSOA NA TABELA DE TELEFONES

            if (con.State == ConnectionState.Open)
                con.Close();

            return "Inserido com sucesso!";
        }

        //Verifica se este hospital existe
        public static bool Exists(string nome)
        {
            bool resultado = false;

            try
            {
                if (con.State == ConnectionState.Closed)
                    con.Open();

                MySqlCommand query = new MySqlCommand("SELECT * FROM hospital WHERE nome_fantasia = @nome", con);
                query.Parameters.AddWithValue("@nome", nome);
                MySqlDataReader reader = query.ExecuteReader();

                resultado = reader.Read();

                con.Close();
            }
            catch (Exception error)
            {
                string errorMessage = error.Message;
                resultado = false;
                con.Close();
            }

            return resultado;
        }

        //---------------------------------METODO BUSCA USUARIOS PARA EDITAR-------------------------------------//
        public static Hospital EditarDadosHospital(int idUsuario)
        {
            //CRIAR OBJETO USUARIO
            Hospital hospital = new Hospital();
            //TESTAR A CONEXÃO PARA VER SE ESTÁ CERTO
            try
            {
                //ABRE A CONEXÃO
                con.Open();
                //INSERE O COMANDO QUE VAI SER UTILIZADO NO BANCO DE DADOS
                MySqlCommand query =
                    new MySqlCommand("UPDATE hospital SET cnes= @cnes", con);
                //RECEBE OS VALORES RECEBIDOS E ATRIBUI AOS ATRIBUTOS
                query.Parameters.AddWithValue("@cnes", idUsuario);
                //FAZ A LEITURA DO QUE ESTA RECEBENDO DO BANCO DE DADOS
                MySqlDataReader leitor = query.ExecuteReader();
                //LAÇO PARA VERIFICAR SE ESTA PERCORRENDO TODA LISTA 
                while (leitor.Read())
                {
                    ////BUSCA TODOS OS VALORES DA LISTA
                    //hospital.Cnes = leitor["numID"].ToString();
                    //hospital.Nome = leitor["nome"].ToString();
                    //hospital.Email = leitor["email"].ToString();
                    //hospital.AreaAtuacao = leitor["areaAtuacao"].ToString();
                    //hospital.TipoUsuario = leitor["tipoUsuario"].ToString();
                    //hospital.login = leitor["usuario"].ToString();

                }
            }
            //SE HOUVER ERRO VEM PARA CÁ
            catch (Exception e)
            {
                string msgErro = e.Message;

                //LISTA VAZIA
                hospital = null;
                con.Close();
            }
            //VERIFICA SE A CONEXÃO ESTÁ ABERTO
            if (con.State == ConnectionState.Open)
                //SE TIVER ABERTA, FECHA CONEXÃO
                con.Close();
            //RETORNA A LISTA 
            return hospital;
        }

        // REMOVER OS HOSPITAIS
        public string Remover()
        {
            string mensagem = "Hospital removido com sucesso!";
            try
            {
                con.Open();
                MySqlCommand query = new MySqlCommand("DELETE from hospital WHERE cnes = @cnes", con);
                query.Parameters.AddWithValue("@cnes", cnes);
                query.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                mensagem = e.Message;
            }

            con.Close();
            return mensagem;
        }

        public string Editar()
        {
            return "TESTE!!!!!!";
        }

        public static string GetIdByCnes(string cnes)
        {
            MySqlConnection conexao = new MySqlConnection(ConfigurationManager.ConnectionStrings["BCD"].ConnectionString);
            string resultado = "";
            try
            {
                if (conexao.State == ConnectionState.Closed)
                    conexao.Open();

                MySqlCommand query = new MySqlCommand("SELECT hospital.cnes FROM hospital WHERE hospital.nome_fantasia = @cnes", conexao);
                query.Parameters.AddWithValue("@cnes", cnes);
                MySqlDataReader reader = query.ExecuteReader();

                reader.Read();
                resultado = reader["cnes"].ToString();

                conexao.Close();
            }
            catch (Exception error)
            {
                string errorMessage = error.Message;
                resultado = "";
                conexao.Close();
            }

            return resultado;
        }

        private byte[] ResizeImageToDefault(byte[] imageEmBytes, int width, int height)
        {
            Image image = Image.FromStream(new MemoryStream(imageEmBytes));

            var destRect = new Rectangle(0, 0, width, height);
            var destImage = new Bitmap(width, height);

            destImage.SetResolution(image.HorizontalResolution, image.VerticalResolution);

            using (var graphics = Graphics.FromImage(destImage))
            {
                graphics.CompositingMode = CompositingMode.SourceCopy;
                graphics.CompositingQuality = CompositingQuality.HighQuality;
                graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphics.SmoothingMode = SmoothingMode.HighQuality;
                graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;

                using (var wrapMode = new ImageAttributes())
                {
                    wrapMode.SetWrapMode(WrapMode.TileFlipXY);
                    graphics.DrawImage(image, destRect, 0, 0, image.Width, image.Height, GraphicsUnit.Pixel, wrapMode);
                }
            }

            MemoryStream ms = new MemoryStream();
            destImage.Save(ms, ImageFormat.Png);
            return ms.ToArray();
        }

        public static string GetPassword(string nome)
        {
            MySqlConnection conexao = new MySqlConnection(ConfigurationManager.ConnectionStrings["BCD"].ConnectionString);
            string resultado = "";
            try
            {
                conexao.Open();

                MySqlCommand query = new MySqlCommand("SELECT CAST(senha as char) as senha FROM hospital WHERE nome_fantasia = @nome", conexao);
                query.Parameters.AddWithValue("@nome", nome);
                MySqlDataReader reader = query.ExecuteReader();

                if (reader.Read())
                    resultado = reader["senha"].ToString();

                conexao.Close();
            }
            catch (Exception error)
            {
                string errorMessage = error.Message;
                resultado = "ERRO";
                conexao.Close();
            }

            string resultadoString = "";

            foreach (byte valor in resultado)
                resultadoString = resultadoString + valor;

            return resultadoString;
        }
        #endregion
    }
}