﻿using MySql.Data.MySqlClient;
using System;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Data;

namespace SIS.Models
{
    public class Address
    {
        /** DECLARAÇÃO DOS ATRIBUTOS DA CLASSE **/
        #region parameters
        // --Variável declarada para conexão com o banco de  dados--
        private static MySqlConnection con = new MySqlConnection(ConfigurationManager.ConnectionStrings["BCD"].ConnectionString);
        // --Atributos da tabela 'endereco'--
        private int id;
        private string cep, estado, cidade, bairro, rua, numero, complemento;

        /** Como nenhum dos atributos pode ser acessado ou modificado diretamente por alguma outra classe, criam-se 
        variavéis que serão as pontes para acessá-los e modificá-los (Getters e Setters)**/
        // --Atributos da tabela 'endereco'--
        public int Id
        {
            get { return Catch_id(); }
            set { id = value; }
        }

        [Required(ErrorMessage = "Obrigatório informar o CEP")]
        [StringLength(9, ErrorMessage = "O CEP deve possuir no máximo 9 caracteres")]
        [Display(Name = "CEP")]
        public string Cep
        {
            get { return cep; }
            set { cep = value; }
        }

        [Required(ErrorMessage = "Obrigatório informar o estado")]
        [StringLength(2, ErrorMessage = "O estado deve possuir no máximo 2 caracteres")]
        [Display(Name = "Estado")]
        public string Estado
        {
            get { return estado; }
            set { estado = value; }
        }

        [Required(ErrorMessage = "Obrigatório informar o município")]
        [StringLength(50, ErrorMessage = "O município deve possuir no máximo 50 caracteres")]
        [Display(Name = "Município")]
        public string Cidade
        {
            get { return cidade; }
            set { cidade = value; }
        }

        [Required(ErrorMessage = "Obrigatório informar o bairro")]
        [StringLength(60, ErrorMessage = "O bairro deve possuir no máximo 60 caracteres")]
        [Display(Name = "Bairro")]
        public string Bairro
        {
            get { return bairro; }
            set { bairro = value; }
        }

        [Required(ErrorMessage = "Obrigatório informar a rua")]
        [StringLength(100, ErrorMessage = "A rua deve possuir no máximo 100 caracteres")]
        [Display(Name = "Rua")]
        public string Rua
        {
            get { return rua; }
            set { rua = value; }
        }

        [Required(ErrorMessage = "Obrigatório informar o número")]
        [StringLength(5, ErrorMessage = "O número deve possuir no máximo 5 caracteres")]
        [Display(Name = "Número")]
        public string Numero
        {
            get { return numero; }
            set { numero = value; }
        }

        [StringLength(30, ErrorMessage = "O complemento deve possuir no máximo 30 caracteres")]
        [Display(Name = "Complemento")]
        public string Complemento
        {
            get { return complemento; }
            set { complemento = value; }
        }
        #endregion

        /** MÉTODOS DA CLASSE **/
        #region methods

        #region address_manipulation
        #region register_address
        // --Método desenvolvido para cadastrar um novo endereço no banco de dados, caso venha a ser necessário, e retornar o id do mesmo--
        public int GetAddress()
        {
            /** DECLARAÇÃO DE VARIÁVEIS **/
            int id;

            /** DESENVOLVIMENTO DO PROCESSO **/
            // Se o endereço recebido não existe, cadastre-o
            if (!CheckExistenceAddress())
            {
                Register();
            }
            id = Catch_id();

            return id;
        }

        // --Método desenvolvido para checar se um endereço específicio possui registro na tabela 'endereco'--
        private bool CheckExistenceAddress()
        {
            // Declaração de variável
            bool result = false;

            try
            {
                // Abrindo conexão com o banco de dados
                if (con.State == ConnectionState.Closed)
                    con.Open();

                // Comando MySql desenvolvido para selecionar um registro da tabela 'endereco' do banco de dados
                MySqlCommand query = new MySqlCommand("select * from endereco where cep = @zip_code and numero = @number and complemento = @complement;", con);
                // Definindo os valores dos parâmetros passados no comando
                query.Parameters.AddWithValue("@zip_code", cep);
                query.Parameters.AddWithValue("@number", numero);
                query.Parameters.AddWithValue("@complement", complemento);

                // Guardando o retorno do comando efetuado
                MySqlDataReader reader = query.ExecuteReader();

                if (reader.Read())
                {
                    result = true;
                }

                con.Close();
            }
            catch (Exception error)
            {
                string errorMessage = error.Message;
                result = false;
                con.Close();
            }

            return result;
        }

        // --Método desenvolvido para cadastrar um novo endereço no sistema--
        private void Register()
        {
            try
            {
                // Abrindo conexão com o banco de dados
                if (con.State == ConnectionState.Closed)
                    con.Open();

                // Comando MySql desenvolvido para adicionar um registro na tabela 'endereco' do banco de dados
                MySqlCommand query = new MySqlCommand("insert into endereco values(@id, @zip_code, @state, @city, @neighborhood, @street, @number, @complement);", con);
                // Definindo os valores dos parâmetros passados no comando MySql
                query.Parameters.AddWithValue("@id", null);
                query.Parameters.AddWithValue("@zip_code", cep);
                query.Parameters.AddWithValue("@state", estado);
                query.Parameters.AddWithValue("@city", cidade);
                query.Parameters.AddWithValue("@neighborhood", bairro);
                query.Parameters.AddWithValue("@street", rua);
                query.Parameters.AddWithValue("@number", numero);
                query.Parameters.AddWithValue("@complement", complemento);

                // Executando o comando MySQL elaborado
                query.ExecuteNonQuery();

                con.Close();
            }
            catch (Exception error)
            {
                string errorMessage = error.Message;
                con.Close();
            }
        }

        // --Método desenvolvido para selecionar o id de um endereço existente no banco de dados--
        private int Catch_id()
        {
            // Declaração de variável
            int id = -1;

            try
            {
                // Abrindo conexão com o banco de dados
                if (con.State == ConnectionState.Closed)
                    con.Open();

                // Comando MySql desenvolvido para selecionar o id de um endereço existente no banco de dados
                MySqlCommand query = new MySqlCommand("select id from endereco where cep = @zip_code and numero = @number and complemento = @complement;", con);
                // Definindo os valores dos parâmetros passados no comando MySql
                query.Parameters.AddWithValue("@zip_code", cep);
                query.Parameters.AddWithValue("@number", numero);
                query.Parameters.AddWithValue("@complement", complemento);

                // Guardando o retorno do comando efetuado
                MySqlDataReader reader = query.ExecuteReader();

                if (reader.Read())
                {
                    id = int.Parse(Convert.ToString(reader["id"]));
                }

                con.Close();
            }
            catch (Exception error)
            {
                string errorMessage = error.Message;
                id = -1;
                con.Close();
            }

            return id;
        }
        #endregion

        // --Método desenvolvido para buscar endereços--
        public static Address SearchAddress(string zip_code, string number, string complement)
        {
            // Declaração de variável
            Address result = new Address();

            try
            {
                // Abrindo conexão com o banco de dados
                con.Open();

                //INSERE O COMANDO QUE VAI SER UTILIZADO NO BANCO DE DADOS
                MySqlCommand query = new MySqlCommand("SELECT * FROM endereco WHERE cep = @zip_code and numero = @number and complemento = @complement;", con);
                // Definindo os valores dos parâmetros passados no comando
                query.Parameters.AddWithValue("@zip_code", zip_code);
                query.Parameters.AddWithValue("@number", number);
                query.Parameters.AddWithValue("@complement", complement);

                //FAZ EXECUÇÃO DOS VALORES NO BANCO
                MySqlDataReader reader = query.ExecuteReader();
                if (reader.Read())
                {
                    result.Id = reader.GetInt32(0);
                    result.Cep = reader.GetString(1);
                    result.Estado = reader.GetString(2);
                    result.Cidade = reader.GetString(3);
                    result.Bairro = reader.GetString(4);
                    result.Rua = reader.GetString(5);
                    result.Numero = reader.GetString(6);
                    result.Complemento = reader.GetString(7);
                }
                else
                    result = null;
            }

            catch (Exception e)
            {
                string msgErro = e.Message;
                result = null;
            }

            if (con.State == ConnectionState.Open)
                con.Close();

            return result;

        }
        #endregion

        #endregion
    }
}