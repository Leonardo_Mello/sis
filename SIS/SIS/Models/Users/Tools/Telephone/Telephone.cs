﻿using MySql.Data.MySqlClient;
using SIS.Models.Users;
using System;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Data;
using System.Text;
using System.Text.RegularExpressions;

namespace SIS.Models
{
    public class Telephone
    {
        /** DECLARAÇÃO DOS ATRIBUTOS DA CLASSE **/
        #region parameters
        // Variável declarada para conexão com o banco de  dados
        private static MySqlConnection con = new MySqlConnection(ConfigurationManager.ConnectionStrings["BCD"].ConnectionString);
        // Atributos da tabela 'telefone'
        private int id;
        private string numero;
        // Atributos da tabela 'tipo_telefone'
        private int type_telephone_id;
        private string type_telephone_descricao;
        // Objeto instanciado para ligação entre as tabelas no banco de dados
        private Hospital hospital = new Hospital();

        /** Como nenhum dos atributos pode ser acessado ou modificado diretamente por alguma outra classe, criam-se 
        variavéis que serão as pontes para acessá-los e modificá-los (Getters e Setters)**/
        // Atributos da tabela 'telefone'
        public int Id
        {
            get { return Catch_id(); }
            set { id = value; }
        }

        [Required(ErrorMessage = "Obrigatório informar um número de telefone")]
        [StringLength(20, ErrorMessage = "O número de telefone deve possuir no máximo 20 caracteres")]
        [Display(Name = "Número de telefone")]
        public string Numero
        {
            get { return numero; }
            set { numero = value; }
        }

        [Required(ErrorMessage = "Obrigatório informar o tipo de telefone")]
        [StringLength(7, ErrorMessage = "O tipo de telefone deve possuir no máximo 7 caracteres")]
        [Display(Name = "Tipo de telefone")]
        public int Type_telephone_id
        {
            get { return type_telephone_id; }
            set { type_telephone_id = value; }
        }

        // Objeto instanciado para ligação entre as tabelas no banco de dados
        public Hospital Hospital
        {
            get { return hospital; }
            set { hospital = value; }
        }
        #endregion

        /** MÉTODOS DA CLASSE **/
        #region methods

        #region telephone_manipulation
        #region register_telephone
        // --Método desenvolvido para cadastrar um novo telefone no banco de dados, caso venha a ser necessário, e conectá-lo a um usuário específico--
        public void Register_user_telephone(string user_id)
        {
            /** DESENVOLVIMENTO DO PROCESSO **/
            // Se o telefone recebido não existe, cadastre-o
            if (!CheckExistenceTelephone())
            {
                Register();
            }
            id = Catch_id();

            Telephone.Relate_user_telephone(user_id, id);
        }

        // --Método desenvolvido para checar se um telefone específico possui registro na tabela 'telefone'--
        private bool CheckExistenceTelephone()
        {
            // Declaração de variável
            bool result = false;

            try
            {
                // Abrindo conexão com o banco de dados
                if (con.State == ConnectionState.Closed)
                    con.Open();

                // Comando MySql desenvolvido para selecionar um registro da tabela 'endereco' do banco de dados
                MySqlCommand query = new MySqlCommand("select * from telefone where numero = @number and tipo = @type;", con);
                // Definindo os valores dos parâmetros passados no comando
                query.Parameters.AddWithValue("@number", numero);
                query.Parameters.AddWithValue("@type", type_telephone_id);

                // Guardando o retorno do comando efetuado
                MySqlDataReader reader = query.ExecuteReader();

                if (reader.Read())
                {
                    result = true;
                }

                con.Close();
            }
            catch (Exception error)
            {
                string errorMessage = error.Message;
                result = false;
                con.Close();
            }

            return result;
        }

        // --Método desenvolvido para cadastrar um novo telefone no sistema--
        private void Register()
        {
            try
            {
                // Abrindo conexão com o banco de dados
                if (con.State == ConnectionState.Closed)
                    con.Open();

                // Comando MySql desenvolvido para adicionar um registro na tabela 'telefone' do banco de dados
                MySqlCommand query = new MySqlCommand("insert into telefone(numero, tipo) values(@number, @type);", con);
                // Definindo os valores dos parâmetros passados no comando MySql
                query.Parameters.AddWithValue("@number", numero);
                query.Parameters.AddWithValue("@type", type_telephone_id);

                // Executando o comando MySQL elaborado
                query.ExecuteNonQuery();

                con.Close();
            }
            catch (Exception error)
            {
                string errorMessage = error.Message;
                con.Close();
            }
        }

        // --Método desenvolvido para selecionar o id de um telefone existente no banco de dados--
        private int Catch_id()
        {
            // Declaração de variável
            int id = -1;

            try
            {
                // Abrindo conexão com o banco de dados
                if (con.State == ConnectionState.Closed)
                    con.Open();

                // Comando MySql desenvolvido para selecionar o id de um telefone existente no banco de dados
                MySqlCommand query = new MySqlCommand("select id from telefone where numero = @number and tipo = @type;", con);
                // Definindo os valores dos parâmetros passados no comando MySql
                query.Parameters.AddWithValue("@number", numero);
                query.Parameters.AddWithValue("@type", type_telephone_id);

                // Guardando o retorno do comando efetuado
                MySqlDataReader reader = query.ExecuteReader();

                if (reader.Read())
                {
                    id = int.Parse(Convert.ToString(reader["id"]));
                }

                con.Close();
            }
            catch (Exception error)
            {
                string errorMessage = error.Message;
                id = -1;
                con.Close();
            }

            return id;
        }

        // --Método desenvolvido para ligar um usuário a um endereço no banco de dados--
        private static void Relate_user_telephone(string person, int telephone)
        {
            try
            {
                // Abrindo conexão com o banco de dados
                if (con.State == ConnectionState.Closed)
                    con.Open();

                // Comando MySql desenvolvido para adicionar um registro na tabela 'pessoa_telefone' do banco de dados
                MySqlCommand query = new MySqlCommand("insert into pessoa_telefone values(@pessoa, @telefone);", con);
                // Definindo os valores dos parâmetros passados no comando MySql
                query.Parameters.AddWithValue("@pessoa", person);
                query.Parameters.AddWithValue("@telefone", telephone);

                // Executando o comando MySQL elaborado
                query.ExecuteNonQuery();

                con.Close();
            }
            catch (Exception error)
            {
                string errorMessage = error.Message;
                con.Close();
            }
        }
        #endregion
        #endregion

        public static void SendViaNett(string telephoneNumber, string code)
        {
            // Conversão do telefone recebido para conter somente números
            Regex regex = new Regex("[0-9]"); // limitando os dados que o número de telefone convertido pode conter
            StringBuilder telephoneNumberConverted = new StringBuilder(); // criando instância de um construtor de string
            foreach (Match m in regex.Matches(telephoneNumber))
            {
                telephoneNumberConverted.Append(m.Value);
            }
            long telephone = Convert.ToInt64(telephoneNumberConverted.ToString());

            try
            {
                using (var vianett = new ViaNett.CPAWebServiceSoapClient("CPAWebServiceSoap12"))
                {
                    var result = vianett.SendSMS_Simple1(
                        "desyree.anny@hotmail.com", "bxlbf",
                        telephone, "Use " + code + " como código de redefinição de senha " +
                        "da conta do SIS");
                }
            }
            catch (Exception error)
            {
                string errorMessage = error.Message;
            }
        }
        #endregion
    }
}