﻿using SIS.Models.Users;
using System;
using System.Configuration;
using System.Net.Mail;
using System.Text.RegularExpressions;

namespace SIS.Models
{
    // CLASSE PARA MANIPULAÇÃO DE EMAILs
    public class Email
    {
        public static void Send_email(string recipient, string username, string code)
        {
            // Camuflando o nome de usuário recebido
            string hiddenUsername = username.Substring(2);
            hiddenUsername = Regex.Replace(hiddenUsername, @"(?i)[0-9a-záéíóúàèìòùâêîôûãõç]", "*");
            username = username.Substring(0, 2) + hiddenUsername;

            MailMessage emailMessage = new MailMessage(ConfigurationManager.ConnectionStrings["emailSIS"].ConnectionString,
                recipient, "Redefinição de senha da conta do SIS", "Código de redefinição de senha\n\nUse esse código para " +
                "redefinir a senha da conta do SIS " + username + ".\n\nAqui está o seu código: " + code + "\n\nAtenciosamente,\nEquipe de contas do SIS");

            //CONFIGURAÇÃO DO SMTP
            try
            {
                SmtpClient smtpServer = new SmtpClient("smtp.gmail.com", 587)
                {
                    EnableSsl = true,
                    UseDefaultCredentials = false,
                    Credentials = new System.Net.NetworkCredential(ConfigurationManager.ConnectionStrings["emailSIS"].ConnectionString, ConfigurationManager.ConnectionStrings["senhaSIS"].ConnectionString)
                };
                smtpServer.Send(emailMessage);
            }
            catch (SmtpException error)
            {
                string errorMessage = error.Message;
            }
        }
    }
}