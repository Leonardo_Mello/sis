﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Data;

namespace SIS.Models.Users
{
    public class Receptionist : Person
    {
        /** MÉTODOS DA CLASSE **/
        #region methods
        #region recover_password
        #region get_contact_info
        // --Método desenvolvido para retornar as informações de contato do usuário--
        public static Receptionist GetContactInfo(string id)
        {
            // Declaração de variável
            Receptionist receptionist = null;

            try
            {
                // Abrindo conexão com o banco de dados
                if (con.State == ConnectionState.Closed)
                    con.Open();

                // Comando MySql desenvolvido para selecionar os dados do usuário informado
                MySqlCommand query = new MySqlCommand("select p.email from pessoa p where p.id = @id;", con);
                // Definindo os valores dos parâmetros passados no comando MySql
                query.Parameters.AddWithValue("@id", id);

                // Guardando o retorno do comando efetuado
                MySqlDataReader reader = query.ExecuteReader();

                if (reader.Read())
                {
                    receptionist = new Receptionist
                    {
                        Email = Convert.ToString(reader["email"])
                    };
                }
                reader.Close();

                if (!receptionist.Equals(null))
                {
                    receptionist.list_telephones = CatchMobilePhone(id);
                }

                con.Close();
            }
            catch (Exception error)
            {
                string errorMessage = error.Message;
                receptionist = null;
                con.Close();
            }

            return receptionist;
        }
        #endregion
        #endregion

        #region account_manipulation
        #region register_receptionist
        // --Método desenvolvido para cadastrar um(a) novo(a) recepcionista no sistema--
        public bool Register()
        {
            // Declaração de variável
            bool result = false;
            // Cadastrando o endereço do usuário, caso necessário, e recebendo o valor do id do mesmo
            endereco.Id = endereco.GetAddress();

            try
            {
                // Abrindo conexão com o banco de dados
                if (con.State == ConnectionState.Closed)
                    con.Open();

                // Comando MySql desenvolvido para adicionar um registro na tabela 'pessoa' do banco de dados
                MySqlCommand query = new MySqlCommand("insert into pessoa(id, cpf, nome, sobrenome, data_nascimento, sexo, foto, email, senha, nivel_acesso, endereco) " +
                    "values(@id, @cpf, @nome, @sobrenome, @data_nascimento, @sexo, @foto, @email, aes_encrypt(@senha, @cryptographyKey), @nivel_acesso, @endereco)", con);
                // Definindo os valores dos parâmetros passados no comando MySql
                query.Parameters.AddWithValue("@id", id);
                query.Parameters.AddWithValue("@cpf", cpf);
                query.Parameters.AddWithValue("@nome", nome);
                query.Parameters.AddWithValue("@sobrenome", sobrenome);
                query.Parameters.AddWithValue("@data_nascimento", data_nascimento);
                query.Parameters.AddWithValue("@sexo", sexo);
                query.Parameters.AddWithValue("@foto", foto);
                query.Parameters.AddWithValue("@email", email);
                query.Parameters.AddWithValue("@senha", senha);
                query.Parameters.AddWithValue("@cryptographyKey", ConfigurationManager.ConnectionStrings["cryptographyKey"].ConnectionString);
                query.Parameters.AddWithValue("@nivel_acesso", nivel_acesso);
                query.Parameters.AddWithValue("@endereco", endereco.Id);

                // Executando o comando MySQL elaborado
                query.ExecuteNonQuery();

                // Cadastrando os telefones do usuário, caso necessário, e conectando-os ao mesmo no banco de dados
                foreach (Telephone telephone in list_telephones)
                {
                    telephone.Register_user_telephone(id);
                }

                result = true;

                con.Close();
            }
            catch (Exception error)
            {
                string errorMessage = error.Message;
                result = false;
                con.Close();
            }

            return result;
        }
        #endregion

        #region view_profile
        // --Método desenvolvido para retornar os dados do administrador autenticado no sistema--
        public static Receptionist GetInfo(string id)
        {
            // Declaração de variável
            Receptionist receptionist = null;

            try
            {
                // Abrindo conexão com o banco de dados
                if (con.State == System.Data.ConnectionState.Closed)
                    con.Open();

                // Comando MySql desenvolvido para selecionar os dados do usuário autenticado
                MySqlCommand query = new MySqlCommand("select p.id, p.cpf, p.nome, p.sobrenome, p.data_nascimento, p.sexo, p.foto, p.email, cast(aes_decrypt(p.senha, @cryptographyKey) as char) as senha, p.nivel_acesso, " +
                    "e.cep, e.estado, e.cidade, e.bairro, e.rua, e.numero, e.complemento " +
                    "from pessoa p, endereco e " +
                    "where p.id = @id and e.id = p.endereco;", con);
                // Definindo os valores dos parâmetros passados no comando MySql
                query.Parameters.AddWithValue("@cryptographyKey", ConfigurationManager.ConnectionStrings["cryptographyKey"].ConnectionString);
                query.Parameters.AddWithValue("@id", id);

                // Guardando o retorno do comando efetuado
                MySqlDataReader reader = query.ExecuteReader();

                if (reader.Read())
                {
                    receptionist = new Receptionist
                    {
                        Id = Convert.ToString(reader["id"]),
                        Cpf = Convert.ToString(reader["cpf"]),
                        Nome = Convert.ToString(reader["nome"]),
                        Sobrenome = Convert.ToString(reader["sobrenome"]),
                        Data_nascimento = Convert.ToDateTime(reader["data_nascimento"]),
                        Sexo = Convert.ToString(reader["sexo"]),
                        Email = Convert.ToString(reader["email"]),
                        Senha = Convert.ToString(reader["senha"]),
                        Nivel_acesso = Convert.ToString(reader["nivel_acesso"])
                    };

                    try
                    {
                        if (reader["foto"] != DBNull.Value)
                            receptionist.foto = (byte[])reader["foto"];
                    }
                    catch (Exception error)
                    {
                        string errorMessage = error.Message;
                    }

                    receptionist.Endereco.Cep = Convert.ToString(reader["cep"]);
                    receptionist.Endereco.Estado = Convert.ToString(reader["estado"]);
                    receptionist.Endereco.Cidade = Convert.ToString(reader["cidade"]);
                    receptionist.Endereco.Bairro = Convert.ToString(reader["bairro"]);
                    receptionist.Endereco.Rua = Convert.ToString(reader["rua"]);
                    receptionist.Endereco.Numero = Convert.ToString(reader["numero"]);
                    receptionist.Endereco.Complemento = Convert.ToString(reader["complemento"]);
                }
                reader.Close();

                if (!receptionist.Equals(null))
                {
                    receptionist.list_telephones = Person.CatchTelephone(id);
                }

                con.Close();
            }
            catch (Exception error)
            {
                string errorMessage = error.Message;
                receptionist = null;
                con.Close();
            }

            return receptionist;
        }
        #endregion

        #region list_receptionists
        // --Método desenvolvido para retornar lista populada com usuários que possuem nível de acesso igual a "Recepcionista"--
        public static List<Receptionist> List_receptionists(string hospital)
        {
            // Declaração de variável
            List<Receptionist> receptionists = new List<Receptionist>();

            try
            {
                // Abrindo conexão com o banco de dados
                if (con.State == ConnectionState.Closed)
                    con.Open();

                // Comando MySql desenvolvido para selecionar os usuários que possuem nível de acesso igual a "Recepcionista"
                MySqlCommand query = new MySqlCommand("select p.id, p.cpf, p.nome, p.sobrenome, p.data_nascimento, p.sexo " +
                    "from pessoa p, funcionario_hospital f_h where p.nivel_acesso = @user_type and f_h.funcionario = p.id and f_h.hospital = @cnes;", con);
                // Definindo os valores dos parâmetros passados no comando MySql
                query.Parameters.AddWithValue("@user_type", "Recepcionista");
                query.Parameters.AddWithValue("@cnes", hospital);

                // Guardando o retorno do comando efetuado
                MySqlDataReader reader = query.ExecuteReader();

                while (reader.Read())
                {
                    Receptionist receptionist = new Receptionist
                    {
                        Id = Convert.ToString(reader["id"]),
                        Cpf = Convert.ToString(reader["cpf"]),
                        Nome = Convert.ToString(reader["nome"]),
                        Sobrenome = Convert.ToString(reader["sobrenome"]),
                        Data_nascimento = Convert.ToDateTime(reader["data_nascimento"]),
                        Sexo = Convert.ToString(reader["sexo"])
                    };

                    receptionists.Add(receptionist);
                }

                con.Close();
            }
            catch (Exception error)
            {
                string errorMessage = error.Message;
                receptionists = null;
                con.Close();
            }

            return receptionists;
        }
        #endregion
        
        //EXEMPLO PARA EDITAR OS Recepcionista
        public string Editar()
        {
            return null;
        }
        
        //---------------------------------METODO REMOVER-------------------------------------//
        public string Remover()
        {
            string mensagem = "Recepcionista removido com sucesso!";
            try
            {
                con.Open();
                MySqlCommand query = new MySqlCommand("DELETE from pessoa WHERE id = @id", con);
                query.Parameters.AddWithValue("@id", id);
                query.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                mensagem = e.Message;
            }

            con.Close();
            return mensagem;
        }
        #endregion

        //public string Editar()
        //{
        //    try
        //    {
        //        con.Open();

        //        MySqlCommand query = new MySqlCommand("UPDATE pessoa SET id = @id, cpf = @cpf, nome = @nome, sobrenome = @sobrenome, " +
        //                         "data_nascimento = @data_nascimento, sexo = @sexo, foto = @foto, email = @email, " +
        //                         "nivel_acesso = @nivel_acesso, nome_mae = @nome_mae, nome_pai = @nome_pai, area_atuacao = @area_atuacao WHERE id = @id", con);


        //        //INSERE OS VALORES DO OBJETO AO QUERY
        //        query.Parameters.AddWithValue("@id", id);
        //        query.Parameters.AddWithValue("@cpf", cpf);
        //        query.Parameters.AddWithValue("@nome", nome);
        //        query.Parameters.AddWithValue("@sobrenome", sobrenome);
        //        query.Parameters.AddWithValue("@data_nascimento", data_nascimento);
        //        query.Parameters.AddWithValue("@sexo", sexo);
        //        query.Parameters.AddWithValue("@foto", foto);
        //        query.Parameters.AddWithValue("@email", email);
        //        query.Parameters.AddWithValue("@nivel_acesso", nivel_acesso);
        //        query.Parameters.AddWithValue("@nome_mae", nome_mae);
        //        query.Parameters.AddWithValue("@nome_pai", nome_pai);
        //        query.Parameters.AddWithValue("@area_atuacao", area_atuacao);

        //        query.ExecuteNonQuery();
        //    }
        //    catch (Exception erro)
        //    {
        //        con.Close();
        //        return erro.Message;
        //    }

        //    if (con.State == System.Data.ConnectionState.Open)
        //        con.Close();

        //    return "Usuário modificado com sucesso!";
        //}

        //public string Remover()
        //{
        //    if (nivel_acesso.ToLower().Equals("paciente"))
        //        return "Paciente não pode ser excluido!";
        //    try
        //    {
        //        con.Open();

        //        MySqlCommand query = new MySqlCommand("DELETE FROM pessoa WHERE id = @id", con);
        //        query.Parameters.AddWithValue("@id", id);
        //        query.ExecuteNonQuery();
        //    }
        //    catch (Exception erro)
        //    {
        //        con.Close();
        //        return erro.Message;
        //    }

        //    if (con.State == ConnectionState.Open)
        //        con.Close();
        //    return "Excluido com sucesso!";
        //}
        #endregion
    }
}