﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;

namespace SIS.Models.Users
{
    public class Person
    {
        /** DECLARAÇÃO DOS ATRIBUTOS DA CLASSE **/
        #region parameters
        // --Variável declarada para conexão com o banco de  dados--
        protected static MySqlConnection con = new MySqlConnection(ConfigurationManager.ConnectionStrings["BCD"].ConnectionString);
        // --Atributos da tabela 'pessoa' que são comuns entre os diferentes níveis de acesso existentes--
        protected string id, cpf, nome, sobrenome, email, senha;
        protected DateTime data_nascimento;
        protected byte[] foto;
        // --Atributos da tabela 'sexo'--
        protected int sexo_id;
        protected string sexo_descricao;
        // --Atributos da tabela 'nivel_acesso'--
        protected int nivel_acesso_id;
        protected string nivel_acesso_descricao;
        // --Objetos instanciados para ligação entre as tabelas no banco de dados--
        protected Address endereco = new Address();
        protected List<Telephone> list_telephones = new List<Telephone>();
        protected Telephone telefone = new Telephone();

        /** Como nenhum dos atributos pode ser acessado ou modificado diretamente por alguma outra classe, criam-se 
        variavéis que serão as pontes para acessá-los e modificá-los (Getters e Setters)**/
        // --Atributos da tabela 'pessoa'--
        [Required(ErrorMessage = "Obrigatório informar o código de identificação")]
        [StringLength(13, ErrorMessage = "O código de identificação deve possuir no máximo 13 caracteres")]
        [Display(Name = "ID")]
        public string Id
        {
            get { return id; } // Devolve o valor do atributo 'id'
            set { id = value; } // Modifica o valor do atributo 'id'
        }

        [Required(ErrorMessage = "Obrigatório informar o CPF")]
        [StringLength(14, ErrorMessage = "O CPF deve possuir no máximo 14 caracteres")]
        [Display(Name = "CPF")]
        public string Cpf
        {
            get { return cpf; }
            set { cpf = value; }
        }

        [Required(ErrorMessage = "Obrigatório informar o nome")]
        [StringLength(30, ErrorMessage = "O nome deve possuir no máximo 30 caracteres")]
        [Display(Name = "Nome")]
        public string Nome
        {
            get { return nome; }
            set { nome = value; }
        }

        [Required(ErrorMessage = "Obrigatório informar o sobrenome")]
        [StringLength(80, ErrorMessage = "O sobrenome deve possuir no máximo 80 caracteres")]
        [Display(Name = "Sobrenome")]
        public string Sobrenome
        {
            get { return sobrenome; }
            set { sobrenome = value; }
        }

        [Required(ErrorMessage = "Obrigatório informar a data de nascimento")]
        [StringLength(10, ErrorMessage = "A data de nascimento deve possuir no máximo 10 caracteres")]
        [Display(Name = "Data de nascimento")]
        public DateTime Data_nascimento
        {
            get { return data_nascimento; }
            set { data_nascimento = value; }
        }

        public string Data_nascimento_Formatada
        {
            get { return data_nascimento.Day + "/" + data_nascimento.Month + "/" + data_nascimento.Year; }
        }

        [Required(ErrorMessage = "Obrigatório informar o sexo")]
        [StringLength(10, ErrorMessage = "O sexo deve possuir no máximo 10 caracteres")]
        [Display(Name = "Sexo")]
        public string Sexo_descricao
        {
            get { return sexo_descricao; }
            set { sexo_descricao = value; }
        }

        [Display(Name = "Foto de perfil")]
        public byte[] Foto
        {
            get { return getImage(); }
            set { foto = ResizeImageToDefault(value, 512, 512); }
        }

        [Required(ErrorMessage = "Obrigatório informar um endereço de e-mail")]
        [StringLength(100, ErrorMessage = "O e-mail deve possuir no máximo 100 caracteres")]
        [Display(Name = "E-mail")]
        public string Email
        {
            get { return email; }
            set { email = value; }
        }

        [Required(ErrorMessage = "Obrigatório informar uma senha")]
        [StringLength(30, ErrorMessage = "A senha deve possuir no máximo 30 caracteres")]
        [Display(Name = "Senha")]
        public string Senha
        {
            get { return senha; }
            set { senha = value; }
        }

        public string Nivel_acesso
        {
            get
            {
                if (nivel_acesso == null)
                    nivel_acesso = "";
                return nivel_acesso;
            }
            set { nivel_acesso = value; }
        }

        // --Objeto instanciado para ligação entre as tabelas no banco de dados--
        public Address Endereco
        {
            get { return endereco; }
            set { endereco = value; }
        }

        public string Endereco_Formatado
        {
            get { return endereco.Rua + ", " + endereco.Numero + " - " + endereco.Bairro + "," + endereco.Cidade; }
        }

        public List<Telephone> ListaTelefone
        {
            get { return list_telephones; }
            set { list_telephones = value; }
        }

        public Telephone Telefone
        {
            get { return telefone; }
            set { telefone = value; }
        }
        #endregion

        /** MÉTODOS DA CLASSE **/
        #region methods

        #region recover_password
        #region get_contact_info
        // --Método desenvolvido para retornar os dados de contato do usuário--
        public static Person GetContactData(string id)
        {
            // Declaração de variável
            Person result = null;

            try
            {
                // Abrindo conexão com o banco de dados
                if (con.State == ConnectionState.Closed)
                    con.Open();

                // Comando MySql desenvolvido para selecionar o usuário que possui as especificações recebidas
                MySqlCommand query = new MySqlCommand("select * from pessoa where id = @id;", con);
                // Definindo os valores dos parâmetros passados no comando MySql
                query.Parameters.AddWithValue("@id", id);

                // Guardando o retorno do comando efetuado
                MySqlDataReader reader = query.ExecuteReader();

                if (reader.Read())
                {
                    switch (int.Parse(Convert.ToString(reader["nivel_acesso"])))
                    {
                        case 1:
                            reader.Close();
                            result = Administrator.GetContactInfo(id);
                            break;

                        case 2:
                            reader.Close();
                            result = Receptionist.GetContactInfo(id);
                            break;

                        case 3:
                            reader.Close();
                            result = Doctor.GetContactInfo(id);
                            break;

                        case 4:
                            reader.Close();
                            result = Patient.GetContactInfo(id);
                            break;
                    }
                }

                if (reader.IsClosed == false)
                    reader.Close();
            }

            catch (Exception error)
            {
                string errorMsg = error.Message;
                con.Close();
                return null;
            }

            if (con.State == ConnectionState.Open)
                con.Close();

            return result;
        }

        public static List<Telephone> CatchMobilePhone(string id)
        {
            // Declaração de variável
            List<Telephone> listaTelefone = new List<Telephone>();

            try
            {
                // Abrindo conexão com o banco de dados
                if (con.State == ConnectionState.Closed)
                    con.Open();

                // Comando MySql desenvolvido para selecionar os telefones móveis do usuário informado
                MySqlCommand query = new MySqlCommand("select t.numero from pessoa_telefone p_t, telefone t " +
                    "where p_t.pessoa = @id and t.id = p_t.telefone and t.tipo = @type;", con);
                // Definindo os valores dos parâmetros passados no comando MySql
                query.Parameters.AddWithValue("@id", id);
                query.Parameters.AddWithValue("@type", 1);

                // Guardando o retorno do comando efetuado
                MySqlDataReader reader = query.ExecuteReader();

                while (reader.Read())
                {
                    Telephone telephone = new Telephone
                    {
                        Numero = Convert.ToString(reader["numero"]),
                    };

                    listaTelefone.Add(telephone);
                }

                con.Close();
            }
            catch (Exception error)
            {
                string errorMessage = error.Message;
                listaTelefone = null;
                con.Close();
            }

            return listaTelefone;
        }
        #endregion

        #region code_generation
        // --Método desenvolvido para gerar código de 7 dígitos e criar, ou atualizar, registro na tabela 'codigo_pessoa' para esse usuário em específico--
        public static string GetCode(string username)
        {
            /** DECLARAÇÃO DE VARIÁVEIS **/
            // Criando objeto da classe Person com o id do usuário que requeriu o código de verificação de identidade
            Person user = new Person
            {
                Id = username
            };

            /** Geração de código de 7 dígitos com letras e números aleatórios **/
            // Declarando e inicializando string que conterá os caracteres disponíveis para geração do código
            string characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            // Declarando string que conterá o código aleatório a ser gerado
            string code = "";
            // Gerando código randômico
            Random random = new Random();
            code = new string(
                Enumerable.Repeat(characters, 7)
                  .Select(s => s[random.Next(s.Length)])
                  .ToArray());

            /** DESENVOLVIMENTO DO PROCESSO **/
            // Se o usuário já possui um código registrado, atualize o valor do código e da data em que foi gerado
            if (user.CheckExistenceCodeRecord())
            {
                user.UpdateCodeRecord(code);
            }
            // Senão crie um novo registro na tabela 'codigo_pessoa' para esse usuário em específico
            else
            {
                user.CreateCodeRecord(code);
            }

            return code;
        }

        // --Método desenvolvido para checar se um usuário em específicio possui registro na tabela 'codigo_pessoa'--
        private bool CheckExistenceCodeRecord()
        {
            // Declaração de variável
            bool result;

            try
            {
                // Abrindo conexão com o banco de dados
                if (con.State == ConnectionState.Closed)
                    con.Open();

                MySqlCommand query = new MySqlCommand("select * from codigo_pessoa WHERE usuario = @username;", con);
                // Definindo os valores dos parâmetros passados no comando
                query.Parameters.AddWithValue("@username", id);

                //FAZ EXECUÇÃO DOS VALORES NO BANCO
                MySqlDataReader reader = query.ExecuteReader();
                reader.Read();
                result = reader.HasRows;
                reader.Close();
            }

            catch (Exception error)
            {
                string errorMsg = error.Message; //USAR BREAKPOINT PARA VISUALIZAR O ERRO
                result = false;
            }

            if (con.State == ConnectionState.Open)
                con.Close();

            return result;
        }

        // --Método desenvolvido para inserir novo registro na tabela 'codigo_pessoa'--
        private void CreateCodeRecord(string code)
        {
            try
            {
                // Abrindo conexão com o banco de dados
                if (con.State == ConnectionState.Closed)
                    con.Open();

                // Comando MySql desenvolvido para inserir novo registro na tabela 'codigo_pessoa'
                MySqlCommand query = new MySqlCommand("insert into codigo_pessoa(usuario, codigo) values(@username, @code);", con);
                // Definindo os valores dos parâmetros passados no comando MySql
                query.Parameters.AddWithValue("@username", id);
                query.Parameters.AddWithValue("@code", code);

                // Efetuando o comando MySql desenvolvido acima
                query.ExecuteNonQuery();
            }

            catch (Exception error)
            {
                string errorMsg = error.Message;
            }

            if (con.State == ConnectionState.Open)
                con.Close();
        }

        // --Método desenvolvido para atualizar registro presente na tabela 'codigo_pessoa'--
        private void UpdateCodeRecord(string code)
        {
            try
            {
                // Abrindo conexão com o banco de dados
                if (con.State == ConnectionState.Closed)
                    con.Open();

                // Comando MySql desenvolvido para atualizar registro presente na tabela 'codigo_pessoa'
                MySqlCommand query = new MySqlCommand("update codigo_pessoa set codigo = @code, data = @date where usuario = @username;", con);
                // Definindo os valores dos parâmetros passados no comando MySql
                query.Parameters.AddWithValue("@username", id);
                query.Parameters.AddWithValue("@code", code);
                query.Parameters.AddWithValue("@date", DateTime.Now.ToString("yyyy-MM-dd HH:mm"));

                // Efetuando o comando MySql desenvolvido acima
                query.ExecuteNonQuery();
            }

            catch (Exception error)
            {
                string errorMsg = error.Message;
            }

            if (con.State == ConnectionState.Open)
                con.Close();
        }
        #endregion

        #region code_validation
        // --Método desenvolvido para validar o código informado pelo usuário--
        public static bool[,] ValidateCode(string username, string informedCode)
        {
            // Declaração de variável
            bool[,] result = new bool[1, 2]; // a primeira posição da matriz é destinada a armazenar o valor booleano referente ao tempo de validade 
                                             // do último código gerado a esse usuário e a segunda posição é destinada a armazenar o valor booleano 
                                             // referente a validade do código informado pelo usuário

            try
            {
                // Abrindo conexão com o banco de dados
                if (con.State == ConnectionState.Closed)
                    con.Open();

                MySqlCommand query = new MySqlCommand("select * from codigo_pessoa where usuario = @username", con);
                // Definindo os valores dos parâmetros passados no comando
                query.Parameters.AddWithValue("@username", username);

                //FAZ EXECUÇÃO DOS VALORES NO BANCO
                MySqlDataReader reader = query.ExecuteReader();

                if (reader.Read())
                {
                    DateTime date = Convert.ToDateTime(reader["data"]);
                    DateTime deadline = date.AddMinutes(20);
                    if (DateTime.Now >= deadline)
                    {
                        result[0, 0] = false; // o tempo de validade do último código gerado esgotou-se
                    }
                    else
                    {
                        result[0, 0] = true; // o tempo de validade do último código gerado ainda não esgotou-se

                        if (!informedCode.Equals(Convert.ToString(reader["codigo"])))
                            result[0, 1] = false; // o código informado pelo usuário é inválido
                        else
                            result[0, 1] = true; // o código informado pelo usuário é válido
                    }
                }
                else
                {
                    result[0, 0] = true;
                    result[0, 1] = false;
                }

                reader.Close();
            }

            catch (Exception error)
            {
                string errorMsg = error.Message; //USAR BREAKPOINT PARA VISUALIZAR O ERRO
                result[0, 0] = false;
            }

            if (con.State == ConnectionState.Open)
                con.Close();

            return result;
        }
        #endregion

        #region redefine_password
        // --Método desenvolvido para alterar a atual senha do usuário--
        public void ChangePassword(string newPassword)
        {
            try
            {
                // Abrindo conexão com o banco de dados
                if (con.State == ConnectionState.Closed)
                    con.Open();

                // Comando MySql desenvolvido para alterar a senha de um usuário presente na tabela 'pessoa'
                MySqlCommand query = new MySqlCommand("update pessoa set senha = aes_encrypt(@newPassword, @cryptographyKey) where id = @id;", con);
                // Definindo os valores dos parâmetros passados no comando MySql
                query.Parameters.AddWithValue("@id", id);
                query.Parameters.AddWithValue("@newPassword", newPassword);
                query.Parameters.AddWithValue("@cryptographyKey", ConfigurationManager.ConnectionStrings["cryptographyKey"].ConnectionString);

                // Efetuando o comando MySql desenvolvido acima
                query.ExecuteNonQuery();
            }

            catch (Exception error)
            {
                string errorMsg = error.Message;
            }

            if (con.State == ConnectionState.Open)
                con.Close();
        }
        #endregion
        #endregion

        #region log_in
        // --Método de autenticação de usuário--
        public static Person Login(string login, string password)
        {
            // Declaração de variável
            Person result = null;

            try
            {
                // Abrindo conexão com o banco de dados
                if (con.State == ConnectionState.Closed)
                    con.Open();

                // Comando MySql desenvolvido para selecionar o usuário que possui as especificações recebidas
                MySqlCommand query = new MySqlCommand("select * from pessoa where id = @login and senha = aes_encrypt(@password, @cryptographyKey);", con);
                // Definindo os valores dos parâmetros passados no comando MySql
                query.Parameters.AddWithValue("@login", login);
                query.Parameters.AddWithValue("@password", password);
                query.Parameters.AddWithValue("@cryptographyKey", ConfigurationManager.ConnectionStrings["cryptographyKey"].ConnectionString);

                // Guardando o retorno do comando efetuado
                MySqlDataReader reader = query.ExecuteReader();

                if (reader.Read())
                {
                    switch (reader.GetString("nivel_acesso"))
                    {
                        case "Administrador":
                            reader.Close();
                            result = Administrator.GetInfo(login);
                            break;

                        case "Médico":
                            reader.Close();
                            result = Doctor.GetInfo(login);
                            break;

                        case "Recepcionista":
                            reader.Close();
                            result = Receptionist.GetInfo(login);
                            break;

                        case "Paciente":
                            reader.Close();
                            result = Patient.GetInfo(login);
                            break;
                    }
                }

                if (reader.IsClosed == false)
                    reader.Close();
            }

            catch (Exception error)
            {
                string errorMsg = error.Message;
                con.Close();
                result = null;
            }

            if (con.State == ConnectionState.Open)
                con.Close();

            return result;
        }
        #endregion

        #region account_manipulation
        #region view_profile
        public static List<Telephone> CatchTelephone(string id)
        {
            // Declaração de variável
            List<Telephone> listaTelefone = new List<Telephone>();

            try
            {
                // Abrindo conexão com o banco de dados
                if (con.State == ConnectionState.Closed)
                    con.Open();

                // Comando MySql desenvolvido para selecionar os telefones do usuário autenticado
                MySqlCommand query = new MySqlCommand("select t.numero, t.tipo from pessoa_telefone p_t, telefone t " +
                    "where p_t.pessoa = @id and t.id = p_t.telefone;", con);
                // Definindo os valores dos parâmetros passados no comando MySql
                query.Parameters.AddWithValue("@id", id);

                // Guardando o retorno do comando efetuado
                MySqlDataReader reader = query.ExecuteReader();

                while (reader.Read())
                {
                    Telephone telephone = new Telephone
                    {
                        Numero = Convert.ToString(reader["numero"]),
                        Tipo = Convert.ToString(reader["tipo"])
                    };

                    listaTelefone.Add(telephone);
                }

                con.Close();
            }
            catch (Exception error)
            {
                string errorMessage = error.Message;
                listaTelefone = null;
                con.Close();
            }

            return listaTelefone;
        }
        #endregion
        #endregion

        // --Método desenvolvido para validar a existência do usuário--
        public static bool CheckExistenceUser(string id)
        {
            // Declaração de variável
            bool result;

            try
            {
                // Abrindo conexão com o banco de dados
                if (con.State == ConnectionState.Closed)
                    con.Open();

                // Comando MySql desenvolvido para selecionar o usuário que possui as especificações recebidas
                MySqlCommand query = new MySqlCommand("select * from pessoa where id = @id;", con);
                // Definindo os valores dos parâmetros passados no comando MySql
                query.Parameters.AddWithValue("@id", id);

                // Guardando o retorno do comando efetuado
                MySqlDataReader reader = query.ExecuteReader();

                if (reader.Read())
                    result = true;
                else
                    result = false;

                if (reader.IsClosed == false)
                    reader.Close();
            }

            catch (Exception error)
            {
                string errorMsg = error.Message;
                con.Close();
                result = false;
            }

            if (con.State == ConnectionState.Open)
                con.Close();

            return result;
        }

        private byte[] getImage()
        {
            byte[] resultado;
            MySqlConnection conexao = new MySqlConnection(ConfigurationManager.ConnectionStrings["BCD"].ConnectionString);
            try
            {

                // Abrindo conexão com o banco de dados
                if (conexao.State == ConnectionState.Closed)
                    conexao.Open();


                MySqlCommand query = new MySqlCommand("SELECT pessoa.foto FROM pessoa WHERE id = @id", conexao);
                query.Parameters.AddWithValue("@id", id);
                MySqlDataReader reader = query.ExecuteReader();

                reader.Read();
                resultado = (byte[])reader["foto"];

                reader.Close();
                conexao.Close();
            }
            catch (Exception error)
            {
                string errorMessage = error.Message;
                resultado = null;
                conexao.Close();
            }

            return resultado;
        }

        public static string GetIdByName(string nome)
        {
            MySqlConnection conexao = new MySqlConnection(ConfigurationManager.ConnectionStrings["BCD"].ConnectionString);
            string resultado = "";
            try
            {
                if (conexao.State == ConnectionState.Closed)
                    conexao.Open();

                MySqlCommand query = new MySqlCommand("SELECT pessoa.id FROM pessoa WHERE CONCAT(nome, ' ', sobrenome) = @nome", conexao);
                query.Parameters.AddWithValue("@nome", nome);
                MySqlDataReader reader = query.ExecuteReader();

                reader.Read();
                resultado = reader["id"].ToString();

                conexao.Close();
            }
            catch (Exception error)
            {
                string errorMessage = error.Message;
                resultado = "";
                conexao.Close();
            }

            return resultado;
        }

        public static string GetTypeByName(string nome)
        {
            string resultado = "";

            try
            {
                if (con.State == ConnectionState.Closed)
                    con.Open();

                MySqlCommand query = new MySqlCommand("SELECT pessoa.nivel_acesso FROM pessoa WHERE CONCAT(nome, ' ', sobrenome) = @nome", con);
                query.Parameters.AddWithValue("@nome", nome);
                MySqlDataReader reader = query.ExecuteReader();

                reader.Read();
                resultado = reader["nivel_acesso"].ToString();

                con.Close();
            }
            catch (Exception error)
            {
                string errorMessage = error.Message;
                resultado = "";
                con.Close();
            }

            return resultado;
        }

        private byte[] ResizeImageToDefault(byte[] imageEmBytes, int width, int height)
        {
            Image image = Image.FromStream(new MemoryStream(imageEmBytes));

            var destRect = new Rectangle(0, 0, width, height);
            var destImage = new Bitmap(width, height);

            destImage.SetResolution(image.HorizontalResolution, image.VerticalResolution);

            using (var graphics = Graphics.FromImage(destImage))
            {
                graphics.CompositingMode = CompositingMode.SourceCopy;
                graphics.CompositingQuality = CompositingQuality.HighQuality;
                graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphics.SmoothingMode = SmoothingMode.HighQuality;
                graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;

                using (var wrapMode = new ImageAttributes())
                {
                    wrapMode.SetWrapMode(WrapMode.TileFlipXY);
                    graphics.DrawImage(image, destRect, 0, 0, image.Width, image.Height, GraphicsUnit.Pixel, wrapMode);
                }
            }

            MemoryStream ms = new MemoryStream();
            destImage.Save(ms, ImageFormat.Png);
            return ms.ToArray();
        }

        public static string GetPassword(string id)
        {
            MySqlConnection conexao = new MySqlConnection(ConfigurationManager.ConnectionStrings["BCD"].ConnectionString);
            string resultado = "";
            try
            {
                conexao.Open();

                MySqlCommand query = new MySqlCommand("select cast(aes_decrypt(senha, @cryptographyKey) as char) as senha from pessoa where id = @id", conexao);
                query.Parameters.AddWithValue("@id", id);
                MySqlDataReader reader = query.ExecuteReader();

                if (reader.Read())
                    resultado = reader["senha"].ToString();

                conexao.Close();
            }
            catch (Exception error)
            {
                string errorMessage = error.Message;
                resultado = null;
                conexao.Close();
            }

            return resultado;
        }
        #endregion
    }
}