﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;

namespace SIS.Models.Users
{
    public class Administrator : Person
    {
        /** MÉTODOS DA CLASSE **/
        #region methods

        #region recover_password
        #region get_contact_info
        // --Método desenvolvido para retornar as informações de contato do usuário--
        public static Administrator GetContactInfo(string id)
        {
            // Declaração de variável
            Administrator administrator = null;

            try
            {
                // Abrindo conexão com o banco de dados
                if (con.State == ConnectionState.Closed)
                    con.Open();

                // Comando MySql desenvolvido para selecionar os dados do usuário informado
                MySqlCommand query = new MySqlCommand("select p.email from pessoa p where p.id = @id;", con);
                // Definindo os valores dos parâmetros passados no comando MySql
                query.Parameters.AddWithValue("@id", id);

                // Guardando o retorno do comando efetuado
                MySqlDataReader reader = query.ExecuteReader();

                if (reader.Read())
                {
                    administrator = new Administrator
                    {
                        Email = Convert.ToString(reader["email"])
                    };
                }
                reader.Close();

                if (!administrator.Equals(null))
                {
                    administrator.list_telephones = CatchMobilePhone(id);
                }

                con.Close();
            }
            catch (Exception error)
            {
                string errorMessage = error.Message;
                administrator = null;
                con.Close();
            }

            return administrator;
        }
        #endregion
        #endregion

        #region account_manipulation
        #region view_profile
        // --Método desenvolvido para retornar os dados do administrador autenticado no sistema--
        public static Administrator GetInfo(string id)
        {
            // Declaração de variável
            Administrator administrator = null;

            try
            {
                // Abrindo conexão com o banco de dados
                if (con.State == ConnectionState.Closed)
                    con.Open();

                // Comando MySql desenvolvido para selecionar os dados do usuário autenticado
                MySqlCommand query = new MySqlCommand("select p.id, p.cpf, p.nome, p.sobrenome, p.data_nascimento, p.sexo, p.foto, p.email, cast(aes_decrypt(p.senha, @cryptographyKey) as char) as senha, p.nivel_acesso, " +
                    "e.cep, e.estado, e.cidade, e.bairro, e.rua, e.numero, e.complemento " +
                    "from pessoa p, endereco e " +
                    "where p.id = @id and e.id = p.endereco;", con);
                // Definindo os valores dos parâmetros passados no comando MySql
                query.Parameters.AddWithValue("@id", id);
                query.Parameters.AddWithValue("@cryptographyKey", ConfigurationManager.ConnectionStrings["cryptographyKey"].ConnectionString);

                // Guardando o retorno do comando efetuado
                MySqlDataReader reader = query.ExecuteReader();

                if (reader.Read())
                {
                    administrator = new Administrator
                    {
                        Id = Convert.ToString(reader["id"]),
                        Cpf = Convert.ToString(reader["cpf"]),
                        Nome = Convert.ToString(reader["nome"]),
                        Sobrenome = Convert.ToString(reader["sobrenome"]),
                        Data_nascimento = Convert.ToDateTime(reader["data_nascimento"]),
                        Sexo = Convert.ToString(reader["sexo"]),
                        Email = Convert.ToString(reader["email"]),
                        Senha = Convert.ToString(reader["senha"]),
                        Nivel_acesso = Convert.ToString(reader["nivel_acesso"])
                    };

                    try
                    {
                        administrator.Foto = (byte[])reader["foto"];
                    }
                    catch (Exception error)
                    {
                        string errorMessage = error.Message;
                    }

                    administrator.Endereco.Cep = Convert.ToString(reader["cep"]);
                    administrator.Endereco.Estado = Convert.ToString(reader["estado"]);
                    administrator.Endereco.Cidade = Convert.ToString(reader["cidade"]);
                    administrator.Endereco.Bairro = Convert.ToString(reader["bairro"]);
                    administrator.Endereco.Rua = Convert.ToString(reader["rua"]);
                    administrator.Endereco.Numero = Convert.ToString(reader["numero"]);
                    administrator.Endereco.Complemento = Convert.ToString(reader["complemento"]);
                }
                reader.Close();

                if (!administrator.Equals(null))
                {
                    administrator.list_telephones = Person.CatchTelephone(id);
                }

                con.Close();
            }
            catch (Exception error)
            {
                string errorMessage = error.Message;
                administrator = null;
                con.Close();
            }

            return administrator;
        }
        #endregion
        #endregion

        #endregion
    }
}