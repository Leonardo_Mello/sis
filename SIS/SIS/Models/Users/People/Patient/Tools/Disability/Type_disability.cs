﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Data;

namespace SIS.Models.Users
{
    public class Type_disability
    {
        /** DECLARAÇÃO DOS ATRIBUTOS DA CLASSE **/
        #region parameters
        // Variável declarada para conexão com o banco de  dados
        private static MySqlConnection con = new MySqlConnection(ConfigurationManager.ConnectionStrings["BCD"].ConnectionString);
        // Atributos da tabela 'tipo_deficiencia'
        private int id;
        private string descricao;

        /** Como nenhum dos atributos pode ser acessado ou modificado diretamente por alguma outra classe, criam-se 
        variavéis que serão as pontes para acessá-los e modificá-los (Getters e Setters)**/
        // --Atributos da tabela 'tipo_deficiencia'--
        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        [Required(ErrorMessage = "Obrigatório informar o tipo de deficiência")]
        [StringLength(150, ErrorMessage = "A descrição do tipo de deficiência deve possuir no máximo 150 caracteres")]
        [Display(Name = "Tipo de deficiência")]
        public string Descricao
        {
            get { return descricao; }
            set { descricao = value; }
        }
        #endregion

        /** MÉTODOS DA CLASSE **/
        #region methods
        // --Método desenvolvido para retornar lista populada com todos os tipos de deficiências existentes no banco de dados--
        public static List<Type_disability> List_types_disability()
        {
            // Declaração de variável
            List<Type_disability> types_disability = new List<Type_disability>();

            try
            {
                // Abrindo conexão com o banco de dados
                if (con.State == ConnectionState.Closed)
                    con.Open();

                // Comando MySql desenvolvido para selecionar todas as deficiências existentes no banco de dados
                MySqlCommand query = new MySqlCommand("select * from tipo_deficiencia;", con);

                // Guardando o retorno do comando efetuado
                MySqlDataReader reader = query.ExecuteReader();

                while (reader.Read())
                {
                    Type_disability type_disability = new Type_disability
                    {
                        Id = int.Parse(Convert.ToString(reader["id"])),
                        Descricao = Convert.ToString(reader["descricao"])
                    };

                    types_disability.Add(type_disability);
                }

                con.Close();
            }
            catch (Exception error)
            {
                string errorMessage = error.Message;
                types_disability = null;
                con.Close();
            }

            return types_disability;
        }
        #endregion
    }
}