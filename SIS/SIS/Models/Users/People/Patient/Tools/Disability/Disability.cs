﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;

namespace SIS.Models.Users
{
    public class Disability
    {
        /** DECLARAÇÃO DOS ATRIBUTOS DA CLASSE **/
        #region parameters
        // Variável declarada para conexão com o banco de  dados
        private static MySqlConnection con = new MySqlConnection(ConfigurationManager.ConnectionStrings["BCD"].ConnectionString);
        // Atributos da tabela 'deficiencia'
        private string cid;
        // --Objeto instanciado para ligação entre as tabelas no banco de dados--
        private Type_disability tipo = new Type_disability();

        /** Como nenhum dos atributos pode ser acessado ou modificado diretamente por alguma outra classe, criam-se 
        variavéis que serão as pontes para acessá-los e modificá-los (Getters e Setters)**/
        // --Atributos da tabela 'deficiencia'--
        public string Cid
        {
            get { return cid; }
            set { cid = value; }
        }

        // Objeto instanciado para ligação entre as tabelas no banco de dados
        public Type_disability Tipo
        {
            get { return tipo; }
            set { tipo = value; }
        }
        #endregion

        /** MÉTODOS DA CLASSE **/
        #region methods

        #region disability_manipulation
        #region register_disability
        // --Método desenvolvido para cadastrar uma nova deficiência no banco de dados, caso venha a ser necessário, e conectá-la a um paciente específico--
        public void Register_patient_disability(string patient_id)
        {
            /** DESENVOLVIMENTO DO PROCESSO **/
            // Se a deficiência recebida não existe, cadastre-a
            if (!CheckExistenceDisability())
            {
                Register();
            }
            //cid = Catch_id();

            //Telephone.Relate_user_telephone(user_id, cid);
        }

        // --Método desenvolvido para checar se uma deficiência específica possui registro na tabela 'deficiencia'--
        private bool CheckExistenceDisability()
        {
            // Declaração de variável
            bool result = false;

            try
            {
                // Abrindo conexão com o banco de dados
                if (con.State == ConnectionState.Closed)
                    con.Open();

                // Comando MySql desenvolvido para selecionar um registro da tabela 'deficiencia' do banco de dados
                MySqlCommand query = new MySqlCommand("select * from deficiencia where cid = @cid;", con);
                // Definindo os valores dos parâmetros passados no comando
                query.Parameters.AddWithValue("@cid", cid);

                // Guardando o retorno do comando efetuado
                MySqlDataReader reader = query.ExecuteReader();

                if (reader.Read())
                {
                    result = true;
                }

                con.Close();
            }
            catch (Exception error)
            {
                string errorMessage = error.Message;
                result = false;
                con.Close();
            }

            return result;
        }

        // --Método desenvolvido para cadastrar uma nova deficiência no sistema--
        private void Register()
        {
            try
            {
                // Abrindo conexão com o banco de dados
                if (con.State == ConnectionState.Closed)
                    con.Open();

                // Comando MySql desenvolvido para adicionar um registro na tabela 'deficiencia' do banco de dados
                MySqlCommand query = new MySqlCommand("insert into deficiencia values(@cid, @type);", con);
                // Definindo os valores dos parâmetros passados no comando MySql
                query.Parameters.AddWithValue("@cid", cid);
                query.Parameters.AddWithValue("@type", tipo.Id);

                // Executando o comando MySQL elaborado
                query.ExecuteNonQuery();

                con.Close();
            }
            catch (Exception error)
            {
                string errorMessage = error.Message;
                con.Close();
            }
        }

        // --Método desenvolvido para ligar um paciente a uma deficiência no banco de dados--
        private static void Relate_patient_disability(string patient, string disability)
        {
            try
            {
                // Abrindo conexão com o banco de dados
                if (con.State == ConnectionState.Closed)
                    con.Open();

                // Comando MySql desenvolvido para adicionar um registro na tabela 'paciente_deficiencia' do banco de dados
                MySqlCommand query = new MySqlCommand("insert into paciente_deficiencia values(@patient, @disability);", con);
                // Definindo os valores dos parâmetros passados no comando MySql
                query.Parameters.AddWithValue("@patient", patient);
                query.Parameters.AddWithValue("@disability", disability);

                // Executando o comando MySQL elaborado
                query.ExecuteNonQuery();

                con.Close();
            }
            catch (Exception error)
            {
                string errorMessage = error.Message;
                con.Close();
            }
        }
        #endregion
        #endregion

        public static List<Disability> CatchDisability(string rg)
        {
            // Declaração de variável
            List<Disability> listaDeficiencia = new List<Disability>();

            try
            {
                // Abrindo conexão com o banco de dados
                if (con.State == ConnectionState.Closed)
                    con.Open();

                // Comando MySql desenvolvido para selecionar os dados do usuário autenticado
                MySqlCommand query = new MySqlCommand("select d.descricao from Pessoa_deficiencia p_d, deficiencia d " +
                    "where p_d.Pessoa = @rg and d.id = p_d.deficiencia;", con);
                // Definindo os valores dos parâmetros passados no comando MySql
                query.Parameters.AddWithValue("@rg", rg);

                // Guardando o retorno do comando efetuado
                MySqlDataReader reader = query.ExecuteReader();

                while (reader.Read())
                {
                    Disability disability = new Disability
                    {
                        //Descricao = Convert.ToString(reader["descricao"])
                    };

                    listaDeficiencia.Add(disability);
                }

                con.Close();
            }
            catch (Exception error)
            {
                string errorMessage = error.Message;
                listaDeficiencia = null;
                con.Close();
            }

            return listaDeficiencia;
        }

        private int SearchID()
        {
            // Declaração de variável
            int result = -1;

            try
            {
                // Abrindo conexão com o banco de dados
                if (con.State == ConnectionState.Closed)
                    con.Open();

                //INSERE O COMANDO QUE VAI SER UTILIZADO NO BANCO DE DADOS
                MySqlCommand query = new MySqlCommand("SELECT id FROM deficiencia WHERE descricao = @description;", con);
                // Definindo os valores dos parâmetros passados no comando
                //query.Parameters.AddWithValue("@description", descricao);

                //FAZ EXECUÇÃO DOS VALORES NO BANCO
                MySqlDataReader reader = query.ExecuteReader();

                if (reader.Read())
                    result = reader.GetInt32(0);

                if (reader.IsClosed == false)
                    reader.Close();
            }

            catch (Exception error)
            {
                string errorMsg = error.Message;
                con.Close();
                return -1;
            }

            if (con.State == ConnectionState.Open)
                con.Close();

            return result;
        }
        #endregion
    }
}