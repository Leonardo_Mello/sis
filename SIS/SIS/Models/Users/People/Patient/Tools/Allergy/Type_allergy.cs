﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Data;

namespace SIS.Models.Users
{
    public class Type_allergy
    {
        /** DECLARAÇÃO DOS ATRIBUTOS DA CLASSE **/
        #region parameters
        // Variável declarada para conexão com o banco de  dados
        private static MySqlConnection con = new MySqlConnection(ConfigurationManager.ConnectionStrings["BCD"].ConnectionString);
        // Atributos da tabela 'tipo_alergia'
        private int id;
        private string descricao;

        /** Como nenhum dos atributos pode ser acessado ou modificado diretamente por alguma outra classe, criam-se 
        variavéis que serão as pontes para acessá-los e modificá-los (Getters e Setters)**/
        // --Atributos da tabela 'tipo_alergia'--
        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        [Required(ErrorMessage = "Obrigatório informar o tipo de alergia")]
        [StringLength(150, ErrorMessage = "A descrição do tipo de alergia deve possuir no máximo 150 caracteres")]
        [Display(Name = "Tipo de alergia")]
        public string Descricao
        {
            get { return descricao; }
            set { descricao = value; }
        }
        #endregion

        /** MÉTODOS DA CLASSE **/
        #region methods
        // --Método desenvolvido para retornar lista populada com todos os tipos de alergias existentes no banco de dados--
        public static List<Type_allergy> List_allergy_types()
        {
            // Declaração de variável
            List<Type_allergy> allergy_types = new List<Type_allergy>();

            try
            {
                // Abrindo conexão com o banco de dados
                if (con.State == ConnectionState.Closed)
                    con.Open();

                // Comando MySql desenvolvido para selecionar todas as alergias existentes no banco de dados
                MySqlCommand query = new MySqlCommand("select * from tipo_alergia;", con);

                // Guardando o retorno do comando efetuado
                MySqlDataReader reader = query.ExecuteReader();

                while (reader.Read())
                {
                    Type_allergy allergy_type = new Type_allergy
                    {
                        Id = int.Parse(Convert.ToString(reader["id"])),
                        Descricao = Convert.ToString(reader["descricao"])
                    };

                    allergy_types.Add(allergy_type);
                }

                con.Close();
            }
            catch (Exception error)
            {
                string errorMessage = error.Message;
                allergy_types = null;
                con.Close();
            }

            return allergy_types;
        }
        #endregion
    }
}