﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Data;

namespace SIS.Models.Users
{
    public class Allergy
    {
        /** DECLARAÇÃO DOS ATRIBUTOS DA CLASSE **/
        #region parameters
        // Variável declarada para conexão com o banco de  dados
        private static MySqlConnection con = new MySqlConnection(ConfigurationManager.ConnectionStrings["BCD"].ConnectionString);
        // Atributos da tabela 'alergia'
        private int id;
        private string descricao;
        // --Objeto instanciado para ligação entre as tabelas no banco de dados--
        private Type_allergy tipo = new Type_allergy();

        /** Como nenhum dos atributos pode ser acessado ou modificado diretamente por alguma outra classe, criam-se 
        variavéis que serão as pontes para acessá-los e modificá-los (Getters e Setters)**/
        // --Atributos da tabela 'alergia'--
        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        [Required(ErrorMessage = "Obrigatório informar a alergia")]
        [StringLength(150, ErrorMessage = "A descrição da alergia deve possuir no máximo 150 caracteres")]
        [Display(Name = "Alergia")]
        public string Descricao
        {
            get { return descricao; }
            set { descricao = value; }
        }

        // --Objeto instanciado para ligação entre as tabelas no banco de dados--
        public Type_allergy Tipo
        {
            get { return tipo; }
            set { tipo = value; }
        }
        #endregion

        /** MÉTODOS DA CLASSE **/
        #region methods

        #region allergy_manipulation
        #region register_allergy
        // --Método desenvolvido para ligar um paciente a uma alergia no banco de dados--
        public static void Relate_patient_allergy(string patient, int allergy)
        {
            try
            {
                // Abrindo conexão com o banco de dados
                if (con.State == ConnectionState.Closed)
                    con.Open();

                // Comando MySql desenvolvido para adicionar um registro na tabela 'paciente_alergia' do banco de dados
                MySqlCommand query = new MySqlCommand("insert into paciente_alergia values(@patient, @allergy);", con);
                // Definindo os valores dos parâmetros passados no comando MySql
                query.Parameters.AddWithValue("@patient", patient);
                query.Parameters.AddWithValue("@allergy", allergy);

                // Executando o comando MySQL elaborado
                query.ExecuteNonQuery();

                con.Close();
            }
            catch (Exception error)
            {
                string errorMessage = error.Message;
                con.Close();
            }
        }
        #endregion

        #region list_allergies
        // --Método desenvolvido para retornar lista populada com todas as alergias existentes no banco de dados--
        public static List<Allergy> List_allergies()
        {
            // Declaração de variável
            List<Allergy> allergies = new List<Allergy>();

            try
            {
                // Abrindo conexão com o banco de dados
                if (con.State == ConnectionState.Closed)
                    con.Open();

                // Comando MySql desenvolvido para selecionar todas as alergias existentes no banco de dados
                MySqlCommand query = new MySqlCommand("select * from alergia;", con);

                // Guardando o retorno do comando efetuado
                MySqlDataReader reader = query.ExecuteReader();

                while (reader.Read())
                {
                    Allergy allergy = new Allergy
                    {
                        Id = int.Parse(Convert.ToString(reader["id"])),
                        Descricao = Convert.ToString(reader["descricao"])
                    };
                    allergy.Tipo.Id = int.Parse(Convert.ToString(reader["tipo"]));

                    allergies.Add(allergy);
                }

                con.Close();
            }
            catch (Exception error)
            {
                string errorMessage = error.Message;
                allergies = null;
                con.Close();
            }

            return allergies;
        }
        #endregion
        #endregion

        #endregion
    }
}