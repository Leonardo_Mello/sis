﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Data;

namespace SIS.Models.Users
{
    public class Patient : Person
    {
        /** DECLARAÇÃO DOS ATRIBUTOS DA CLASSE **/
        #region parameters
        // --Atributos da tabela 'pessoa' que são exclusivos do usuário que possui nível de acesso igual a 'Pessoa'--
        private string grupo_sanguineo, nome_mae, nome_pai;
        // --Objeto instanciado para ligação entre as tabelas no banco de dados--
        private List<Disability> list_disabilities = new List<Disability>();
        private Disability disability = new Disability();
        private List<Allergy> list_allergies = new List<Allergy>();
        private Allergy allergy = new Allergy();

        /** Como nenhum dos atributos pode ser acessado ou modificado diretamente por alguma outra classe, criam-se 
        variavéis que serão as pontes para acessá-los e modificá-los (Getters e Setters)**/
        // --Atributos da tabela 'pessoa'--
        [Required(ErrorMessage = "Obrigatório informar o RG")]
        [StringLength(12, ErrorMessage = "O RG deve possuir no máximo 12 caracteres")]
        [Display(Name = "RG")]
        public new string Id
        {
            get { return id; }
            set { id = value; }
        }

        [Required(ErrorMessage = "Obrigatório informar o grupo sanguíneo")]
        [StringLength(3, ErrorMessage = "O grupo sanguíneo deve possuir no máximo 3 caracteres")]
        [Display(Name = "Grupo sanguíneo")]
        public string Grupo_sanguineo
        {
            get { return grupo_sanguineo; }
            set { grupo_sanguineo = value; }
        }

        [Required(ErrorMessage = "Obrigatório informar o nome completo da mãe")]
        [StringLength(100, ErrorMessage = "O nome completo da mãe deve possuir no máximo 100 caracteres")]
        [Display(Name = "Nome completo da mãe")]
        public string Nome_mae
        {
            get { return nome_mae; }
            set { nome_mae = value; }
        }

        [Required(ErrorMessage = "Obrigatório informar o nome completo do pai")]
        [StringLength(100, ErrorMessage = "O nome completo do pai deve possuir no máximo 100 caracteres")]
        [Display(Name = "Nome completo do pai")]
        public string Nome_pai
        {
            get { return nome_pai; }
            set { nome_pai = value; }
        }

        // --Objeto instanciado para ligação entre as tabelas no banco de dados--
        public List<Disability> List_disabilities
        {
            get { return list_disabilities; }
            set { list_disabilities = value; }
        }

        public Disability Disability
        {
            get { return disability; }
            set { disability = value; }
        }

        public List<Allergy> List_allergies
        {
            get { return list_allergies; }
            set { list_allergies = value; }
        }

        public Allergy Allergy
        {
            get { return allergy; }
            set { allergy = value; }
        }
        #endregion

        /** MÉTODOS DA CLASSE **/
        #region methods

        #region recover_password
        #region get_contact_info
        // --Método desenvolvido para retornar as informações de contato do usuário--
        public static Patient GetContactInfo(string rg)
        {
            // Declaração de variável
            Patient patient = null;

            try
            {
                // Abrindo conexão com o banco de dados
                if (con.State == ConnectionState.Closed)
                    con.Open();

                // Comando MySql desenvolvido para selecionar os dados do usuário informado
                MySqlCommand query = new MySqlCommand("select p.email from pessoa p where p.id = @rg;", con);
                // Definindo os valores dos parâmetros passados no comando MySql
                query.Parameters.AddWithValue("@rg", rg);

                // Guardando o retorno do comando efetuado
                MySqlDataReader reader = query.ExecuteReader();

                if (reader.Read())
                {
                    patient = new Patient
                    {
                        Email = Convert.ToString(reader["email"])
                    };
                }
                reader.Close();

                if (!patient.Equals(null))
                {
                    patient.list_telephones = CatchMobilePhone(rg);
                }

                con.Close();
            }
            catch (Exception error)
            {
                string errorMessage = error.Message;
                patient = null;
                con.Close();
            }

            return patient;
        }
        #endregion
        #endregion

        #region patient_account
        #region view_profile
        // --Método desenvolvido para retornar os dados do Pessoa autenticado no sistema--
        public static Patient GetInfo(string rg)
        {
            // Declaração de variável
            Patient patient = null;

            try
            {
                // Abrindo conexão com o banco de dados
                if (con.State == ConnectionState.Closed)
                    con.Open();

                // Comando MySql desenvolvido para selecionar os dados do usuário autenticado
                MySqlCommand query = new MySqlCommand("select p.id, p.cpf, p.nome, p.sobrenome, p.data_nascimento, p.sexo, p.foto, p.email, cast(aes_decrypt(p.senha, @cryptographyKey) as char) as senha, p.nivel_acesso, p.nome_mae, p.nome_pai, " +
                    "e.cep, e.estado, e.cidade, e.bairro, e.rua, e.numero, e.complemento " +
                    "from pessoa p, endereco e " +
                    "where p.id = @rg and e.id = p.endereco;", con);
                // Definindo os valores dos parâmetros passados no comando MySql
                query.Parameters.AddWithValue("@cryptographyKey", ConfigurationManager.ConnectionStrings["cryptographyKey"].ConnectionString);
                query.Parameters.AddWithValue("@rg", rg);

                // Guardando o retorno do comando efetuado
                MySqlDataReader reader = query.ExecuteReader();

                if (reader.Read())
                {
                    patient = new Patient
                    {
                        Id = Convert.ToString(reader["id"]),
                        Cpf = Convert.ToString(reader["cpf"]),
                        Nome = Convert.ToString(reader["nome"]),
                        Sobrenome = Convert.ToString(reader["sobrenome"]),
                        Data_nascimento = Convert.ToDateTime(reader["data_nascimento"]),
                        Sexo = Convert.ToString(reader["sexo"]),
                        Email = Convert.ToString(reader["email"]),
                        Senha = Convert.ToString(reader["senha"]),
                        Nivel_acesso = Convert.ToString(reader["nivel_acesso"]),
                        Nome_mae = Convert.ToString(reader["nome_mae"]),
                        Nome_pai = Convert.ToString(reader["nome_pai"])
                    };

                    try
                    {
                        patient.Foto = (byte[])reader["foto"];
                    }
                    catch (Exception error)
                    {
                        string errorMessage = error.Message;
                    }

                    patient.Endereco.Cep = Convert.ToString(reader["cep"]);
                    patient.Endereco.Estado = Convert.ToString(reader["estado"]);
                    patient.Endereco.Cidade = Convert.ToString(reader["cidade"]);
                    patient.Endereco.Bairro = Convert.ToString(reader["bairro"]);
                    patient.Endereco.Rua = Convert.ToString(reader["rua"]);
                    patient.Endereco.Numero = Convert.ToString(reader["numero"]);
                    patient.Endereco.Complemento = Convert.ToString(reader["complemento"]);
                }
                reader.Close();

                if (!patient.Equals(null))
                {
                    patient.list_telephones = Person.CatchTelephone(rg);
                    patient.list_disabilities = Disability.CatchDisability(rg);
                }

                con.Close();
            }
            catch (Exception error)
            {
                string errorMessage = error.Message;
                patient = null;
                con.Close();
            }

            return patient;
        }
        #endregion

        #region register_patient
        // --Método desenvolvido para cadastrar um(a) novo(a) paciente no sistema--
        public bool Register()
        {
            // Declaração de variável
            bool result = false;
            // Cadastrando o endereço do usuário, caso necessário, e recebendo o valor do id do mesmo
            endereco.Id = endereco.GetAddress();

            try
            {
                // Abrindo conexão com o banco de dados
                if (con.State == ConnectionState.Closed)
                    con.Open();

                // Comando MySql desenvolvido para adicionar um registro na tabela 'pessoa' do banco de dados
                MySqlCommand query = new MySqlCommand("insert into pessoa(id, cpf, nome, sobrenome, data_nascimento, sexo, foto, email, senha, nivel_acesso, grupo_sanguineo, nome_mae, nome_pai, endereco) " +
                    "values(@id, @cpf, @nome, @sobrenome, @data_nascimento, @sexo, @foto, @email, aes_encrypt(@senha, @cryptographyKey), @nivel_acesso, @grupo_sanguineo, @nome_mae, @nome_pai, @endereco)", con);
                // Definindo os valores dos parâmetros passados no comando MySql
                query.Parameters.AddWithValue("@id", id);
                query.Parameters.AddWithValue("@cpf", cpf);
                query.Parameters.AddWithValue("@nome", nome);
                query.Parameters.AddWithValue("@sobrenome", sobrenome);
                query.Parameters.AddWithValue("@data_nascimento", data_nascimento);
                query.Parameters.AddWithValue("@sexo", sexo);
                query.Parameters.AddWithValue("@foto", foto);
                query.Parameters.AddWithValue("@email", email);
                query.Parameters.AddWithValue("@senha", senha);
                query.Parameters.AddWithValue("@cryptographyKey", ConfigurationManager.ConnectionStrings["cryptographyKey"].ConnectionString);
                query.Parameters.AddWithValue("@nivel_acesso", nivel_acesso);
                query.Parameters.AddWithValue("@grupo_sanguineo", grupo_sanguineo);
                query.Parameters.AddWithValue("@nome_mae", nome_mae);
                query.Parameters.AddWithValue("@nome_pai", nome_pai);
                query.Parameters.AddWithValue("@endereco", endereco.Id);

                // Executando o comando MySQL elaborado
                query.ExecuteNonQuery();

                // Cadastrando os telefones do usuário, caso necessário, e conectando-os ao mesmo no banco de dados
                foreach (Telephone telephone in list_telephones)
                {
                    telephone.Register_user_telephone(id);
                }

                result = true;

                con.Close();
            }
            catch (Exception error)
            {
                string errorMessage = error.Message;
                result = false;
                con.Close();
            }

            return result;
        }
        #endregion

        #region list_patients
        // --Método desenvolvido para retornar lista populada com usuários que possuem nível de acesso igual a "Pessoa"--
        public static List<Patient> List_patients()
        {
            // Declaração de variável
            List<Patient> patients = new List<Patient>();

            try
            {
                // Abrindo conexão com o banco de dados
                if (con.State == ConnectionState.Closed)
                    con.Open();

                // Comando MySql desenvolvido para selecionar os usuários que possuem nível de acesso igual a "Pessoa"
                MySqlCommand query = new MySqlCommand("select id, cpf, nome, sobrenome, data_nascimento, sexo " +
                   "from pessoa where nivel_acesso = @user_type", con);
                // Definindo os valores dos parâmetros passados no comando MySql
                query.Parameters.AddWithValue("@user_type", "Paciente");

                // Guardando o retorno do comando efetuado
                MySqlDataReader reader = query.ExecuteReader();

                while (reader.Read())
                {
                    Patient patient = new Patient
                    {
                        Id = Convert.ToString(reader["id"]),
                        Cpf = Convert.ToString(reader["cpf"]),
                        Nome = Convert.ToString(reader["nome"]),
                        Sobrenome = Convert.ToString(reader["sobrenome"]),
                        Data_nascimento = Convert.ToDateTime(reader["data_nascimento"]),
                        Sexo = Convert.ToString(reader["sexo"])
                    };

                    patients.Add(patient);
                }

                con.Close();
            }
            catch (Exception error)
            {
                string errorMessage = error.Message;
                patients = null;
                con.Close();
            }

            return patients;
        }
        #endregion

        #region edit_patient
        #endregion
        #endregion

        public object[] ViewScheduleList()
        {
            return null;
        }

        public void ViewSchedule()
        {

        }

        #endregion
    }
}