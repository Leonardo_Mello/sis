﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Data;

namespace SIS.Models.Users
{
    public class Doctor : Person
    {
        /** DECLARAÇÃO DOS ATRIBUTOS DA CLASSE **/
        #region parameters
        // --Objetos instanciados para ligação entre as tabelas no banco de dados--
        private Occupation_Area area_atuacao = new Occupation_Area();
        private List<Medical_Specialty> list_medical_specialties = new List<Medical_Specialty>();
        private Medical_Specialty especialidade = new Medical_Specialty();

        /** Como nenhum dos atributos pode ser acessado ou modificado diretamente por alguma outra classe, criam-se 
        variavéis que serão as pontes para acessá-los e modificá-los (Getters e Setters)**/
        // --Atributos da tabela 'pessoa'--
        [Required(ErrorMessage = "Obrigatório informar o CRM")]
        [StringLength(13, ErrorMessage = "O CRM deve possuir no máximo 13 caracteres")]
        [Display(Name = "CRM")]
        public string Crm
        {
            get { return id; }
            set { id = value; }
        }

        // --Objetos instanciados para ligação entre as tabelas no banco de dados--
        public Occupation_Area Area_atuacao
        {
            get { return area_atuacao; }
            set { area_atuacao = value; }
        }

        public List<Medical_Specialty> List_medical_specialties
        {
            get { return list_medical_specialties; }
            set { list_medical_specialties = value; }
        }

        public Medical_Specialty Especialidade
        {
            get { return especialidade; }
            set { especialidade = value; }
        }
        #endregion

        /** MÉTODOS DA CLASSE **/
        #region methods

        #region recover_password
        #region get_contact_info
        // --Método desenvolvido para retornar as informações de contato do usuário--
        public static Doctor GetContactInfo(string crm)
        {
            // Declaração de variável
            Doctor doctor = null;

            try
            {
                // Abrindo conexão com o banco de dados
                if (con.State == ConnectionState.Closed)
                    con.Open();

                // Comando MySql desenvolvido para selecionar os dados do usuário informado
                MySqlCommand query = new MySqlCommand("select p.email from pessoa p where p.id = @crm;", con);
                // Definindo os valores dos parâmetros passados no comando MySql
                query.Parameters.AddWithValue("@crm", crm);

                // Guardando o retorno do comando efetuado
                MySqlDataReader reader = query.ExecuteReader();

                if (reader.Read())
                {
                    doctor = new Doctor
                    {
                        Email = Convert.ToString(reader["email"])
                    };
                }
                reader.Close();

                if (!doctor.Equals(null))
                {
                    doctor.list_telephones = CatchMobilePhone(crm);
                }

                con.Close();
            }
            catch (Exception error)
            {
                string errorMessage = error.Message;
                doctor = null;
                con.Close();
            }

            return doctor;
        }
        #endregion
        #endregion

        #region account_manipulation
        #region register_doctor
        // --Método desenvolvido para cadastrar um(a) novo(a) médico(a) no sistema--
        public bool Register()
        {
            // Declaração de variável
            bool result = false;
            // Cadastrando o endereço do usuário, caso necessário, e recebendo o valor do id do mesmo
            endereco.Id = endereco.GetAddress();


            try
            {
                // Abrindo conexão com o banco de dados
                if (con.State == ConnectionState.Closed)
                    con.Open();

                // Comando MySql desenvolvido para adicionar um registro na tabela 'pessoa' do banco de dados
                MySqlCommand query = new MySqlCommand("insert into pessoa(id, cpf, nome, sobrenome, data_nascimento, sexo, foto, email, senha, nivel_acesso, area_atuacao, endereco) " +
                    "values(@id, @cpf, @nome, @sobrenome, @data_nascimento, @sexo, @foto, @email, aes_encrypt(@senha, @cryptographyKey), @nivel_acesso, @area_atuacao, @endereco)", con);
                // Definindo os valores dos parâmetros passados no comando MySql
                query.Parameters.AddWithValue("@id", id);
                query.Parameters.AddWithValue("@cpf", cpf);
                query.Parameters.AddWithValue("@nome", nome);
                query.Parameters.AddWithValue("@sobrenome", sobrenome);
                query.Parameters.AddWithValue("@data_nascimento", data_nascimento);
                query.Parameters.AddWithValue("@sexo", sexo);
                query.Parameters.AddWithValue("@foto", foto);
                query.Parameters.AddWithValue("@email", email);
                query.Parameters.AddWithValue("@senha", senha);
                query.Parameters.AddWithValue("@cryptographyKey", ConfigurationManager.ConnectionStrings["cryptographyKey"].ConnectionString);
                query.Parameters.AddWithValue("@nivel_acesso", nivel_acesso);
                query.Parameters.AddWithValue("@area_atuacao", area_atuacao);
                query.Parameters.AddWithValue("@endereco", endereco.Id);

                // Executando o comando MySQL elaborado
                query.ExecuteNonQuery();

                // Cadastrando os telefones do usuário, caso necessário, e conectando-os ao mesmo no banco de dados
                foreach (Telephone telephone in list_telephones)
                {
                    telephone.Register_user_telephone(id);
                }
                especialidade.RegisterSpecialty();
                InsertDoctorSpeciality(id);

                result = true;

                con.Close();
            }
            catch (Exception error)
            {
                string errorMessage = error.Message;
                result = false;
                con.Close();
            }

            return result;
        }
        #endregion

        #region view_profile
        // --Método desenvolvido para retornar os dados do médico autenticado no sistema--
        public static Doctor GetInfo(string crm)
        {
            MySqlConnection con = new MySqlConnection(ConfigurationManager.ConnectionStrings["BCD"].ConnectionString);
            // Declaração de variável
            Doctor doctor = null;

            try
            {
                // Abrindo conexão com o banco de dados
                if (con.State == ConnectionState.Closed)
                    con.Open();

                // Comando MySql desenvolvido para selecionar os dados do usuário autenticado
                MySqlCommand query = new MySqlCommand("select p.id, p.cpf, p.nome, p.sobrenome, p.data_nascimento, p.sexo, p.foto, p.email, cast(aes_decrypt(p.senha, @cryptographyKey) as char) as senha, p.nivel_acesso, p.area_atuacao, " +
                    "e.cep, e.estado, e.cidade, e.bairro, e.rua, e.numero, e.complemento " +
                    "from pessoa p, endereco e " +
                    "where p.id = @crm and e.id = p.endereco;", con);
                // Definindo os valores dos parâmetros passados no comando MySql
                query.Parameters.AddWithValue("@cryptographyKey", ConfigurationManager.ConnectionStrings["cryptographyKey"].ConnectionString);
                query.Parameters.AddWithValue("@crm", crm);

                // Guardando o retorno do comando efetuado
                MySqlDataReader reader = query.ExecuteReader();

                if (reader.Read())
                {
                    doctor = new Doctor
                    {
                        Id = Convert.ToString(reader["id"]),
                        Cpf = Convert.ToString(reader["cpf"]),
                        Nome = Convert.ToString(reader["nome"]),
                        Sobrenome = Convert.ToString(reader["sobrenome"]),
                        Data_nascimento = Convert.ToDateTime(reader["data_nascimento"]),
                        Sexo = Convert.ToString(reader["sexo"]),
                        Email = Convert.ToString(reader["email"]),
                        Senha = Convert.ToString(reader["senha"]),
                        Nivel_acesso = Convert.ToString(reader["nivel_acesso"])
                    };

                    try
                    {
                        doctor.Foto = (byte[])reader["foto"];
                    }
                    catch (Exception error)
                    {
                        string errorMessage = error.Message;
                    }

                    doctor.Area_atuacao.Id = int.Parse(Convert.ToString(reader["area_atuacao"]));
                    doctor.Endereco.Cep = Convert.ToString(reader["cep"]);
                    doctor.Endereco.Estado = Convert.ToString(reader["estado"]);
                    doctor.Endereco.Cidade = Convert.ToString(reader["cidade"]);
                    doctor.Endereco.Bairro = Convert.ToString(reader["bairro"]);
                    doctor.Endereco.Rua = Convert.ToString(reader["rua"]);
                    doctor.Endereco.Numero = Convert.ToString(reader["numero"]);
                    doctor.Endereco.Complemento = Convert.ToString(reader["complemento"]);
                }
                reader.Close();

                if (!doctor.Equals(null))
                {
                    doctor.list_telephones = Person.CatchTelephone(crm);
                    doctor.list_medical_specialties = Doctor.CatchMedicalSpecialty(crm);
                }

                con.Close();
            }
            catch (Exception error)
            {
                string errorMessage = error.Message;
                doctor = null;
                con.Close();
            }
            return doctor;
        }

        public static List<Medical_Specialty> CatchMedicalSpecialty(string crm)
        {
            // Declaração de variável
            List<Medical_Specialty> listaEspecialidade = new List<Medical_Specialty>();

            try
            {
                // Abrindo conexão com o banco de dados
                if (con.State == ConnectionState.Closed)
                    con.Open();

                // Comando MySql desenvolvido para selecionar os dados do usuário autenticado
                MySqlCommand query = new MySqlCommand("select e.descricao from medico_especialidade m_e, especialidade e " +
                    "where m_e.medico = @crm and e.id = m_e.especialidade;", con);
                // Definindo os valores dos parâmetros passados no comando MySql
                query.Parameters.AddWithValue("@crm", crm);

                // Guardando o retorno do comando efetuado
                MySqlDataReader reader = query.ExecuteReader();

                while (reader.Read())
                {
                    Medical_Specialty specialty = new Medical_Specialty
                    {
                        Descricao = Convert.ToString(reader["descricao"])
                    };

                    listaEspecialidade.Add(specialty);
                }

                con.Close();
            }
            catch (Exception error)
            {
                string errorMessage = error.Message;
                listaEspecialidade = null;
                con.Close();
            }

            return listaEspecialidade;
        }
        #endregion

        #region register_doctor
        public void InsertDoctorSpeciality(string id)
        {
            {
                try
                {
                    // Abrindo conexão com o banco de dados
                    if (con.State == ConnectionState.Closed)
                        con.Open();
                    //INSERE O COMANDO QUE VAI SER UTILIZADO NO BANCO DE DADOS
                    MySqlCommand query = new MySqlCommand("insert into medico_especialidade values (@medico, @especialidade);", con);
                    // Definindo os valores dos parâmetros passados no comando
                    query.Parameters.AddWithValue("@medico", id);
                    //query.Parameters.AddWithValue("@especialidade", especialidade.Id);
                    query.ExecuteNonQuery();
                }
                catch (Exception error)
                {
                    string errorMessage = error.Message;
                    con.Close();
                    //APRESENTA A MENSAGEM CASO HOUVER ERRO
                }
                if (con.State == ConnectionState.Open)
                    con.Close();
            }
        }
        #endregion

        #region list_doctors
        // --Método desenvolvido para retornar lista populada com usuários que possuem nível de acesso igual a "Médico"--
        public static List<Doctor> List_doctors(string hospital)
        {
            // Declaração de variável
            List<Doctor> doctors = new List<Doctor>();

            try
            {
                // Abrindo conexão com o banco de dados
                if (con.State == ConnectionState.Closed)
                    con.Open();

                // Comando MySql desenvolvido para selecionar os usuários que possuem nível de acesso igual a "Médico"
                MySqlCommand query = new MySqlCommand("select p.id, p.cpf, p.nome, p.sobrenome, p.data_nascimento, p.sexo, a_a.descricao as area_atuacao from pessoa p, area_atuacao a_a, funcionario_hospital f_h " +
                    "where p.nivel_acesso = @user_type and a_a.id = p.area_atuacao and f_h.funcionario = p.id and f_h.hospital = @cnes;", con);
                // Definindo os valores dos parâmetros passados no comando MySql
                query.Parameters.AddWithValue("@user_type", "Médico");
                query.Parameters.AddWithValue("@cnes", hospital);

                // Guardando o retorno do comando efetuado
                MySqlDataReader reader = query.ExecuteReader();

                while (reader.Read())
                {
                    Doctor doctor = new Doctor
                    {
                        Id = Convert.ToString(reader["id"]),
                        Cpf = Convert.ToString(reader["cpf"]),
                        Nome = Convert.ToString(reader["nome"]),
                        Sobrenome = Convert.ToString(reader["sobrenome"]),
                        Data_nascimento = Convert.ToDateTime(reader["data_nascimento"]),
                        Sexo = Convert.ToString(reader["sexo"])
                    };
                    doctor.Area_atuacao.Descricao = Convert.ToString(reader["area_atuacao"]);

                    doctors.Add(doctor);
                }

                con.Close();
            }
            catch (Exception error)
            {
                string errorMessage = error.Message;
                doctors = null;
                con.Close();
            }

            return doctors;
        }
        #endregion

        #region edit_doctor
        //EXEMPLO PARA EDITAR OS MEDICOS
        public string Editar()
        {
            return null;
        }
        #endregion

        #region exclude_doctor
        //---------------------------------METODO REMOVER-------------------------------------//
        public string Remover()
        {
            string mensagem = "Médico removido com sucesso!";
            try
            {
                con.Open();
                MySqlCommand query = new MySqlCommand("DELETE from medico_especialidade WHERE medico = @medico", con);
                query.Parameters.AddWithValue("@medico", id);
                query.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                mensagem = e.Message;
            }

            con.Close();
            return mensagem;
        }

        #endregion
        #endregion
        #endregion
    }
}