﻿using MySql.Data.MySqlClient;
using System;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Data;

namespace SIS.Models.Users
{
    public class Medical_Specialty
    {
        /** DECLARAÇÃO DOS ATRIBUTOS DA CLASSE **/
        #region parameters
        // Variável declarada para conexão com o banco de  dados
        private static MySqlConnection con = new MySqlConnection(ConfigurationManager.ConnectionStrings["BCD"].ConnectionString);
        // Atributos da tabela 'especialidade'
        private int id;
        private string descricao;

        /** Como nenhum dos atributos pode ser acessado ou modificado diretamente por alguma outra classe, criam-se 
        variavéis que serão as pontes para acessá-los e modificá-los (Getters e Setters)**/
        // --Atributos da tabela 'especialidade'--
        public int Id
        {
            get { return SearchID(); }
            set { id = value; }
        }

        [StringLength(60, ErrorMessage = "A descrição da especialidade deve possuir no máximo 60 caracteres")]
        [Display(Name = "Especialidade")]
        public string Descricao
        {
            get { return descricao; }
            set { descricao = value; }
        }
        #endregion

        /** MÉTODOS DA CLASSE **/
        #region methods
        private bool CheckExistenceSpecialty()
        {
            // Declaração de variável
            bool result = false;

            try
            {
                // Abrindo conexão com o banco de dados
                if (con.State == ConnectionState.Closed)
                    con.Open();

                MySqlCommand query = new MySqlCommand("select * from especialidade WHERE descricao = @description", con);
                // Definindo os valores dos parâmetros passados no comando MySql
                query.Parameters.AddWithValue("@description", descricao);

                // Guardando o retorno do comando efetuado
                MySqlDataReader reader = query.ExecuteReader();

                if (reader.Read())
                {
                    result = true;
                    reader.Close();
                }

                if (reader.IsClosed == false)
                    reader.Close();
            }

            catch (Exception error)
            {
                string errorMsg = error.Message; //USAR BREAKPOINT PARA VISUALIZAR O ERRO
                con.Close();
                return false;
            }

            if (con.State == ConnectionState.Open)
                con.Close();

            return result;
        }

        public void RegisterSpecialty()
        {
            if (!CheckExistenceSpecialty())
            {
                try
                {
                    // Abrindo conexão com o banco de dados
                    if (con.State == ConnectionState.Closed)
                        con.Open();

                    //INSERE O COMANDO QUE VAI SER UTILIZADO NO BANCO DE DADOS
                    MySqlCommand query = new MySqlCommand("insert into especialidade values (@id, @description);", con);
                    // Definindo os valores dos parâmetros passados no comando
                    query.Parameters.AddWithValue("@id", null);
                    query.Parameters.AddWithValue("@description", descricao);

                    //FAZ EXECUÇÃO DOS VALORES NO BANCO
                    query.ExecuteNonQuery();
                }

                catch (Exception error)
                {
                    string errorMsg = error.Message;
                    con.Close();
                }
            }

            if (con.State == ConnectionState.Open)
                con.Close();
        }

        public int SearchID()
        {
            // Declaração de variável
            int result = -1;

            try
            {
                // Abrindo conexão com o banco de dados
                if (con.State == ConnectionState.Closed)
                    con.Open();

                //INSERE O COMANDO QUE VAI SER UTILIZADO NO BANCO DE DADOS
                MySqlCommand query = new MySqlCommand("SELECT id FROM especialidade WHERE descricao = @description;", con);
                // Definindo os valores dos parâmetros passados no comando
                query.Parameters.AddWithValue("@description", descricao);

                //FAZ EXECUÇÃO DOS VALORES NO BANCO
                MySqlDataReader reader = query.ExecuteReader();

                if (reader.Read())
                    result = reader.GetInt32(0);

                if (reader.IsClosed == false)
                    reader.Close();
            }

            catch (Exception error)
            {
                string errorMsg = error.Message;
                con.Close();
                return -1;
            }

            if (con.State == ConnectionState.Open)
                con.Close();

            return result;
        }
        #endregion
    }
}