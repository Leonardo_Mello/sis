﻿using MySql.Data.MySqlClient;
using System.ComponentModel.DataAnnotations;
using System.Configuration;

namespace SIS.Models.Users
{
    public class Occupation_Area
    {
        /** DECLARAÇÃO DOS ATRIBUTOS DA CLASSE **/
        #region parameters
        // Variável declarada para conexão com o banco de  dados
        private static MySqlConnection con = new MySqlConnection(ConfigurationManager.ConnectionStrings["BCD"].ConnectionString);
        // Atributos da tabela 'area_atuacao'
        private int id;
        private string descricao;

        /** Como nenhum dos atributos pode ser acessado ou modificado diretamente por alguma outra classe, criam-se 
        variavéis que serão as pontes para acessá-los e modificá-los (Getters e Setters)**/
        // --Atributos da tabela 'area_atuacao'--
        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        [Required(ErrorMessage = "Obrigatório informar a área de atuação")]
        [StringLength(150, ErrorMessage = "A descrição da área de atuação deve possuir no máximo 150 caracteres")]
        [Display(Name = "Área de atuação")]
        public string Descricao
        {
            get { return descricao; }
            set { descricao = value; }
        }
        #endregion
    }
}