﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace SIS.Models.Users.Agenda
{
    public class PrescricaoMedica
    {
        #region parameters
        private static MySqlConnection con = new MySqlConnection(ConfigurationManager.ConnectionStrings["BCD"].ConnectionString);
        private string id, medicamento, quantidade, duracao, consulta;

        public string ID
        {
            get { return id; }
            set { id = value; }
        }
        public string Medicamento
        {
            get { return medicamento; }
            set { medicamento = value; }
        }
        public string Quantidade
        {
            get { return quantidade; }
            set { quantidade = value; }
        }
        public string Duracao
        {
            get { return duracao; }
            set { duracao = value; }
        }
        public string Consulta
        {
            get { return consulta; }
            set { consulta = value; }
        }

        #endregion

        #region methods
        public string RegistraPrescricao()
        {
            try
            {
                if (con.State == System.Data.ConnectionState.Closed)
                    con.Open();

                MySqlCommand query = new MySqlCommand("INSERT INTO prescricao_medica(medicamento, quantidade, duracao, consulta) values(@medicamento, @quantidade, @duracao, @consulta)", con);
                query.Parameters.AddWithValue("@medicamento", medicamento);
                query.Parameters.AddWithValue("@quantidade", quantidade);
                query.Parameters.AddWithValue("@duracao", duracao);
                query.Parameters.AddWithValue("@consulta", consulta);

                query.ExecuteNonQuery();
                con.Close();
            }
            catch (Exception error)
            {
                string errorMessage = error.Message;
                con.Close();
                return "Erro ao registrar prescricao médica!";
            }

            return "Prescrição médica registrada com sucesso!";
        }
        #endregion
    }
}