﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Configuration;

namespace SIS.Models.Users.Agenda
{
    public class Appointment
    {
        /** DECLARAÇÃO DOS ATRIBUTOS DA CLASSE **/
        #region parameters
        // --Variável declarada para conexão com o banco de  dados--
        private static MySqlConnection con = new MySqlConnection(ConfigurationManager.ConnectionStrings["BCD"].ConnectionString);
        // --Atributos da tabela 'consulta--
        private int codigo;
        private DateTime data, duracao;
        private string especialidade, titulo, comentario, observacao_medica, diagnostico;
        private bool estado;
        // --Objetos instanciados para ligação entre as tabelas no banco de dados--
        private Hospital hospital = new Hospital();
        private Person[] patient_doctor = new Person[2];

        /** Como nenhum dos atributos pode ser acessado ou modificado diretamente por alguma outra classe, criam-se 
        variavéis que serão as pontes para acessá-los e modificá-los (Getters e Setters)**/
        // --Atributos da tabela 'consulta'--
        public int Codigo
        {
            get { return codigo; }
            set { codigo = value; }
        }

        [Required(ErrorMessage = "Obrigatório informar a data")]
        [StringLength(10, ErrorMessage = "A data deve possuir no máximo 10 caracteres")]
        [Display(Name = "Data")]
        public DateTime Data
        {
            get { return data; }
            set { data = value; }
        }

        [Required(ErrorMessage = "Obrigatório informar a duração")]
        [StringLength(5, ErrorMessage = "A duração deve possuir no máximo 5 caracteres")]
        [Display(Name = "Duração")]
        public DateTime Duracao
        {
            get { return duracao; }
            set { duracao = value; }
        }

        [Required(ErrorMessage = "Obrigatório informar a especialidade")]
        [StringLength(60, ErrorMessage = "A especialidade deve possuir no máximo 60 caracteres")]
        [Display(Name = "Especialidade")]
        public string Especialidade
        {
            get { return especialidade; }
            set { especialidade = value; }
        }

        [StringLength(50, ErrorMessage = "O título deve possuir no máximo 50 caracteres")]
        [Display(Name = "Título")]
        public string Titulo
        {
            get { return titulo; }
            set { titulo = value; }
        }

        [Display(Name = "Comentário")]
        public string Comentario
        {
            get { return comentario; }
            set { comentario = value; }
        }

        [Display(Name = "Observação médica")]
        public string Observacao_medica
        {
            get { return observacao_medica; }
            set { observacao_medica = value; }
        }

        [Display(Name = "Diagnóstico")]
        public string Diagnostico
        {
            get { return diagnostico; }
            set { diagnostico = value; }
        }

        public bool Estado
        {
            get { return estado; }
            set { estado = value; }
        }

        public Hospital Hospital
        {
            get { return hospital; }
            set { hospital = value; }
        }

        // --Objetos instanciados para ligação entre as tabelas no banco de dados--
        public Person[] Patient_doctor
        {
            get { return patient_doctor; }
            set { patient_doctor = value; }
        }

        public string DataFormatada
        {
            get { return data.Day + "/" + data.Month + "/" + data.Year + " - " + data.Hour + "h" + data.Minute; }
        }
        
        //public string Nome_hospital
        //{
        //    //get { return Hospital.GetInfo(Cnes_hospital).Nome_fantasia; }
        //}
        #endregion

        /** MÉTODOS DA CLASSE **/
        #region methods
        public string AgendaConsulta()
        {
            try
            {
                if (con.State == System.Data.ConnectionState.Closed)
                    con.Open();

                MySqlCommand query = new MySqlCommand("insert into consulta(data, duracao, especialidade, titulo, comentario, hospital, estado) values (@data, @duracao, @especialidade, @titulo, @comentario, @hospital, @estado)", con);
                query.Parameters.AddWithValue("@data", data);
                query.Parameters.AddWithValue("@duracao", duracao);
                query.Parameters.AddWithValue("@especialidade", especialidade);
                query.Parameters.AddWithValue("@titulo", titulo);
                query.Parameters.AddWithValue("@comentario", comentario);
                query.Parameters.AddWithValue("@hospital", hospital.Cnes);
                query.Parameters.AddWithValue("@estado", estado);

                query.ExecuteNonQuery();
                con.Close();

                relacionaPessoaConsulta(patient_doctor[0].Id);
                relacionaPessoaConsulta(patient_doctor[1].Id);
            }
            catch (Exception error)
            {
                string errorMessage = error.Message;
                con.Close();
                return "Erro ao agendar consulta!";
            }

            return "Consulta agendada com sucesso!";
        }

        private void relacionaPessoaConsulta(string id_pessoa)
        {
            try
            {
                if (con.State == System.Data.ConnectionState.Closed)
                    con.Open();

                MySqlCommand query = new MySqlCommand("insert into pessoa_consulta values (@pessoa, (SELECT MAX(codigo) FROM consulta))", con);
                query.Parameters.AddWithValue("@pessoa", id_pessoa);

                query.ExecuteNonQuery();
                con.Close();
            }
            catch (Exception error)
            {
                string errorMessage = error.Message;
                con.Close();
            }
        }

        public static string ConfirmaConsulta(string idConsulta, string diagnostico, PrescricaoMedica prescricao, Exame exame)
        {
            try
            {
                if (con.State == System.Data.ConnectionState.Closed)
                    con.Open();

                MySqlCommand query = new MySqlCommand("UPDATE consulta SET diagnostico = @diagnostico WHERE codigo = @idConsulta", con);
                query.Parameters.AddWithValue("@diagnostico", diagnostico);
                query.Parameters.AddWithValue("@idConsulta", idConsulta);

                query.ExecuteNonQuery();
                con.Close();

                if (prescricao != null)
                {
                    prescricao.Consulta = idConsulta;
                    prescricao.RegistraPrescricao();
                }

                if (exame != null)
                {
                    exame.RegistrarExame(idConsulta);
                    return "Consulta registrada com sucesso e novo exame cadastrado com sucesso!";
                }
            }
            catch (Exception error)
            {
                string errorMessage = error.Message;
                con.Close();
                return "Erro ao registrar consulta!";
            }

            return "Consulta registrada com sucesso!";
        }

        public static string CancelarConsulta(string idConsulta)
        {
            try
            {
                if (con.State == System.Data.ConnectionState.Closed)
                    con.Open();

                MySqlCommand query = new MySqlCommand("DELETE FROM pessoa_consulta WHERE consulta = @idConsulta", con);
                query.Parameters.AddWithValue("@idConsulta", idConsulta);

                query.ExecuteNonQuery();
                con.Close();
            }
            catch (Exception error)
            {
                string errorMessage = error.Message;
                con.Close();
                return "Erro ao cancelar consulta!";
            }

            try
            {
                if (con.State == System.Data.ConnectionState.Closed)
                    con.Open();

                MySqlCommand query = new MySqlCommand("DELETE FROM consulta WHERE codigo = @idConsulta", con);
                query.Parameters.AddWithValue("@idConsulta", idConsulta);

                query.ExecuteNonQuery();
                con.Close();
            }
            catch (Exception error)
            {
                string errorMessage = error.Message;
                con.Close();
                return "Erro ao cancelar consulta!";
            }

            return "Consulta cancelada!";
        }
        #endregion
    }
}
