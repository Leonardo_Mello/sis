﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace SIS.Models.Users.Agenda
{
    public class Exame
    {
        #region parameters
        private static MySqlConnection con = new MySql.Data.MySqlClient.MySqlConnection(ConfigurationManager.ConnectionStrings["BCD"].ConnectionString);
        private string id, descricao, observacoes;
        private byte[] resultado;
        private Appointment consulta;

        public string ID
        {
            get { return id; }
            set { id = value; }
        }

        public string Descricao
        {
            get { return descricao; }
            set { descricao = value; }
        }
        
        public byte[] Resultado
        {
            get { return resultado; }
            set { resultado = value; }
        }

        public string Observacoes
        {
            get { return observacoes; }
            set { observacoes = value; }
        }

        public Appointment Consulta
        {
            get { return consulta; }
            set { consulta = value; }
        }

        public bool Concluido
        {
            get
            {
                try
                {
                    return Resultado.Length > 0;
                }
                catch (NullReferenceException)
                {
                    return false;
                }
            }
        }
        #endregion

        #region methods
        public string RegistrarExame(string consulta)
        {
            try
            {
                if (con.State == System.Data.ConnectionState.Closed)
                    con.Open();

                MySqlCommand query = new MySqlCommand("INSERT INTO exame(descricao, consulta) values (@descricao, @consulta)", con);
                query.Parameters.AddWithValue("@descricao", descricao);
                query.Parameters.AddWithValue("@consulta", consulta);

                query.ExecuteNonQuery();
                con.Close();
            }
            catch (Exception error)
            {
                string errorMessage = error.Message;
                con.Close();
                return "Erro ao agendar consulta!";
            }

            return "Consulta agendada com sucesso!";
        }

        public static string CancelarExame(string idExame)
        {
            try
            {
                if (con.State == System.Data.ConnectionState.Closed)
                    con.Open();

                MySqlCommand query = new MySqlCommand("DELETE FROM exame WHERE codigo = @idExame", con);
                query.Parameters.AddWithValue("@idExame", idExame);

                query.ExecuteNonQuery();
                con.Close();
            }
            catch (Exception error)
            {
                string errorMessage = error.Message;
                con.Close();
                return "Erro ao cancelar exame!";
            }

            return "Exame cancelado!";
        }

        public static string ConcluirExame(string idExame, byte[] documento_resultado, string observacoes)
        {
            try
            {
                if (con.State == System.Data.ConnectionState.Closed)
                    con.Open();

                MySqlCommand query = new MySqlCommand("UPDATE exame SET resultado = @documento_resultado, observacoes = @observacoes WHERE codigo = @idExame", con);
                query.Parameters.AddWithValue("@documento_resultado", documento_resultado);
                query.Parameters.AddWithValue("@observacoes", observacoes);
                query.Parameters.AddWithValue("@idExame", idExame);

                query.ExecuteNonQuery();
                con.Close();
            }
            catch (Exception error)
            {
                string errorMessage = error.Message;
                con.Close();
                return "Erro ao concluir o exame!";
            }

            return "Exame concluido!";
        }
        #endregion
    }
}