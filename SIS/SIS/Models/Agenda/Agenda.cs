﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace SIS.Models.Users.Agenda
{
    public class Agenda
    {
        private static MySqlConnection con = new MySqlConnection(ConfigurationManager.ConnectionStrings["BCD"].ConnectionString);

        public static List<Appointment> listaConsultas()
        {
            List<Appointment> lista = new List<Appointment>();
            try
            {
                if (con.State == System.Data.ConnectionState.Closed)
                    con.Open();

                MySqlCommand query = new MySqlCommand("SELECT c.codigo, c.data, c.duracao, c.especialidade, c.titulo, c. comentario, c.diagnostico, c.hospital, r.pessoa FROM consulta c, pessoa_consulta r WHERE c.codigo = r.consulta", con);

                MySqlDataReader reader = query.ExecuteReader();

                while(reader.Read())
                {
                    Appointment item = new Appointment();

                    item.Codigo = Convert.ToString(reader["codigo"]);
                    item.Data = Convert.ToDateTime(reader["data"]);
                    item.Duracao = Convert.ToDateTime(reader["duracao"].ToString());
                    item.Especialidade = Convert.ToString(reader["especialidade"]);
                    item.Titulo = Convert.ToString(reader["titulo"]);
                    item.Comentario = Convert.ToString(reader["comentario"]);
                    item.Diagnostico = Convert.ToString(reader["diagnostico"]);
                    
                    item.Patient_doctor[0] = Patient.GetInfo(reader["pessoa"].ToString());
                    
                    if(reader.Read())
                        item.Patient_doctor[1] = Doctor.GetInfo(reader["pessoa"].ToString());

                    //item. = Convert.ToString(reader["hospital"]);
                    //item.Id_Pessoa = reader["pessoa"].ToString();
                    //item.Estado = int.Parse(reader["estado"].ToString()) == 1;

                    //if(reader.Read()) // O Pessoa e o médico são guardados na mesma tabela e campo, por isso buscando o campo 'pessoa' duas vezes
                    //    item.Id_medico = reader["pessoa"].ToString();

                    lista.Add(item);
                }

                con.Close();
            }
            catch (Exception error)
            {
                string errorMessage = error.Message;
                con.Close();
                return new List<Appointment>();
            }

            return lista;
        }

        public static List<Exame> listaExames()
        {
            List<Exame> lista = new List<Exame>();
            try
            {
                if (con.State == System.Data.ConnectionState.Closed)
                    con.Open();

                MySqlCommand query = new MySqlCommand("SELECT * FROM exame", con);

                MySqlDataReader reader = query.ExecuteReader();

                while (reader.Read())
                {
                    Exame item = new Exame();

                    item.ID = reader["codigo"].ToString();
                    item.Descricao = reader["descricao"].ToString();

                    

                    if (reader["resultado"] != DBNull.Value)
                        item.Resultado = (byte[])reader["resultado"];
                    
                    item.Observacoes = reader["observacoes"].ToString();
                    string idConsulta = reader["consulta"].ToString();
                    item.Consulta = VisualizarConsulta(idConsulta);

                    lista.Add(item);
                }
                reader.Close();
                con.Close();
            }
            catch (Exception error)
            {
                string errorMessage = error.Message;
                con.Close();
                return new List<Exame>();
            }

            return lista;
        }

        public static Appointment VisualizarConsulta(string id)
        {
            MySqlConnection con = new MySqlConnection(ConfigurationManager.ConnectionStrings["BCD"].ConnectionString);
            try
            {
                if (con.State == System.Data.ConnectionState.Closed)
                    con.Open();

                MySqlCommand query = new MySqlCommand("SELECT c.codigo, c.data, c.duracao, c.especialidade, c.titulo, c. comentario, CAST(AES_DECRYPT(c.diagnostico, @chaveCriptografia) as char) as diagnostico, c.hospital, r.pessoa FROM consulta c, pessoa_consulta r WHERE c.codigo = r.consulta and codigo = @id", con);
                query.Parameters.AddWithValue("@chaveCriptografia", ConfigurationManager.ConnectionStrings["cryptographyKey"].ConnectionString);
                query.Parameters.AddWithValue("@id", id);

                MySqlDataReader reader = query.ExecuteReader();

                Appointment consulta = new Appointment();
                while (reader.Read())
                {
                    consulta.Codigo = reader["codigo"].ToString();
                    consulta.Data = (DateTime)reader["data"];
                    consulta.Duracao = Convert.ToDateTime(reader["duracao"].ToString());
                    consulta.Especialidade = reader["especialidade"].ToString();
                    consulta.Titulo = reader["titulo"].ToString();
                    consulta.Comentario = reader["comentario"].ToString();
                    consulta.Diagnostico = reader["diagnostico"].ToString();
                    consulta.Patient_doctor[0] = Patient.GetInfo(reader["pessoa"].ToString());

                    if (reader.Read()) // O Pessoa e o médico são guardados na mesma tabela e campo, por isso buscando o campo 'pessoa' duas vezes
                        consulta.Patient_doctor[1] = Doctor.GetInfo(reader["pessoa"].ToString());
                }
                con.Close();
                return consulta;
            }
            catch (Exception error)
            {
                string errorMessage = error.Message;
                con.Close();
                return null;
            }
        }

        public static Exame VisualizarExame(string id)
        {
            Exame exame = new Exame();
            try
            {
                if (con.State == System.Data.ConnectionState.Closed)
                    con.Open();

                MySqlCommand query = new MySqlCommand("SELECT codigo, descricao, CAST(AES_DECRYPT(resultado, @chaveCriptografia) as char) as resultado, observacoes, consulta FROM exame WHERE codigo = @id", con);
                query.Parameters.AddWithValue("@chaveCriptografia", ConfigurationManager.ConnectionStrings["cryptographyKey"].ConnectionString);
                query.Parameters.AddWithValue("@id", id);

                MySqlDataReader reader = query.ExecuteReader();


                if (reader.Read())
                {

                    exame.Descricao = reader["codigo"].ToString();
                    exame.Descricao = reader["descricao"].ToString();

                    if (reader["resultado"] != DBNull.Value)
                        exame.Resultado = (byte[])reader["resultado"];

                    exame.Observacoes = reader["observacoes"].ToString();
                    string idConsulta = reader["consulta"].ToString();
                    exame.Consulta = VisualizarConsulta(idConsulta);


                    reader.Close();
                    con.Close();
                }

                reader.Close();
                con.Close();
            }
            catch (Exception error)
            {
                string errorMessage = error.Message;
                con.Close();
                return null;
            }

            return exame;
        }

        public static PrescricaoMedica VisualizarPrescricaoMedica(string consulta)
        {
            MySqlConnection con = new MySqlConnection(ConfigurationManager.ConnectionStrings["BCD"].ConnectionString);
            PrescricaoMedica prescricao = new PrescricaoMedica();
            try
            {
                if (con.State == System.Data.ConnectionState.Closed)
                    con.Open();

                MySqlCommand query = new MySqlCommand("SELECT * FROM prescricao_medica WHERE consulta = @consulta", con);
                query.Parameters.AddWithValue("@chaveCriptografia", ConfigurationManager.ConnectionStrings["cryptographyKey"].ConnectionString);
                query.Parameters.AddWithValue("@consulta", consulta);

                MySqlDataReader reader = query.ExecuteReader();
                
                if (reader.Read())
                {
                    prescricao.ID = reader["codigo"].ToString();
                    prescricao.Medicamento = reader["medicamento"].ToString();
                    prescricao.Quantidade = reader["quantidade"].ToString();
                    prescricao.Duracao = reader["duracao"].ToString();
                    prescricao.Consulta = reader["consulta"].ToString();
                }
                con.Close();
                return prescricao;
            }
            catch (Exception error)
            {
                string errorMessage = error.Message;
                con.Close();
                return null;
            }
        }
    }
}