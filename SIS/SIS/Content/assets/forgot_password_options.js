﻿
(function ($) {
    "use strict";

    /** DECLARAÇÃO DAS VARIÁVEIS GLOBAIS **/
    var radios = document.getElementsByName("contact");
    var i, l;

    /** DESENVOLVIMENTO DAS FUNÇÕES **/
    $('input[name="contact"]').change(function () {
        if ($('#rbEmail').is(':checked')) {
            for (l = 1; l < radios.length; l++) {
                $('.telephone' + l + 'HiddenDiv').css('display', 'none');
                $('input[name="telephone' + l + 'Option"]').val("").blur();
                $('.telephone' + l + 'ErrorDiv').css('display', 'none');
            }
            $('.emailHiddenDiv').css('display', 'block');
            var email = $('label[for="rbEmail"]').text();
            var emailAddress = email.substring(email.indexOf("@"));
            $('input[name="emailAddress"]').val(emailAddress);
            $('.login100-form-btn').text('Enviar código');
        } else {
            for (i = 1; i < radios.length; i++) {
                if ($('#rbTelephone' + i).is(':checked')) {
                    $('.emailHiddenDiv').css('display', 'none');
                    $('input[name="emailOption"]').val("").blur();
                    $('.emailErrorDiv').css('display', 'none');
                    for (l = 1; l < radios.length; l++) {
                        $('.telephone' + l + 'HiddenDiv').css('display', 'none');
                        $('input[name="telephone' + l + 'Option"]').val("").blur();
                        $('.telephone' + l + 'ErrorDiv').css('display', 'none');
                    }
                    $('.telephone' + i + 'HiddenDiv').css('display', 'block');
                    var telephoneNumber = $('label[for="rbTelephone' + i + '"]').text();
                    var lastNumbers = telephoneNumber.substring(telephoneNumber.length - 2);
                    $('#telephone' + i + 'NumberSpan').text(lastNumbers);
                    $('.login100-form-btn').text('Enviar código');
                }
            }
        }
    });

    /* Função desenvolvida para validar o formulário da página html 'Forgot_password_options' antes do mesmo 
     * ser enviado ao seu respectivo controlador */
    // A função será chamada ao clicar-se no botão que possui id igual a 'sendCode'
    $('#sendCode').click(function () {
        // Declaração de variável
        var rbSelected = false;

        // Laço criado para percorrer todos os radio buttons existentes no formulário e validar se algum deles foi selecionado
        for (i = 0; i < radios.length; i++) {
            if (radios[i].checked) {
                rbSelected = true;
            }
        }

        // Se nenhuma opção de contato foi selecionada cancele o envio dos dados para o controlador e retorne mensagem de erro
        if (rbSelected === false) {
            $('#alert').css('display', 'block');
            return false;
        }
        // Se uma opção de contato foi selecionada
        else {
            // Se a opção que foi escolhida é a de envio de e-mail
            if ($('#rbEmail').is(':checked')) {
                // Declaração de variáveis locais e atribuição de dados simultaneamente
                var selectedEmail = $('label[for="rbEmail"]').text();
                var informedEmail = $('input[name="emailOption"]').val();
                var firstLetters_selectedEmail = selectedEmail.substring(11, 13);
                var firstLetters_informedEmail = informedEmail.substring(0, 2);
                // Se a parte do e-mail que está visível ao usuário não condizer com a informação inserida pelo mesmo 
                // cancele o envio dos dados e retorne mensagem de erro
                if (firstLetters_informedEmail !== firstLetters_selectedEmail) {
                    $('.emailErrorDiv').css('display', 'block');
                    $('#emailTextErrorSpan').text(firstLetters_selectedEmail);
                    return false;
                } else {
                    $('#selectedContactOption').val("0");
                    var informedData = informedEmail + $('input[name="emailAddress"]').val();
                    $('#informedData').val(informedData);
                }
            }
            // Se a opção que foi escolhida é a de envio de sms
            else {
                for (i = 1; i < radios.length; i++) {
                    if ($('#rbTelephone' + i).is(':checked')) {
                        // Declaração de variáveis locais e atribuição de dados simultaneamente
                        var selectedTelephone = $('label[for="rbTelephone' + i + '"]').text();
                        var informedTelephone = $('input[name="telephone' + i + 'Option"]').val();
                        var lastNumbers_selectedTelephone = selectedTelephone.substring(selectedTelephone.length - 2);
                        var lastNumbers_informedTelephone = informedTelephone.substring(informedTelephone.length - 2);
                        // Se a parte do número de telefone que está visível ao usuário não condizer com a informação inserida 
                        // pelo mesmo cancele o envio dos dados e retorne mensagem de erro
                        if (lastNumbers_informedTelephone !== lastNumbers_selectedTelephone) {
                            $('.telephone' + i + 'ErrorDiv').css('display', 'block');
                            $('#telephone' + i + 'NumberErrorSpan').text(lastNumbers_selectedTelephone);
                            return false;
                        } else {
                            $('#selectedContactOption').val(i);
                            $('#informedData').val(informedTelephone);
                        }
                    }
                }
            }
        }
        return true;

        //var postUrl = "/Home/Forgot_password_options";
        //$.post(postUrl, { username, selectedContactOption, informedData }, null);
    });

})(jQuery);