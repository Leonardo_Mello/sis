﻿
(function ($) {
    "use strict";

    /** DESENVOLVIMENTO DAS FUNÇÕES **/
    $(document).ready(function () {
        $('input[name="disability"]').change(function () {
            if ($('#rb_have_disability').is(':checked')) {
                $('.disability_hidden_div').css('display', 'block');
            } else {
                $('.disability_hidden_div').css('display', 'none');
            }
        });
    });

    $(document).ready(function () {
        $('input[name="allergy"]').change(function () {
            if ($('#rb_have_allergy').is(':checked')) {
                $('.type_allergy_hidden_div').css('display', 'block');
            } else {
                // Limpando o valor que possa ter sido selecionado como o tipo de alergia do paciente e desabilitando a respectiva div
                $('#type_allergy').val(null).trigger('change');
                $('.type_allergy_hidden_div').css('display', 'none');

                // Limpando o valor que possa ter sido selecionado/informado como a alergia do paciente e desabilitando a respectiva div
                $('div[class="respiratory_allergy"] select').val(null).trigger('change');
                $('div[class="dermatological_allergy"] select').val(null).trigger('change');
                $('div[class="food_allergy"] select').val(null).trigger('change');
                $('div[class="drug_allergy"] input').val("");
                $('div[class="eye_allergy"] select').val(null).trigger('change');
                $('.allergy_hidden_div').css('display', 'none');
            }
        });
    });

    $(document).ready(function () {
        $('select[id="type_allergy"]').change(function () {
            if ($(this).val() !== "" && $(this).val() !== null) {
                // Escondendo os elementos que dispõe opções de alergias
                $('div[class="respiratory_allergy"]').css('display', 'none');
                $('div[class="dermatological_allergy"]').css('display', 'none');
                $('div[class="food_allergy"]').css('display', 'none');
                $('div[class="drug_allergy"]').css('display', 'none');
                $('div[class="eye_allergy"]').css('display', 'none');

                var type_allergy = $(this).val();
                switch (type_allergy) {
                    case '1':
                        // Disponibilizando select que possui as opções referentes a alergias respiratórias
                        $('div[class="respiratory_allergy"]').css('display', 'block');
                        break;
                    case '2':
                        // Disponibilizando select que possui as opções referentes a alergias dermatológicas
                        $('div[class="dermatological_allergy"]').css('display', 'block');
                        break;
                    case '3':
                        // Disponibilizando select que possui as opções referentes a alergias alimentares
                        $('div[class="food_allergy"]').css('display', 'block');
                        break;
                    case '4':
                        // Disponibilizando campo para inserção da alergia medicamentosa
                        $('div[class="drug_allergy"]').css('display', 'block');
                        break;
                    case '5':
                        // Disponibilizando select que possui as opções referentes a alergias oculares
                        $('div[class="eye_allergy"]').css('display', 'block');
                        break;
                    default:
                        break;
                }
                $('.allergy_hidden_div').css('display', 'block');
            }
            else {
                $('.allergy_hidden_div').css('display', 'none');
            }
        });
    });

    $(document).ready(function () {
        $('select[id="telephone_type"]').change(function () {
            $('input[id="field_for_viewing"]').css('display', 'none');
            $('div[id="cell_phone"] input').val("");
            $('div[id="cell_phone"]').css('display', 'none');
            $('div[id="landline_phone"] input').val("");
            $('div[id="landline_phone"]').css('display', 'none');

            if ($(this).val() !== "" && $(this).val() !== null) {
                var telephone_type = $(this).val();
                if (telephone_type === "Celular") {
                    $('div[id="cell_phone"]').css('display', 'block');
                }
                else {
                    $('div[id="landline_phone"]').css('display', 'block');
                }
            }
            else {
                $('input[id="field_for_viewing"]').css('display', 'block');
            }
        });
    });

    /** DECLARAÇÃO DAS VARIÁVEIS GLOBAIS **/
    var radios_sexo = document.getElementsByName("sex");
    var i;

    $('button[id="register_patient"]').click(function () {
        //
        var id = $('input[name="id"]').val();
        $('input[name="Id"]').val(id);
        //
        var cpf = $('input[name="cpf"]').val();
        $('input[name="Cpf"]').val(cpf);
        //
        var nome = $('input[name="nome"]').val();
        $('input[name="Nome"]').val(nome);
        //
        var sobrenome = $('input[name="sobrenome"]').val();
        $('input[name="Sobrenome"]').val(sobrenome);
        //
        var data_nascimento = $('input[name="data_nascimento"]').val();
        $('input[name="Data_nascimento"]').val(data_nascimento);
        //
        var sexo;
        for (var i = 0; i < radios_sexo.length; i++) {
            if (rads[i].checked) {
                sexo = rads[i].value;
            }
        }
        $('input[name="Sexo"]').val(sexo);
        //
        var nome_mae = $('input[name="nome_mae"]').val();
        $('input[name="Nome_mae"]').val(nome_mae);
        //
        var nome_pai = $('input[name="nome_pai"]').val();
        $('input[name="Nome_pai"]').val(nome_pai);
        //
        var grupo_sanguineo = $('#grupo_sanguineo').find(":selected").val();
        $('input[name="Grupo_sanguineo"]').val(grupo_sanguineo);
        //

    });

})(jQuery);