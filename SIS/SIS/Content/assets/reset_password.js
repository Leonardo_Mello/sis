﻿
(function ($) {
    "use strict";

    /** DESENVOLVIMENTO DAS FUNÇÕES **/
    $('#changePassword').click(function () {
        var newPassword = $('input[name="newPassword"]').val();
        var confirmation_newPassword = $('input[name="confirmation_newPassword"]').val();
        if (newPassword !== confirmation_newPassword) {
            $('#alert').css('display', 'block');
            return false;
        }
        return true;
    });

})(jQuery);